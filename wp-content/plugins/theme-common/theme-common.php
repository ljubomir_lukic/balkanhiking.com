<?php
/**
* Plugin Name: theme-common
* Plugin URI: vergatheme.com
* Description: A plugin to create custom post type, metabox,...
* Version:  1.0
* Author: Vergatheme
* Author URI: vergatheme.com
* License:  GPL2
*/


include dirname( __FILE__ ) . '/custom-post-type/dolomia_trek_post_type.php';
include dirname( __FILE__ ) . '/custom-visual/shortcodes.php';
include dirname( __FILE__ ) . '/custom-visual/vc-shortcode.php';
include dirname( __FILE__ ) . '/redux/ReduxCore/framework.php';
include dirname( __FILE__ ) . '/redux/sample/sample-config.php';
include dirname( __FILE__ ) . '/cmb2/dolomia_cmb2.php';

return true;