<?php

global $pre_text;
$pre_text = 'VG ';

//Home showcase
add_shortcode('dolomia_home_showcase', 'dolomia_home_showcase_func');
function dolomia_home_showcase_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_showcase_img'       => '',
        'dolomia_home_showcase_title'       => '',
        'dolomia_home_showcase_heading'       => '',
        'dolomia_home_showcase_content'       => '',
        'dolomia_home_showcase_btn'       => '',
        'dolomia_home_showcase_btn_link'       => '',
        'dolomia_home_showcase_project'       => '',
        
    ), $atts));
    ob_start();
   
    $dolomia_showcase_img_src = wp_get_attachment_image_src($dolomia_showcase_img,'');
    $home_showcase_project = vc_param_group_parse_atts( $atts['dolomia_home_showcase_project'] );
?>
<!--  Vertical slider  -->
<!-- <div id="fullpage" class="full-width"> -->
    <!--  Main slide  -->
    <div class="section main-section" style="background-image:url(<?php echo esc_url($dolomia_showcase_img_src[0]); ?>)">
        <div class="text main text-center no-opacity">
            <h2 class="margin-bottom-null title center line white"><?php echo esc_attr($dolomia_home_showcase_title) ?></h2>
            <p class="heading center grey-light z-index"><?php echo htmlspecialchars_decode($dolomia_home_showcase_heading) ?></p>
            <div class="padding-onlytop-md">
                <p class="white"><?php echo esc_attr($dolomia_home_showcase_content) ?></p>
                <a href="<?php echo esc_attr($dolomia_home_showcase_btn_link) ?>" class="btn-alt small margin-null active"><?php echo esc_attr($dolomia_home_showcase_btn) ?></a>                               
            </div>
        </div>
        <div class="gradient"></div>
    </div>
    <!--  END Main slide  -->
    <?php 
    foreach ($home_showcase_project as $showcase_project) { 
        $dolomia_home_showcase_img_src = wp_get_attachment_image_src($showcase_project['dolomia_home_showcase_img'],'');

        ?>
   <!--  Slide  -->
    <div class="section project" style="background-image:url(<?php echo esc_url($dolomia_home_showcase_img_src[0]) ?>)">
        <div class="text text-center no-opacity">
            <h2 class="margin-bottom-null title line center white"><?php echo esc_attr($showcase_project['dolomia_home_showcase_title_pro']) ?></h2>
            <p class="heading center white grey-light margin-bottom"><?php echo htmlspecialchars_decode($showcase_project['dolomia_home_showcase_heading_pro']) ?></p>
            <a href="<?php echo esc_attr($showcase_project['dolomia_home_showcase_link_pro']) ?>" class="btn-alt small margin-null"><?php echo esc_attr($showcase_project['dolomia_home_showcase_btn_pro']) ?></a>
        </div>
        <div class="gradient"></div>
    </div>
   <!--  END Slide  --> 
    <?php
    }
     ?>
<!-- </div> -->
<!--  END Vertical slider  -->
<?php  
    return ob_get_clean();
}

// Home Showcase Footer
add_shortcode('dolomia_home_showcase_footer', 'dolomia_home_showcase_footer_func');
function dolomia_home_showcase_footer_func($atts, $content = null){
    extract(shortcode_atts(array(
        
    ), $atts));
    ob_start();
   $theme_option = get_option('theme_option');
?>
<div class="container">
    <div class="row no-margin">
        <div class="col-sm-4 col-md-2 padding-leftright-null">
            <?php if ( is_active_sidebar( 'footer-1' ) ) : 
              dynamic_sidebar('footer-1');
              endif;
            ?>
        </div>
        <div class="col-sm-4 col-md-2 padding-leftright-null">
            <?php if ( is_active_sidebar( 'footer-2' ) ) : 
              dynamic_sidebar('footer-2');
              endif;
            ?>
        </div>
        <div class="col-sm-4 col-md-4 padding-leftright-null">
            <?php if ( is_active_sidebar( 'footer-3' ) ) : 
              dynamic_sidebar('footer-3');
              endif;
            ?>
        </div>
        <div class="col-md-4 padding-leftright-null">
            <?php 
                if (is_active_sidebar('newsletters_widget')) {
                  dynamic_sidebar('newsletters_widget');
                }                       
             ?>
        </div>
    </div>
    <div class="copy">
        <div class="row no-margin">
            <div class="col-md-8 padding-leftright-null">
                <?php if (isset($theme_option['footer_copyright_content'])) { echo $theme_option['footer_copyright_content'];
                        } else{}; ?>
            </div>
            <div class="col-md-4 padding-leftright-null">
                <ul class="social">
                    <?php  
                    if (isset($theme_option['footer_social'])) {
                      foreach ($theme_option['footer_social'] as $footer_socials) {
                        $footer_social_explode = explode(",",$footer_socials);

                    ?>
                      <li> <a href="<?php echo esc_attr($footer_social_explode[0]) ?>"><i class="fa fa-<?php echo esc_attr($footer_social_explode[1]) ?>" aria-hidden="true"></i></a> </li>
             
                  <?php } 
                    }
                   ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php  
    return ob_get_clean();
}

//Home Carousel Slider
add_shortcode('dolomia_carousel_slider', 'dolomia_carousel_slider_func');
function dolomia_carousel_slider_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_carousel_slider_item'       => '',
    ), $atts));
    ob_start();

    $dolomia_carousel_slider_item = vc_param_group_parse_atts( $atts['dolomia_carousel_slider_item'] );
?>
    <!--  Slider  -->
    <div class="home-carousel">
    <?php 
        foreach ($dolomia_carousel_slider_item as $carousel_slider) { 
            $img_src = wp_get_attachment_image_src($carousel_slider['dolomia_carousel_slider_img'],'');
            ?>
             <!--  Single Item  -->
        <div class="item">
            <div class="image" style="background-image:url(<?php echo esc_url($img_src[0]); ?>)">
                <div class="content text-center">
                    <div class="content-top">
                        <h2 class="margin-bottom-null title line center white"><?php echo esc_attr($carousel_slider['dolomia_carousel_slider_title']); ?></h2>
                        <p class="heading center white grey-light margin-bottom"><?php echo esc_attr($carousel_slider['dolomia_carousel_slider_heading']); ?></p>
                    </div>
                    <div class="content-bottom">
                        <a href="<?php echo esc_attr($carousel_slider['dolomia_carousel_slider_link']); ?>" class="btn-alt small shadow"><?php echo esc_attr($carousel_slider['dolomia_carousel_slider_btn']); ?></a>
                    </div>
                </div>
            </div>
        </div>
        <!--  END Single Item  -->
        <?php
        }
     ?>     
    </div>

<?php  
    return ob_get_clean();
}

//Trip Showcase
add_shortcode('dolomia_showcase', 'dolomia_showcase_func');
function dolomia_showcase_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_showcase_title'       => '',
        'dolomia_showcase_subtitle'       => '',
        'dolomia_showcase_id'       => '',
        
    ), $atts));
    ob_start();
   
   $showcase_ids = explode(',' ,$dolomia_showcase_id);
?>
    <!-- Trip Showcase  -->
    <div id="showcase-treks" class="text padding-bottom-null grey-background center">
        <div class="container">
            <div class="col-md-12 padding-leftright-null text-center">
                <h2 class="margin-bottom-null title line center"><?php echo esc_attr($dolomia_showcase_title); ?></h2>
                <p class="heading center grey margin-bottom-null"><?php echo esc_attr($dolomia_showcase_subtitle) ?></p>
            </div>
            <div class="col-md-12 padding-leftright-null">
                <section class="showcase-carousel text">
                 <?php
                $show_post = array(
                    'post_type' => 'trek',
                    'posts_per_page'  => -1,
                    'post__in'  => $showcase_ids,
                    );
                 $all_post = new WP_Query($show_post);

                 if ($all_post->have_posts()) : while($all_post->have_posts()) : $all_post->the_post();
                        $name_place = get_post_meta( get_the_ID(), 'trek_name_place', true );
                        $country = get_post_meta( get_the_ID(), 'trek_country', true );
                        $trek_datas = get_post_meta( get_the_ID(),'trek_data', true );
                        $guide = get_post_meta( get_the_ID(),'trek_visitor', true );
                ?>
                    <!--  Single Trip  -->
                    <div id="treks-hp" class="item">
                        <div class="showcase-trek">
                     <?php 
                        if(isset($trek_datas) && $trek_datas != ''){
                             $x = '';
                             $y = '';
                            foreach ($trek_datas as $entrys ) {
                               $x .= $entrys['trek_data2'].' ';
                               $y .= $entrys['trek_data1'].',';
                        }
                        $a = explode("|", $x);
                        $b = explode(",", $y);
                        //var_dump($b);
                    ?>
                        <?php if($a[0] !== "0"):?>
                            <span class="read">
                                <?php echo $a[0]; ?>
                            </span>
                            <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="">
                            <div class="content text-center">
                                <div class="row margin-leftright-null">
                                    <div class="meta">
                                        <div class="col-md-4 padding-leftright-null">
                                            <h3><?php echo $a[4]; ?></h3>
                                            <h4><?php echo $b[4]; ?></h4>
                                        </div>
                                        <div class="col-md-4 padding-leftright-null">
                                            <h3><?php echo $a[3]; ?></h3>
                                            <h4><?php echo $b[3]; ?></h4>
                                        </div>
                                        <div class="col-md-4 padding-leftright-null">
                                            <h3><?php echo $a[2]; ?></h3>
                                            <h4><?php echo $b[2]; ?></h4>
                                        </div>
                                    </div>
                                    <div class="category">
                                        <h3><?php the_title(); ?></h3>
                                    </div>
                                    <div class="info">
                                    <!-- <?php 
                                         if(isset($trek_datas) && $trek_datas != ''){
                                     ?> -->
                                        <div class="col-md-12 padding-leftright-null">
                                            <h6 class="heading"><?php echo esc_attr($name_place); ?></h6>
                                            <p class="margin-null"><?php echo esc_attr($guide); ?></p>
                                        </div>
                                        <!-- <?php } ?> -->
                                    </div>
                                </div>
                            </div>
                            <?php else:?>
                            <div class="trek-without-data" style='background-image: url("<?php echo wp_get_attachment_url(get_post_thumbnail_id())?>")'>
                            	<img src="<?php echo includes_url('images/transparent-460x250.png'); ?>" alt="">
                            	<div class="content text-center">
                               		<div class="row margin-leftright-null">
                                        <div class="meta">
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3><?php echo $a[4]; ?></h3>
                                                <h4><?php echo $b[4]; ?></h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3><?php echo $a[3]; ?></h3>
                                                <h4><?php echo $b[3]; ?></h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3><?php echo $a[2]; ?></h3>
                                                <h4><?php echo $b[2]; ?></h4>
                                            </div>
                                        </div>
                                        <div class="category">
                                            <h3><?php the_title(); ?></h3>
                                        </div>
                                        <div class="info">
                                        <!-- <?php 
                                             if(isset($trek_datas) && $trek_datas != ''){
                                         ?> -->
                                            <div class="col-md-12 padding-leftright-null">
                                                <h6 class="heading"><?php echo esc_attr($name_place); ?></h6>
                                                <p class="margin-null"><?php echo esc_attr($guide); ?></p>
                                            </div>
                                            <!-- <?php } ?> -->
                                        </div>
                                	</div>
                                </div>
                            </div>
                            <?php endif;?>
                        <a href="<?php the_permalink() ?>" class="link"></a>
                        <?php } ?>
                    </div>

                    </div>
                    <!--  END Single Trip  -->
                <?php endwhile; endif; ?>
                </section>
            </div>
        </div>
    </div>
	<!--  END Trip Showcase  -->
<?php  
    return ob_get_clean();
}

// ========Single treks========

//row margin-leftright-null grey-background
add_shortcode('dolomia_grey_background', 'dolomia_grey_background_func');
function dolomia_grey_background_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_grey_background_image'       => '',
        'dolomia_grey_background_data'       => '',
        
    ), $atts));
    ob_start();

    $dolomia_grey_background_image_src = wp_get_attachment_image_src($dolomia_grey_background_image,'');
    $dolomia_grey_background_data = vc_param_group_parse_atts( $atts['dolomia_grey_background_data'] );
   
?>
     <div class="row margin-leftright-null grey-background">
        <div class="bg-img overlay simple-parallax responsive" style="background-image:url(<?php echo esc_url($dolomia_grey_background_image_src[0]) ?>)">
           <div class="container">
               <!-- Trek data -->
 
                <div class="text trek-data text-center">
                <?php 
                    foreach ($dolomia_grey_background_data as $data_item) { ?>
                   <div class="col-sm-4 col-md-2">
                       <i class="pd-icon-<?php echo esc_attr($data_item['dolomia_grey_background_data_icon']);?> service big margin-bottom-null white"></i>
                       <em class="color"><?php echo esc_attr($data_item['dolomia_grey_background_data_title']);?></em>
                       <h3 class="white"><?php echo esc_attr($data_item['dolomia_grey_background_data_subtitle']);?></h3>
                   </div>
                   <?php
                    }
                ?>
               </div>
                    
               <!-- END Trek data -->
           </div>
        </div>
    </div>
<?php  
    return ob_get_clean();
}


//trek content
add_shortcode('dolomia_content_trek', 'dolomia_content_trek_func');
function dolomia_content_trek_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_content_trek_heading'       => '',
        'dolomia_content_trek_description'       => '',
        'dolomia_content_trek_content'      => '',
        'dolomia_content_trek_table_responsive'      => '',
    ), $atts));
    ob_start();

    $dolomia_content_trek_table_responsive = vc_param_group_parse_atts( $atts['dolomia_content_trek_table_responsive'] );
    
?>

   <div class="row margin-leftright-null">
        <div class="container">
            <div class="col-md-12 text padding-bottom-null text-center">
                <h2 class="margin-bottom-null title line center"><?php echo esc_attr($dolomia_content_trek_heading); ?></h2>
                <p class="heading center grey margin-bottom-null"><?php echo esc_attr($dolomia_content_trek_description); ?></p>
            </div>
            <div class="col-md-12 text text-description">
                <?php echo htmlspecialchars_decode($dolomia_content_trek_content); ?>
                <?php if(count($dolomia_content_trek_table_responsive)>0):?>
                <div class="table-responsive shadow">
                    <table class="table">
                        <tbody>
                            <?php 
                                foreach ($dolomia_content_trek_table_responsive as $trek_table_responsive) { ?>
                                    <tr>
                                        <th><?php echo esc_attr($trek_table_responsive['dolomia_content_trek_content_1']); ?></th>
                                        <th><?php echo esc_attr($trek_table_responsive['dolomia_content_trek_content_2']); ?></th>
                                        <th><?php echo esc_attr($trek_table_responsive['dolomia_content_trek_content_3']); ?></th>
                                        <th><?php echo esc_attr($trek_table_responsive['dolomia_content_trek_content_4']); ?></th>
										<th><?php echo esc_attr($trek_table_responsive['dolomia_content_trek_content_5']); ?></th>
                                        <th><?php echo esc_attr($trek_table_responsive['dolomia_content_trek_content_6']); ?></th>
                                        <th><?php echo esc_attr($trek_table_responsive['dolomia_content_trek_content_7']); ?></th>
                                        <th><?php echo esc_attr($trek_table_responsive['dolomia_content_trek_content_8']); ?></th>
                                        <th><?php echo esc_attr($trek_table_responsive['dolomia_content_trek_content_9']); ?></th>
                                    </tr>                            
                                <?php
                                }
                             ?>
                            
                        </tbody>
                    </table>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>

<?php  
    return ob_get_clean();
}

//trek-map

add_shortcode('dolomia_trek_map', 'dolomia_trek_map_func');
function dolomia_trek_map_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_trek_map_frame'       => '',

    ), $atts));
    ob_start();
?>

    <div class="row margin-leftright-null trek-map">
        <iframe src="<?php echo esc_attr($dolomia_trek_map_frame); ?>">           
        </iframe>
    </div>

<?php  
    return ob_get_clean();
}

//Share Button
add_shortcode('dolomia_share_btn', 'dolomia_share_btn_func');
function dolomia_share_btn_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_share_btn_share'       => '',

    ), $atts));
    ob_start();

    $dolomia_share_btn_share = vc_param_group_parse_atts( $atts['dolomia_share_btn_share'] );
?>

     <div id="share">
        <a class="share-btn">
            <i class="icon ion-android-share-alt"></i>
        </a>
        <div class="share-icons" style="display:none">
        <?php 
            foreach ($dolomia_share_btn_share as $share_btn) { ?>
                <a href="<?php echo esc_attr($share_btn['dolomia_share_btn_share_link']); ?>" class="share-<?php echo esc_attr($share_btn['dolomia_share_btn_share_name_icon']); ?>">
                    <i class="<?php echo esc_attr($share_btn['dolomia_share_btn_share_icon']); ?>" aria-hidden="true"></i>
                </a>  
        <?php        
            }
         ?>
            
        </div>
    </div>

<?php  
    return ob_get_clean();
}

//Table
add_shortcode('dolomia_table', 'dolomia_table_func');
function dolomia_table_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_table_group'       => '',

    ), $atts));
    ob_start();

    $dolomia_table_group = vc_param_group_parse_atts( $atts['dolomia_table_group'] );
?>

       <!--  Table  -->
    <div class="row padding-sm">
        <div class="col-md-12">
            <div class="table-responsive shadow">
                <table class="table">
                    <tbody>
                        <tr>
                            <th>#</th>
                            <th>Title 1</th>
                            <th>Title 2</th>
                            <th>Title 3</th>
                            <th>Title 4</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--  END Table  -->
<?php  
    return ob_get_clean();
}


//Contact trek
add_shortcode('dolomia_trek_contact', 'dolomia_trek_contact_func');
function dolomia_trek_contact_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_trek_contact_heading'       => '',
        'dolomia_trek_contact_des'       => '',
        'dolomia_trek_contact_method'      => '',
        'dolomia_trek_contact_button'      => '',
    ), $atts));
    ob_start();


?>
        <div class="col-md-12 text-center">
            <h4 class="big white"><?php echo esc_attr($dolomia_trek_contact_heading); ?></h4>
            <h4 class="big margin-bottom-small white"><?php echo esc_attr($dolomia_trek_contact_des); ?> <a href="" class="btn-pro simple white"><?php echo esc_attr($dolomia_trek_contact_method); ?></a></h4>
            <a href="<?php echo site_url('contact')?>" target="_blank" class="btn-alt small white margin-null active shadow"><?php echo esc_attr($dolomia_trek_contact_button); ?></a>
        </div>

<?php  
    return ob_get_clean();
}

//Contact  All Treks
add_shortcode('dolomia_all_trek_contact', 'dolomia_all_trek_contact_func');
function dolomia_all_trek_contact_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_all_trek_contact_title'       => '',
        'dolomia_all_trek_contact_btn'       => '',
    ), $atts));
    ob_start();
?>
    <!--  Call to Action  -->
    <div class="row margin-leftright-null color-background">
        <div class="col-md-12 text text-center">
            <h4 class="big white margin-bottom-small"><?php echo esc_attr($dolomia_all_trek_contact_title); ?></h4>
            <a href="<?php echo site_url('contact')?>" target="_blank" class="btn-alt small white margin-null active shadow"><?php echo esc_attr($dolomia_all_trek_contact_btn); ?></a>
        </div>
    </div>
    <!--  END Call to Action  -->
<?php  
    return ob_get_clean();
}

//trek_gallery
add_shortcode('dolomia_trek_gallery', 'dolomia_trek_gallery_func');
function dolomia_trek_gallery_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_trek_gallery_heading'       => '',
        'dolomia_trek_gallery_des'       => '',
        'dolomia_trek_gallery_item'      => '',
    ), $atts));
    ob_start();
    $dolomia_trek_gallery_item = vc_param_group_parse_atts( $atts['dolomia_trek_gallery_item'] );
?>
    <div class="row margin-leftright-null">
        <div class="container text">
            <div class="col-md-12 text padding-top-null text-center">
                <h2 class="margin-bottom-null title line center"><?php echo esc_attr($dolomia_trek_gallery_heading); ?></h2>
                <p class="heading center grey margin-bottom-null"><?php echo esc_attr($dolomia_trek_gallery_des); ?></p>
            </div>
            <!-- Simple Gallery -->
            <section class="grid-images padding-top-null">
                
                <div class="row padding-onlytop-sm">
                <?php 

                    foreach ($dolomia_trek_gallery_item as $gallery_item) {

                        $gallery_main_img = wp_get_attachment_image_src($gallery_item['dolomia_trek_gallery_main_img'],'');
                        $gallery_sub_img = wp_get_attachment_image_src($gallery_item['dolomia_trek_gallery_sub_img'],'');
                        ?>
                        <div class="col-md-6">
                            <div class="image simple-shadow" style="background-image:url(<?php echo esc_url($gallery_main_img[0]); ?>)">
                                <a class="lightbox-image" href="<?php if($gallery_sub_img != ''){ echo esc_url($gallery_sub_img[0]);} else{} ?>">
                                </a>
                            </div>
                        </div>
                    <?php
                    }

                 ?>                   
                </div>
            </section>
            <!-- END Simple Gallery -->
        </div>
    </div>

<?php  
    return ob_get_clean();
}

//Main content of single trek
add_shortcode('dolomia_trek_main', 'dolomia_trek_main_func');
function dolomia_trek_main_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_trek_main_content'       => '',

    ), $atts));
    ob_start();

?>

<div id="post-wrap" class="content-section fullpage-wrap">
<div class="row margin-leftright-null grey-background">
    <div class="container">
        <div class="col-md-12 text">
            <?php echo htmlspecialchars_decode($dolomia_trek_main_content); ?>
        </div>
    </div>
</div>
</div>


<?php  
    return ob_get_clean();
}

//Similar Trips 
add_shortcode('dolomia_trek_similar', 'dolomia_trek_similar_func');
function dolomia_trek_similar_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_similar_trips_title'       => '',
        'dolomia_similar_trips_subtitle'       => '',
        'dolomia_similar_trips_id'       => '',

    ), $atts));
    ob_start();

    $arr_id_trip = explode(',' ,$dolomia_similar_trips_id);
?>

    <div id="showcase-treks" class="row margin-leftright-null grey-background">
        <div class="container">
            <div class="col-md-12 text padding-bottom-null text-center">
                <h2 class="margin-bottom-null title line center"><?php echo esc_attr($dolomia_similar_trips_title) ?></h2>
                <p class="heading center grey margin-bottom-null"><?php echo esc_attr($dolomia_similar_trips_subtitle) ?></p>
            </div>
            <div class="col-md-12 text" id="treks">
            <!-- Single Trip -->
            <?php
                $arr_id_trip = explode(',' ,$dolomia_similar_trips_id);
                $show_post = array(
                    'post_type' => 'trek',
                    'posts_per_page'  => -1,
                    'post__in'  => $arr_id_trip,
                    );
                 $all_post = new WP_Query($show_post);

                if ($all_post->have_posts()) : while($all_post->have_posts()) : $all_post->the_post();
                        $name_place = get_post_meta( get_the_ID(), 'trek_name_place', true );
                        $country = get_post_meta( get_the_ID(), 'trek_country', true );
                        $trek_datas = get_post_meta( get_the_ID(),'trek_data', true );
                        $guide = get_post_meta( get_the_ID(),'trek_visitor', true );
                ?>
                <div class="item col-md-6">
                    <div class="showcase-trek">
                     <?php 
                        if(isset($trek_datas) && $trek_datas != ''){
                             $x = '';
                             $y = '';
                            foreach ($trek_datas as $entrys ) {
                               $x .= $entrys['trek_data2'].' ';
                               $y .= $entrys['trek_data1'].',';
                        }
                        $a = explode("|", $x);
                        $b = explode(",", $y);
                        // var_dump($b);
                    ?>
                       <span class="read">
                            <?php echo $a[0]; ?>
                        </span>
                        <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="">
                        <div class="content text-center">
                            <div class="row margin-leftright-null">
                                <div class="meta">
                                    <div class="col-md-4 padding-leftright-null">
                                        <h3><?php echo $a[4]; ?></h3>
                                        <h4><?php echo $b[4]; ?></h4>
                                    </div>
                                    <div class="col-md-4 padding-leftright-null">
                                        <h3><?php echo $a[3]; ?></h3>
                                        <h4><?php echo $b[3]; ?></h4>
                                    </div>
                                    <div class="col-md-4 padding-leftright-null">
                                        <h3><?php echo $a[2]; ?></h3>
                                        <h4><?php echo $b[2]; ?></h4>
                                    </div>
                                </div>
                                <div class="category">
                                    <h3><?php the_title(); ?></h3>
                                </div>
                                <div class="info">
                                <!-- <?php 
                                     if(isset($trek_datas) && $trek_datas != ''){
                                 ?> -->
                                    <div class="col-md-12 padding-leftright-null">
                                        <h6 class="heading"><?php echo esc_attr($name_place); ?></h6>
                                        <p class="margin-null"><?php echo esc_attr($guide); ?></p>
                                    </div>
                                    <!-- <?php } ?> -->
                                </div>
                            </div>
                        </div>
                        <a href="<?php the_permalink() ?>" class="link"></a>
                        <?php } ?>
                    </div>
                </div>
                <!-- END Single Trip -->
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
<!-- END Similar Trips -->

<?php  
    return ob_get_clean();
}

// Contact Us
add_shortcode('dolomia_contact', 'dolomia_contact_func');
function dolomia_contact_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_contact_title'                       => '',
        'dolomia_contact_subtitle'                      => '',
        'dolomia_contact_content'                   => '',
        'dolomia_contact_item'                   => '',
        'dolomia_contact_day'                   => '',
    ), $atts));
    ob_start();

    $dolomia_contact_item = vc_param_group_parse_atts( $atts['dolomia_contact_item'] );
    $dolomia_contact_day = vc_param_group_parse_atts( $atts['dolomia_contact_day'] );
?>
    <!--  Contact Info  -->
    <!-- <div class="container"> -->
    <div class="padding-leftright-null">
        <div class="text">
            <h2 class="margin-bottom-null title line left"><?php echo esc_attr($dolomia_contact_title); ?></h2>
            <p class="heading center grey margin-bottom-null"><?php echo esc_attr($dolomia_contact_subtitle); ?></p>
            <div class="padding-onlytop-md">
                <p class="margin-bottom"><?php echo esc_attr($dolomia_contact_content); ?></p>
                <p>
                <?php 
                    foreach ($dolomia_contact_item as $contact_item) { 
                        // if ($contact_item['dolomia_contact_link'] && $contact_item['dolomia_contact_link'] != '') {
                        ?>
                            <span class="contact-info"><?php echo esc_attr($contact_item['dolomia_contact_by']) ?> <a href="<?php echo esc_attr($contact_item['dolomia_contact_link']) ?>"><em><?php echo esc_attr($contact_item['dolomia_contact_type']) ?>
                            </em>
                            </a>
                            </span>
                            <br>
                        <?php
                        // }
                        // else{ ?>
                            <!-- <span class="contact-info"><?php echo esc_attr($contact_item['dolomia_contact_by']) ?> 
                            <em><?php echo esc_attr($contact_item['dolomia_contact_type']) ?>
                            </em>
                            </span>
                            <br> -->
                        <?php
                        // }
                    }
                 ?>                    
                </p>
                <p class="margin-md-bottom-null">                
                <?php 
                    foreach ($dolomia_contact_day as $contact_day) { ?>
                        <span class="contact-info"><?php echo esc_attr($contact_day['dolomia_contact_day1']) ?> 
                            <em><?php echo esc_attr($contact_day['dolomia_contact_time']) ?></em> 
                        </span>
                        <br>
                   <?php
                    }
                 ?>                   
                </p>
            </div>
        </div>
    </div>
    <!-- </div> -->

    <!--  END Contact Info -->
<?php
    return ob_get_clean();
}

//Map
add_shortcode('dolomia_map', 'dolomia_map_func');
function dolomia_map_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_map_lat'                       => '',
        'dolomia_map_lon'                      => '',
    ), $atts));
    ob_start();

    
?>
    <div class="row margin-leftright-null">
        <!--  Map. Settings in assets/js/maps.js  -->
        <div class="col-md-12 padding-leftright-null map">
            <div id="map"></div
        </div>
        <!--  END Map  -->
    </div>
    <script type="text/javascript">
    jQuery(document).ready(function($) {   
        var $ = jQuery.noConflict();

    (function($) {
    "use strict";

    /*-------------------------------------------------*/
    /* =  Contact Map
    /*-------------------------------------------------*/

    var contact = {"lat":"<?php echo esc_attr($dolomia_map_lat) ?>", "lon":"<?php echo esc_attr($dolomia_map_lon) ?>"}; //Change a map coordinate here!

        var map;
        var MY_MAPTYPE_ID = 'custom_style';
        function initialize() {
            var featureOpts = [];
            
            var myLatlng = new google.maps.LatLng(contact.lat, contact.lon);
            var mapOptions = {
                zoom: 15,
                center: myLatlng,
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
                },
                mapTypeId: MY_MAPTYPE_ID,
                disableDefaultUI: true,
                zoomControl: true,
                scrollwheel: false
            }
            map = new google.maps.Map(document.getElementById('map'), mapOptions);
            var styledMapOptions = {
                name: 'Custom Style'
            };
            var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
            map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: {
                    path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                    scale: 10,
                    strokeWeight:8,
                    strokeColor:"#2F2911"
                },
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    
})(jQuery);
      });
    </script>
<?php
    return ob_get_clean();
}
 

//Header Home
add_shortcode('dolomia_header_home', 'dolomia_header_home_func');
function dolomia_header_home_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_header_image'                       => '',
        'dolomia_header_title'                      => '',
        'dolomia_header_subtitle'                   => '',
    ), $atts));
    ob_start();
    $dolomia_header_image_src = wp_get_attachment_image_src($dolomia_header_image,'');
    
?>
<!--     <div id="flexslider" class="fullpage-wrap small"> -->
        <ul class="slides">
            <li style="background-image:url(<?php echo esc_url($dolomia_header_image_src[0]); ?>)">
                <div class="container text text-center">
                    <h1 class="white margin-bottom-small"><?php echo esc_attr($dolomia_header_title); ?></sh1>
                    <p class="heading white"><?php echo esc_attr($dolomia_header_subtitle); ?></p>
                </div>
                <div class="gradient dark"></div>
            </li>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active"><?php echo esc_attr($dolomia_header_title); ?></li>
            </ol>
        </ul>
    
<?php
    return ob_get_clean();
}
 

//Left Section
add_shortcode('dolomia_left_section', 'dolomia_left_section_func');
function dolomia_left_section_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_left_section_title'       => '',
        'dolomia_left_section_subtitle'       => '',
        'dolomia_left_section_content'      => '',
        'dolomia_left_subsection_content' => '',
    ), $atts));
    ob_start();
?>
    <div class="padding-leftright-null">
        <div data-responsive="parent-height" data-responsive-id="info" class="text padding-md-bottom-null">
            <h2 class="margin-bottom-null title line left"><?php echo esc_attr($dolomia_left_section_title); ?></h2>
            <p class="heading center grey margin-bottom-null"><?php echo esc_attr($dolomia_left_section_subtitle); ?></p>
            <div class="padding-onlytop-md">
                <p class="margin-bottom-null"><?php echo esc_attr($dolomia_left_section_content); ?></p>
                <br>
                <p class="margin-bottom-null"><?php echo esc_attr($dolomia_left_subsection_content); ?></p>
            </div>
        </div>
    </div>
    
<?php  
    return ob_get_clean();
}

//Skills
add_shortcode('dolomia_skill', 'dolomia_skill_func');
function dolomia_skill_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_skill_list'       => '',
    ), $atts));
    ob_start();

    $dolomia_skill_list = vc_param_group_parse_atts( $atts['dolomia_skill_list'] );
?>

    <div class="padding-leftright-null">
        <div data-responsive="child-height" data-responsive-id="info" class="text padding-md-top-null responsive-md-height-auto">
            <!--  Skills  -->
            <section id="skills" class="padding-onlytop-md paddin-md-null">
                <div class="col-md-12 padding-leftright-null">
                    <ul class="skill-list padding-sm-bottom">
                    <?php 

                        foreach ($dolomia_skill_list as $about_me_list) {
                            ?>
                            <em><?php echo esc_attr($about_me_list['dolomia_skill_list_item']); ?></em>
                            <li>
                                <span class="border-color" data-percent="<?php echo esc_attr($about_me_list['dolomia_skill_list_percent']); ?>%"></span>
                                <span class="label"><span class="count" data-to="<?php echo esc_attr($about_me_list['dolomia_skill_list_percent']); ?>"></span>%</span>
                            </li>
                         <?php   
                        }
                        ?>
                                                     
                    </ul>
                </div>
            </section>
            <!--  END Skills  -->
        </div>
    </div>
<?php  
    return ob_get_clean();
}


//Testimonials
add_shortcode('dolomia_about_me_slider', 'dolomia_about_me_slider_func');
function dolomia_about_me_slider_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_about_me_slider_bg'       => '',
        'dolomia_about_me_slider_item'       => '',
    ), $atts));
    ob_start();

   $dolomia_about_me_slider_bg_src = wp_get_attachment_image_src($dolomia_about_me_slider_bg,'');
   $dolomia_about_me_slider_item = vc_param_group_parse_atts( $atts['dolomia_about_me_slider_item'] );
    
?>  
  <!--  Section Image Background with overlay  -->
    <div class="row margin-leftright-null grey-background">
        <div class="bg-img overlay simple-parallax responsive" style="background-image:url(<?php echo esc_url($dolomia_about_me_slider_bg_src[0]); ?>)">
           <div class="container">
               <!-- Testimonials -->
               <section class="testimonials-carousel-simple col-md-12 text padding-bottom-null">
               <?php 
                    foreach ($dolomia_about_me_slider_item as $slider_item) {?>
                        <div class="item padding-leftright-null">
                           <div class="padding-top-null padding-bottom-null">
                               <blockquote class="margin-bottom-small white"><?php echo esc_attr($slider_item['dolomia_about_me_slider_content']); ?><em class="small grey-light"><?php echo esc_attr($slider_item['dolomia_about_me_slider_author']); ?></em></blockquote>
                           </div>
                       </div>
                    <?php
                    }
                ?>
               </section>
               <!-- END Testimonials -->
           </div>
        </div>
    </div>
    <!--  END Section Image Background with overlay  -->

<?php  
    return ob_get_clean();
}

//Lastest news
add_shortcode('dolomia_lastest', 'dolomia_lastest_func');
function dolomia_lastest_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_lastest_title'       => '',
        'dolomia_lastest_subtitle'       => '',
        'dolomia_lastest_id'       => '',
    ), $atts));
    ob_start();
    $arr_id = explode(',' ,$dolomia_lastest_id);
    
?> 

    <!-- <div class="row margin-leftright-null grey-background"> -->
        <!-- <div class="container"> -->
            <div class="col-md-12 text padding-bottom-null text-center">
                <h2 class="margin-bottom-null title line center"><?php echo esc_attr($dolomia_lastest_title); ?></h2>
                <p class="heading center grey margin-bottom-null"><?php echo esc_attr($dolomia_lastest_subtitle); ?></p>
            </div>
           
            <div class="col-md-12 text" id="news">
                    <!-- Single News -->
                <?php
                    $show_post = array(
                        'post_type' => 'post',
                        'posts_per_page'  => -1,
                        'post__in'  => $arr_id,
                        );
                     $all_post = new WP_Query($show_post);

                     if ($all_post->have_posts()) : while($all_post->have_posts()) : $all_post->the_post();
                    ?>
                    <div class="col-sm-6 single-news horizontal-news">
                        <article>
                            <div class="col-md-6 padding-leftright-null">
                                <div class="image" style="background-image:url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>)"></div>
                            </div>
                            <div class="col-md-6 padding-leftright-null">
                                <div class="content">
                                    <h3><?php the_title(); ?></h3>
                                    <span class="date"><?php the_date(); ?></span>
                                    <p><?php echo dolomia_excerpt(20); ?></p>
                                    <?php 

                                    $categories = get_the_category();
                                    if ( ! empty( $categories ) ) {
                                        foreach ($categories as $category) {    
                                        echo '<span class="category">'.$category->name.'</span>';
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <a href="<?php the_permalink(); ?>" class="link"></a>
                        </article>
                    </div>
                    <?php endwhile; endif; ?>
                    <!-- END Single News -->
                </div>
        <!-- </div> -->
    <!-- </div> -->

<?php  
    return ob_get_clean();
}

//Sponsor
add_shortcode('dolomia_sponsor', 'dolomia_sponsor_func');
function dolomia_sponsor_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_sponsor_imgs'       => '',
    ), $atts));
    ob_start();
    $img_ids = explode(",",$dolomia_sponsor_imgs);
?>
  
  <!--  Sponsor  -->
    <div class="row no-margin">
        <div class="container text">
            <div class="col-md-12 sponsor-carousel padding-leftright-null">
            <?php 
            foreach ($img_ids as $dolomia_sponsor_images) { 
                $dolomia_sponsor_images_src = wp_get_attachment_image_src($dolomia_sponsor_images,'');
                ?>
                <div class="item">
                    <img class="center" src="<?php echo esc_url($dolomia_sponsor_images_src[0]) ?>" alt="client logo">
                </div>
            <?php
            }
             ?>                
            </div>
        </div>
    </div>
    <!--  Sponsor  -->

<?php  
    return ob_get_clean();
}

//Gallery Classic
add_shortcode('dolomia_gallery', 'dolomia_gallery_func');
function dolomia_gallery_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_gallery_ids'   => '',

    ), $atts));
    ob_start();
?>
<div id="page-wrap" class="content-section fullpage-wrap">   
    <!--  Gallery Section  -->
    <section id="gallery" data-isotope="load-simple">
        <div class="masonry-items equal four-columns">
        <?php
            $show_post = array(
                'post_type' => 'trek',
                'posts_per_page'  => $dolomia_gallery_ids,
                );
             $all_post = new WP_Query($show_post);

            if( $all_post->have_posts() ) : while( $all_post->have_posts() ) : $all_post->the_post();

                $trek_gallery = get_post_meta( get_the_ID(),'trek_gallery', true );

                if (get_post_meta(get_the_ID(),'trek_gallery', true) == 'choose') { 
                    ?>
                    <!--  Page Trek  -->
                    <div class="one-item">
                        <div class="image-bg" style="background-image:url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>)"></div>
                        <div class="content figure">
                            <i class="pd-icon-distance"></i>
                            <a href="<?php the_permalink(); ?>" class="link"></a>
                        </div>
                    </div>
                    <!--  END Page Trek  -->
                <?php
                    // }
                }
                else{ ?>
                 <!--  Lightbox trek -->
                    <div class="one-item">
                        <div class="image-bg" style="background-image:url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>)"></div>
                        <div class="content figure">
                            <i class="pd-icon-camera"></i>
                            <a href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="link lightbox"></a>
                        </div>
                    </div>
                    <!--  END Lightbox trek -->
                <?php  
                }  
            endwhile; endif;
        ?>
        </div> 
    </section>
    <!--  END Gallery Section  -->
</div>
<?php  
    return ob_get_clean();
}

//Gallery filter
add_shortcode('dolomia_gallery_filter', 'dolomia_gallery_filter_func');
function dolomia_gallery_filter_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_gallery_filter_ids'   => '',
        'order_post'   => '',
        'order_by_post'   => '',

    ), $atts));
    ob_start();
?>

     <?php
    $show_post = array(
    'post_type' => 'trek',
    'posts_per_page'  => $dolomia_gallery_filter_ids,
    // 'taxonomies' => 'category',
    'order' => $order_post,
    'orderby' => $order_by_post,

    );

    $all_post = new WP_Query($show_post);
    $terms = get_terms( array( 
        'taxonomy' => 'category',
    ) );
    ?>
<div id="page-wrap" class="content-section fullpage-wrap">    
    <section id="masonry-filters" class="text">
        <!--  Filters  -->
        <div class="row margin-null">
            <div class="col-sm-12 padding-leftright-null text-center">
                <ul class="col-md-12 filters padding-leftright-null">
                    <li data-filter="*" class="is-checked">All</li>
                    <?php 
                        if ( ! empty( $terms ) ) {
                            foreach ($terms as $term) { 
                            echo '<li data-filter=".'.$term->slug.'">'.$term->name.'</li>';                               
                            }
                        }
                     ?>                    
                </ul>
            </div>

        </div>
        <!--  END Filters  -->
    </section>

    <!--  Gallery Section  -->
    <section id="gallery" data-isotope="load-simple">
        <div class="masonry-items equal three-columns">
            <?php
            if( $all_post->have_posts() ) : while( $all_post->have_posts() ) : $all_post->the_post();

            $categories = get_the_terms(get_the_ID(),'category' );
            $string='';

            foreach ((array)$categories as $cat_slugs) {
                $string .= $cat_slugs->slug." ";
            }

            $trek_gallery = get_post_meta( get_the_ID(),'trek_gallery', true );
                if (get_post_meta(get_the_ID(),'trek_gallery', true) == 'choose') {  ?>
                <!--  Page Trek  -->
                <div class="one-item <?php echo $string; ?> ">
                    <div class="image-bg" style="background-image:url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>)"></div>
                    <div class="content figure">
                        <i class="pd-icon-distance"></i>
                        <a href="<?php the_permalink(); ?>" class="link"></a>
                    </div>
                </div>
                    <!--  END Page Trek  -->
                <?php
                }
                else{ ?>
                 <!--  Lightbox trek -->
                    <div class="one-item <?php echo $string; ?>">
                        <div class="image-bg" style="background-image:url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>)"></div>
                        <div class="content figure">
                            <i class="pd-icon-camera"></i>
                            <a href="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" class="link lightbox"></a>
                        </div>
                    </div>
                    <!--  END Lightbox trek -->
                <?php  
                } 
        
   ?>    <?php 
    endwhile;
endif;
?>
        </div>
    </section>
    <!--  END Gallery Section  -->
</div>
<?php  
    return ob_get_clean();
}

//All treks
add_shortcode('dolomia_all_treks', 'dolomia_all_treks_func');
function dolomia_all_treks_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_all_treks_id'   => '',
        'dolomia_all_treks_order_by'   => '',
        'dolomia_all_treks_order'   => '',

    ), $atts));
    ob_start();

?>



<div class="container text">
    <?php
    $terms = get_terms( array( 
        'taxonomy' => 'category',
    ) );
    ?>
    <section id="masonry-filters" class="text">
        <!--  Filters  -->
        <div class="row margin-null">
            <div class="col-sm-12 padding-leftright-null text-center">
                <ul class="col-md-12 filters padding-leftright-null">
                    <li data-filter="*" class="is-checked">All</li>
                    <?php 
                        if ( ! empty( $terms ) ) {
                            foreach ($terms as $term) { 
                            echo '<li data-filter=".'.$term->slug.'">'.$term->name.'</li>';                               
                            }
                        }
                     ?>                    
                </ul>
            </div>

        </div>
        <!--  END Filters  -->
    </section>

    <!--  All treks  -->
    <section id="showcase-treks" class="page" data-isotope="load-simple">
        <div class="masonry-items equal four-columns">
            <!--  Single Trek  -->
            <?php
            $show_post = array(
            'post_type' => 'trek',
            'posts_per_page'  => $dolomia_all_treks_id,
            'taxonomies' => 'category',
            'order' => $dolomia_all_treks_order_by,
            'orderby' => $dolomia_all_treks_order,

            );

            $all_post = new WP_Query($show_post);

            if ($all_post->have_posts()) : while($all_post->have_posts()) : $all_post->the_post();

                $categories = get_the_terms(get_the_ID(),'category' );
                $string='';

                foreach ((array)$categories as $cat_slugs) {
                    $string .= $cat_slugs->slug." ";
                }

                $name_place = get_post_meta( get_the_ID(), 'trek_name_place', true );
                $country = get_post_meta( get_the_ID(), 'trek_country', true );
                $trek_datas = get_post_meta( get_the_ID(),'trek_data', true );
                $guide = get_post_meta( get_the_ID(),'trek_visitor', true );

            ?>
            <div class="item one-item <?php echo $string; ?>">
                <div class="showcase-trek">
                 <?php 
                    if(isset($trek_datas) && $trek_datas != ''){
                         $x = '';
                         $y = '';
                        foreach ($trek_datas as $entrys ) {
                           $x .= $entrys['trek_data2'].' ';
                           $y .= $entrys['trek_data1'].',';
                    }
                    $a = explode("|", $x);
                    $b = explode(",", $y);

                ?>
                	<?php if($a[0] !== "0"):?>
                    <span class="read">
                        <?php echo $a[0]; ?>
                    </span>
                    <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="">
                    <div class="content text-center">
                        <div class="row margin-leftright-null">
                            <div class="meta">
                                <div class="col-md-4 padding-leftright-null">
                                    <h3><?php echo $a[4]; ?></h3>
                                    <h4><?php echo $b[4]; ?></h4>
                                </div>
                                <div class="col-md-4 padding-leftright-null">
                                    <h3><?php echo $a[3]; ?></h3>
                                    <h4><?php echo $b[3]; ?></h4>
                                </div>
                                <div class="col-md-4 padding-leftright-null">
                                    <h3><?php echo $a[2]; ?></h3>
                                    <h4><?php echo $b[2]; ?></h4>
                                </div>
                            </div>
                            <div class="category">
                                <h3><?php the_title(); ?></h3>
                            </div>
                            <div class="info">
                            <!-- <?php 
                                 if(isset($trek_datas) && $trek_datas != ''){
                             ?> -->
                                <div class="col-md-12 padding-leftright-null">
                                    <h6 class="heading"><?php echo esc_attr($name_place); ?></h6>
                                    <p class="margin-null"><?php echo esc_attr($guide); ?></p>
                                </div>
                                <!-- <?php } ?> -->
                            </div>
                        </div>
                    </div>
                    <?php else:?>
                            <div class="trek-without-data" style='background-image: url("<?php echo wp_get_attachment_url(get_post_thumbnail_id())?>")'>
                            	<img src="<?php echo includes_url('images/transparent-460x250.png'); ?>" alt="">
                            	<div class="content text-center">
                               		<div class="row margin-leftright-null">
                                        <div class="meta">
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3><?php echo $a[4]; ?></h3>
                                                <h4><?php echo $b[4]; ?></h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3><?php echo $a[3]; ?></h3>
                                                <h4><?php echo $b[3]; ?></h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3><?php echo $a[2]; ?></h3>
                                                <h4><?php echo $b[2]; ?></h4>
                                            </div>
                                        </div>
                                        <div class="category">
                                            <h3><?php the_title(); ?></h3>
                                        </div>
                                        <div class="info">
                                        <!-- <?php 
                                             if(isset($trek_datas) && $trek_datas != ''){
                                         ?> -->
                                            <div class="col-md-12 padding-leftright-null">
                                                <h6 class="heading"><?php echo esc_attr($name_place); ?></h6>
                                                <p class="margin-null"><?php echo esc_attr($guide); ?></p>
                                            </div>
                                            <!-- <?php } ?> -->
                                        </div>
                                	</div>
                                </div>
                            </div>
                            <?php endif;?>
                    <a href="<?php the_permalink() ?>" class="link"></a>
                    <?php } ?>
                </div>
            </div>
            <!-- END Single Trip -->
            <?php endwhile; endif; ?>
            <!--  END Single Trek  -->
        </div>
    </section>
    <!--  END All treks  -->
</div>

<?php  
    return ob_get_clean();
}


//Services
add_shortcode('dolomia_service', 'dolomia_service_func');
function dolomia_service_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_service_icon'   => '',
        'dolomia_service_title'   => '',
        'dolomia_service_content'   => '',
        'dolomia_service_fa_icon' => '',

    ), $atts));
    ob_start();

?>

    <div class="padding-leftright-null">
        <div class="text padding-md-bottom-null">
        	<?php if($dolomia_service_fa_icon):?>
        		<i class="<?php echo esc_attr($dolomia_service_fa_icon); ?> service margin-bottom-null"></i>
        	<?php else: ?>
            	<i class="pd-icon-<?php echo esc_attr($dolomia_service_icon); ?> service margin-bottom-null"></i>
            <?php endif;?>
            <h6 class="heading margin-bottom-extrasmall"><?php echo esc_attr($dolomia_service_title); ?></h6>
            <p class="margin-bottom-null"><?php echo esc_attr($dolomia_service_content); ?></p>
        </div>
    </div>


<?php  
    return ob_get_clean();
}


//Team
add_shortcode('dolomia_team', 'dolomia_team_func');
function dolomia_team_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_team_avatar'   => '',
        'dolomia_team_social'   => '',
        'dolomia_team_name'   => '',
        'dolomia_team_add'   => '',
        'dolomia_team_url'  => '',

    ), $atts));
    ob_start();
   $dolomia_team_social = vc_param_group_parse_atts( $atts['dolomia_team_social'] );
    $dolomia_team_avatar = wp_get_attachment_image_src($dolomia_team_avatar,''); 
?>

    <div class="single-person">
        <div class="content">
            <a href="<?php echo $dolomia_team_url ?>"><img src="<?php echo esc_url($dolomia_team_avatar[0]);?>" alt="guide image"></a>
            <ul class="social">
            <?php foreach ($dolomia_team_social as $team_social) { ?>
                <li><a href="<?php echo esc_attr($team_social['dolomia_team_social_link']); ?>"><i class="icon ion-social-<?php echo esc_attr($team_social['dolomia_team_social_icon']); ?>"></i></a></li>
            <?php
            } ?>                
            </ul>
        </div>
        <div class="description text-center">
            <h5><a href="<?php echo $dolomia_team_url ?>"><?php echo esc_attr($dolomia_team_name) ?></a></h5>
            <h6><?php echo esc_attr($dolomia_team_add) ?></h6>
        </div>
    </div>

<?php  
    return ob_get_clean();
}

//Heading
add_shortcode('dolomia_heading', 'dolomia_heading_func');
function dolomia_heading_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_heading_title'   => '',
        'dolomia_heading_subtitle'   => '',

    ), $atts));
    ob_start();
  
?>
    <div class="col-md-12 padding-onlybottom-md text-center">
        <h2 class="margin-bottom-null title line center"><?php echo esc_attr($dolomia_heading_title) ?></h2>
        <p class="heading center grey margin-bottom-null"><?php echo esc_attr($dolomia_heading_subtitle) ?></p>
    </div>

        
<?php  
    return ob_get_clean();
}

//Project images
add_shortcode('dolomia_project_img', 'dolomia_project_img_func');
function dolomia_project_img_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_project_image'   => '',

    ), $atts));
    ob_start();
    $dolomia_project_image = vc_param_group_parse_atts( $atts['dolomia_project_image'] );
?>

<div class="row margin-null">
    <!--  Grid Images with Lightbox  -->
    <div class="project-images grid">
    <?php 
        foreach ($dolomia_project_image as $project_image) { ?>
            <div class="col-xs-6 col-sm-3 padding-leftright-null">
            <?php 
                $project_image1_src = wp_get_attachment_image_src($project_image['dolomia_project_image1'],'');
                $project_image2_src = wp_get_attachment_image_src($project_image['dolomia_project_image2'],'');

             ?>
                <a href="<?php echo esc_url($project_image1_src[0]); ?>" class="lightbox">
                    <div class="image" style="background-image:url(<?php echo esc_url($project_image2_src[0]); ?>)"></div>
                </a>
            </div>
        <?php
        }

     ?>        
    </div>
    <!--  END Grid Images with Lightbox  -->
</div>
<?php  
    return ob_get_clean();
}

//Service Responsive Icon
add_shortcode('dolomia_services_res', 'dolomia_services_res_func');
function dolomia_services_res_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_services_responsive'   => '',

    ), $atts));
    ob_start();
   $services_responsive = vc_param_group_parse_atts( $atts['dolomia_services_responsive'] );
   
?>
<!--  Services with Icon -->
<div class="service-responsive-bottom padding-md">
    <ul class="services">
    <?php 
        foreach ($services_responsive as $services_res) {?>
            <li>
                <div class="col-xs-2 padding-leftright-null">
                    <div class="icon">
                        <i class="pd-icon-<?php echo esc_attr($services_res['dolomia_services_res_icon']); ?>"></i>
                    </div>
                </div>
                <div class="col-xs-10 padding-leftright-null">
                    <div class="icon-text">
                        <h6 class="small"><?php echo esc_attr($services_res['dolomia_services_res_title']); ?></h6>
                        <p><?php echo esc_attr($services_res['dolomia_services_res_subtitle']); ?></p>
                    </div>
                </div>
            </li>
        <?php
        }

     ?>
    </ul>
</div>
<!--  END Services with Icon -->
<?php  
    return ob_get_clean();
}

//Simple Services
add_shortcode('dolomia_services_simple', 'dolomia_services_simple_func');
function dolomia_services_simple_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_services_simple_item'   => '',

    ), $atts));
    ob_start();
   $services_simple = vc_param_group_parse_atts( $atts['dolomia_services_simple_item'] );
   
?>

<!--  Simple Services -->
<div class="row padding-sm">
<?php 
    foreach ($services_simple as $services) { ?>
        <div class="col-md-6">
            <h6 class="small dark"><?php echo esc_attr($services['dolomia_services_simple_title']); ?></h6>
            <p><?php echo esc_attr($services['dolomia_services_simple_subtitle']); ?></p>
        </div>
    <?php
    }
 ?>    
</div>

<?php  
    return ob_get_clean();
}

//Material Services
add_shortcode('dolomia_services_material', 'dolomia_services_material_func');
function dolomia_services_material_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_services_simple_item'   => '',

    ), $atts));
    ob_start();
   $services_material_item = vc_param_group_parse_atts( $atts['dolomia_services_material_item'] );
   
?>

<!--  Material Services -->
<div class="row padding-sm">
    <div class="col-md-12 padding-leftright-null">
    <?php 
        foreach ($services_material_item as $material_item) { ?>
            <div class="col-md-6">
                <div class="margin-md-bottom">
                    <i class="pd-icon-<?php echo esc_attr($material_item['dolomia_services_material_item_icon']) ?> service material left"></i>
                    <div class="service-content">
                        <h6 class="heading  margin-bottom-extrasmall"><?php echo esc_attr($material_item['dolomia_services_material_item_title']) ?></h6>
                        <p class="margin-bottom-null"><?php echo esc_attr($material_item['dolomia_services_material_item_subtitle']) ?></p>
                    </div>
                </div>
            </div>
        <?php    
        }
     ?>
    </div>
</div>
<!--  END Material Services -->

<?php  
    return ob_get_clean();
}

//Standard Services
add_shortcode('dolomia_services_standard', 'dolomia_services_standard_func');
function dolomia_services_standard_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_services_standard_icon'   => '',
        'dolomia_services_standard_title'   => '',
        'dolomia_services_standard_subtitle'   => '',

    ), $atts));
    ob_start();
   
?>

    <div class="margin-md-bottom">
        <i class="icon ion-ios-<?php echo esc_attr($dolomia_services_standard_icon);?>-outline service"></i>
        <h6 class="heading margin-bottom-extrasmall"><?php echo esc_attr($dolomia_services_standard_title);?></h6>
        <p class="margin-bottom-null"><?php echo esc_attr($dolomia_services_standard_subtitle);?></p>
    </div>
<?php  
    return ob_get_clean();
}

//Tabs
add_shortcode('dolomia_tabs', 'dolomia_tabs_func');
function dolomia_tabs_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_tabs_tablist'   => '',

    ), $atts));
    ob_start();
   $dolomia_tabs_tablist = vc_param_group_parse_atts( $atts['dolomia_tabs_tablist'] );
?>
<!--  Accordion and Tabs  -->

<div class="row margin-null padding-md">
    <div class="col-md-12 padding-leftright-null">
        <div class="row padding-sm">
            <ul class="nav nav-tabs" role="tablist">
        <?php 
            foreach ($dolomia_tabs_tablist as $dolomia_tablist) { ?>
                <li role="presentation" class="<?php if($dolomia_tablist['tablist_active'] == 'active'){echo 'active'; } else{}; ?>" ><a href="#tab-<?php echo esc_attr($dolomia_tablist['dolomia_tablist_order']);?>" aria-controls="tab-<?php echo esc_attr($dolomia_tablist['dolomia_tablist_order']);?>" role="tab" data-toggle="tab"><?php echo esc_attr($dolomia_tablist['dolomia_tablist_name']);?></a>
                </li> 
            <?php
            }
            ?>
            
            </ul>
            <!--  Nav Tabs  -->
            <!-- Tab panes -->
            <div class="tab-content">
            <?php
                foreach ($dolomia_tabs_tablist as $dolomia_tablist) { ?>
                <div role="tabpanel" class="tab-pane <?php if($dolomia_tablist['tablist_active'] == 'active'){echo "active" ;} else{}; ?> >" id="tab-<?php echo esc_attr($dolomia_tablist['dolomia_tablist_order']);?>"><?php echo esc_attr($dolomia_tablist['dolomia_tablist_content']);?>               
                </div>
                <?php
                }
               ?>
            </div>
        </div>
    </div>       
</div>

<?php  
    return ob_get_clean();
}

// ============ Home Classic ===========
// Section About
add_shortcode('dolomia_section_about', 'dolomia_section_about_func');
function dolomia_section_about_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_section_about_title'   			=> '',
        'dolomia_section_about_sub'      		=> '',
        'dolomia_section_about_content'     	=> '',
    ), $atts));
    ob_start();
?>
<!-- Section About -->
<div class="row margin-leftright-null">
    <div class="about_us_container">
        <div class="col-md-12 padding-leftright-null">
            <div class="text text-center">
                <h2 class="margin-bottom-null title line center"><?php echo esc_attr($dolomia_section_about_title); ?></h2>
                <p class="heading center grey z-index"><?php echo esc_attr($dolomia_section_about_sub); ?></p>
                <div class="padding-onlytop-md">
                    <p><?php echo $dolomia_section_about_content; ?></p>                               
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Section About -->
<?php  
    return ob_get_clean();
}


//Carousel Gallery
add_shortcode('dolomia_carousel', 'dolomia_carousel_func');
function dolomia_carousel_func($atts, $content = null){
    extract(shortcode_atts(array(
        'dolomia_carousel_img' => '',
    ), $atts));
    ob_start();
    $carousel_id = explode(",",$dolomia_carousel_img);
    
?>

<!-- Carousel Gallery -->
<div class="row margin-leftright-null padding-sm">
    <div class="gallery-carousel">
    <?php foreach ($carousel_id as $carousel_ids) { 
        $carousel_ids_src = wp_get_attachment_image_src($carousel_ids,'');
        ?>
        <div class="item">
            <div class="image" style="background-image:url(<?php echo esc_url($carousel_ids_src[0]); ?>)"></div>
        </div>
    <?php
    } 
    ?>     
    </div>
</div>
<!-- Carousel Gallery -->

<?php  
    return ob_get_clean();
}

// ============End Home Classic ===========



