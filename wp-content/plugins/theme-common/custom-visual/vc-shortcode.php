<?php 

$pre_text = 'VG ';

//Home showcase 
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Home Showcase ", 'dolomia'),
    "base"     => "dolomia_home_showcase",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        // main section
        array(
          'type' => 'attach_image',
          'value' => '',
          'heading' => 'Upload image',
          'param_name' => 'dolomia_showcase_img',
          'description' => esc_html__( 'Select images from media library.', 'dolomia' ),
        ), 

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Title", 'dolomia'),
            "param_name"  => "dolomia_home_showcase_title",
            "value"       => "",
            "description" => esc_html__("Ex: Dolomia", 'dolomia')
        ), 

        array(
            "type"        => "textarea_html",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Heading", 'dolomia'),
            "param_name"  => "dolomia_home_showcase_heading",
            "value"       => "",
            "description" => esc_html__("Ex: Outdoor, Hiking &amp; Adventures Template", 'dolomia')
        ),

        array(
            "type"        => "textarea",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Content", 'dolomia'),
            "param_name"  => "dolomia_home_showcase_content",
            "value"       => "",
            "description" => esc_html__("Ex: The best template for outdoor business and activity. Ease to use, flexible. All that you need! 4 Home pages, 20+ components, infinite possibilities. We are puredesignThemes", 'dolomia')
        ),

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Button", 'dolomia'),
            "param_name"  => "dolomia_home_showcase_btn",
            "value"       => "",
            "description" => esc_html__("Ex: Buy now", 'dolomia')
        ), 

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Button link", 'dolomia'),
            "param_name"  => "dolomia_home_showcase_btn_link",
            "value"       => "",
            "description" => esc_html__("Ex: #", 'dolomia')
        ),

        //section project
        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_home_showcase_project',
            'params' => array(  
                 
                array(
                  'type' => 'attach_image',
                  'value' => '',
                  'heading' => 'Upload image',
                  'param_name' => 'dolomia_home_showcase_img',
                  'description' => esc_html__( 'Select images from media library.', 'dolomia' ),
                ), 

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Title of section project", 'dolomia'),
                    "param_name"  => "dolomia_home_showcase_title_pro",
                    "value"       => "",
                    "description" => esc_html__("Ex: Adventure", 'dolomia')
                ), 

                array(
                    "type"        => "textarea_html",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Heading of section project", 'dolomia'),
                    "param_name"  => "dolomia_home_showcase_heading_pro",
                    "value"       => "",
                    "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit", 'dolomia')
                ),

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Button of section project", 'dolomia'),
                    "param_name"  => "dolomia_home_showcase_btn_pro",
                    "value"       => "",
                    "description" => esc_html__("Ex: View", 'dolomia')
                ),

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Button link", 'dolomia'),
                    "param_name"  => "dolomia_home_showcase_link_pro",
                    "value"       => "",
                    "description" => esc_html__("Ex: #", 'dolomia')
                ),                                         
            )
        ),

    )));
} 

// Home Showcase Footer
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Home Showcase Footer ", 'dolomia'),
    "base"     => "dolomia_home_showcase_footer",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    // "params"   => array(
        
    //     array(
    //       'type' => 'attach_image',
    //       'value' => '',
    //       'heading' => 'Upload image',
    //       'param_name' => 'dolomia_showcase_img',
    //       'description' => esc_html__( 'Select images from media library.', 'dolomia' ),
    //     ), 

    ));
} 


//Home Carousel Slider
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Home Carousel Slider", 'dolomia'),
    "base"     => "dolomia_carousel_slider",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_carousel_slider_item',
            'params' => array(  
                 
                array(
                  'type' => 'attach_image',
                  'value' => '',
                  'heading' => 'Upload image',
                  'param_name' => 'dolomia_carousel_slider_img',
                  'description' => esc_html__( 'Select images from media library.', 'dolomia' ),
                ), 

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Title", 'dolomia'),
                    "param_name"  => "dolomia_carousel_slider_title",
                    "value"       => "",
                    "description" => esc_html__("Ex: Hiking", 'dolomia')
                ), 

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Heading", 'dolomia'),
                    "param_name"  => "dolomia_carousel_slider_heading",
                    "value"       => "",
                    "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit", 'dolomia')
                ),

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Button", 'dolomia'),
                    "param_name"  => "dolomia_carousel_slider_btn",
                    "value"       => "",
                    "description" => esc_html__("Ex: More info", 'dolomia')
                ), 

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Link", 'dolomia'),
                    "param_name"  => "dolomia_carousel_slider_link",
                    "value"       => "",
                    "description" => esc_html__("Ex: #", 'dolomia')
                ),                                          
            )
        ),

    )));
} 

//Trip Showcase
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Trip Showcase Carousel", 'dolomia'),
    "base"     => "dolomia_showcase",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Title", 'dolomia'),
            "param_name"  => "dolomia_showcase_title",
            "value"       => "",
            "description" => esc_html__("Ex: Featured trips", 'dolomia')
        ),
        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Subtitle", 'dolomia'),
            "param_name"  => "dolomia_showcase_subtitle",
            "value"       => "",
            "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit", 'dolomia')
        ),
        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Enter the trek post's ID that want to show", 'dolomia'),
            "param_name"  => "dolomia_showcase_id",
            "value"       => "",
            "description" => esc_html__("Ex: 397, 114, 291, 109,...", 'dolomia')
        ),

    )));
} 

//Gallery Classic
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Gallery Classic", 'dolomia'),
    "base"     => "dolomia_gallery",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Select number post that want to show in Gallery", 'dolomia'),
            "param_name"  => "dolomia_gallery_ids",
            "value"       => "",
            "description" => esc_html__("Please select the number post in single trek type", 'dolomia')
        ),

    )));
} 


//Gallery Filters
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Gallery Filters", 'dolomia'),
    "base"     => "dolomia_gallery_filter",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Select number post that want to show in Gallery Filters", 'dolomia'),
            "param_name"  => "dolomia_gallery_filter_ids",
            "value"       => "",
            "description" => esc_html__("Please select the number post to display", 'dolomia')
        ),

        array(
             'type' => 'dropdown',
             'heading' => __( 'Sort Order by', 'dolomia' ),
             'param_name' => 'order_by_post',
             'value' => array(
                esc_html__( 'None  ', 'js_composer' ) => '',
                esc_html__( 'Random : Random order.', 'js_composer' ) => 'rand',
                esc_html__( 'Title : Order by title', 'js_composer' ) => 'title',
                esc_html__( 'Name: Post name', 'js_composer' ) => 'name',
                esc_html__( 'Author : Order by author', 'js_composer' ) => 'author',
                esc_html__( 'Date: Order by date', 'js_composer' ) => 'date',
             ),
             'description' => esc_html__( 'Select field do you want Order by.', 'dolomia' ),
        ),

        array(
             'type' => 'dropdown',
             'heading' => __( 'Sort Order', 'dolomia' ),
             'param_name' => 'order_post',
             'value' => array(
                esc_html__( 'None  ', 'js_composer' ) => '',
                esc_html__( 'ASC : lowest to highest', 'js_composer' ) => 'ASC',
                esc_html__( 'DESC : highest to lowest', 'js_composer' ) => 'DESC',
             ),
             'description' => esc_html__( 'Select field do you want to order.', 'dolomia' ),
        ),


    )));
} 

//All treks
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."All Treks", 'dolomia'),
    "base"     => "dolomia_all_treks",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Select number post that want to show in Gallery Filters", 'dolomia'),
            "param_name"  => "dolomia_all_treks_id",
            "value"       => "",
            "description" => esc_html__("Please select the number post to display", 'dolomia')
        ),

        array(
             'type' => 'dropdown',
             'heading' => __( 'Sort Order by', 'dolomia' ),
             'param_name' => 'dolomia_all_treks_order_by',
             'value' => array(
                esc_html__( 'None  ', 'js_composer' ) => '',
                esc_html__( 'Random : Random order.', 'js_composer' ) => 'rand',
                esc_html__( 'Title : Order by title', 'js_composer' ) => 'title',
                esc_html__( 'Name: Post name', 'js_composer' ) => 'name',
                esc_html__( 'Author : Order by author', 'js_composer' ) => 'author',
                esc_html__( 'Date: Order by date', 'js_composer' ) => 'date',
             ),
             'description' => esc_html__( 'Select field do you want Order by.', 'dolomia' ),
        ),

        array(
             'type' => 'dropdown',
             'heading' => __( 'Sort Order', 'dolomia' ),
             'param_name' => 'dolomia_all_treks_order',
             'value' => array(
                esc_html__( 'None  ', 'js_composer' ) => '',
                esc_html__( 'ASC : lowest to highest', 'js_composer' ) => 'ASC',
                esc_html__( 'DESC : highest to lowest', 'js_composer' ) => 'DESC',
             ),
             'description' => esc_html__( 'Select field do you want to order.', 'dolomia' ),
        ),


    )));
} 

// ========Single treks========


//row margin-leftright-null
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text." Content trek", 'dolomia'),
    "base"     => "dolomia_content_trek",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Heading", 'dolomia'),
            "param_name"  => "dolomia_content_trek_heading",
            "value"       => "",
            "description" => esc_html__("Ex: More Info", 'dolomia')
        ),

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Short description", 'dolomia'),
            "param_name"  => "dolomia_content_trek_description",
            "value"       => "",
            "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit", 'dolomia')
        ),
        array(
            "type"        => "textarea_html",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Enter the content", 'dolomia'),
            "param_name"  => "dolomia_content_trek_content",
            "value"       => "",
        ),

        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_content_trek_table_responsive',
            'params' => array(
                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Content 1", 'dolomia'),
                    "param_name"  => "dolomia_content_trek_content_1",
                    "value"       => "",
                    "description" => esc_html__("Ex: Partecipans", 'dolomia')
                ),

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Content 2", 'dolomia'),
                    "param_name"  => "dolomia_content_trek_content_2",
                    "value"       => "",
                    "description" => esc_html__("Ex: Start date", 'dolomia')
                ),

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Content 3", 'dolomia'),
                    "param_name"  => "dolomia_content_trek_content_3",
                    "value"       => "",
                    "description" => esc_html__("Ex: Duration", 'dolomia')
                ),

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Content 4", 'dolomia'),
                    "param_name"  => "dolomia_content_trek_content_4",
                    "value"       => "",
                    "description" => esc_html__("Ex: Price", 'dolomia')
                ),
                              
            )
        ), 

    )));
} 

//row margin-leftright-null trek-map
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Mapping trek", 'dolomia'),
    "base"     => "dolomia_trek_map",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            "type"        => "textarea_html",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Mapping", 'dolomia'),
            "param_name"  => "dolomia_trek_map_frame",
            "value"       => "",
            "description" => esc_html__("Use google map to find the place. After search the place, let click at share button and select the mapping. Please copy the link of mapping and paste in textfield", 'dolomia')
        ),

    )));
} 

//Share Button
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Share Button", 'dolomia'),
    "base"     => "dolomia_share_btn",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_share_btn_share',
            'params' => array(
                array(
                 'type' => 'iconpicker',
                 'heading' => esc_html__( 'Icon', 'dolomia' ),
                 "holder"      => "div",
                 'param_name' => 'dolomia_share_btn_share_icon',
                 
                 "value"       => "",
                 "description" => esc_html__("Select icon from library.", 'dolomia'),
              ),   
                 
                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Name of Icon", 'dolomia'),
                    "param_name"  => "dolomia_share_btn_share_name_icon",
                    "value"       => "",
                    "description" => esc_html__("Ex: google-plus. You can see above", 'dolomia')
                ),
                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Link", 'dolomia'),
                    "param_name"  => "dolomia_share_btn_share_link",
                    "value"       => "",
                    "description" => esc_html__("Ex: #", 'dolomia')
                ),
                              
            )
            ),

    )));
} 

//row text margin-leftright-null color-background
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Contact Trek", 'dolomia'),
    "base"     => "dolomia_trek_contact",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Heading of contact", 'dolomia'),
            "param_name"  => "dolomia_trek_contact_heading",
            "value"       => "",
            "description" => esc_html__("Ex: Would you like more info about this trip?", 'dolomia')
        ),
        array(
                "type"        => "textfield",
                "holder"      => "div",
                "class"       => "",
                "heading"     => esc_html__("Description for contact form", 'dolomia'),
                "param_name"  => "dolomia_trek_contact_des",
                "value"       => "",
                "description" => esc_html__("Ex: Call us at", 'dolomia')
            ),

        array(
                "type"        => "textfield",
                "holder"      => "div",
                "class"       => "",
                "heading"     => esc_html__("Method to contact", 'dolomia'),
                "param_name"  => "dolomia_trek_contact_method",
                "value"       => "",
                "description" => esc_html__("Ex: +40 45615654", 'dolomia')
            ),

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Button of contact more", 'dolomia'),
            "param_name"  => "dolomia_trek_contact_button",
            "value"       => "",
            "description" => esc_html__("Ex: Contact us", 'dolomia')
        ),

    )));
} 

//row margin-leftright-null
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Gallery Trek", 'dolomia'),
    "base"     => "dolomia_trek_gallery",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Heading of contact", 'dolomia'),
            "param_name"  => "dolomia_trek_gallery_heading",
            "value"       => "",
            "description" => esc_html__("Ex: Gallery", 'dolomia')
        ),
        array(
                "type"        => "textfield",
                "holder"      => "div",
                "class"       => "",
                "heading"     => esc_html__("Description for contact form", 'dolomia'),
                "param_name"  => "dolomia_trek_gallery_des",
                "value"       => "",
                "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit", 'dolomia')
            ),

        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_trek_gallery_item',
            'params' => array(  
                 
                 array(
                  'type' => 'attach_image',
                  'value' => '',
                  'heading' => 'Upload image',
                  'param_name' => 'dolomia_trek_gallery_main_img',
                  'description' => esc_html__( 'Select images from media library.', 'dolomia' ),
                ),
                array(
                  'type' => 'attach_image',
                  'value' => '',
                  'heading' => 'Upload lightbox image',
                  'param_name' => 'dolomia_trek_gallery_sub_img',
                  'description' => esc_html__( 'When you click an above image, a different image will be display', 'dolomia' ),
                ),                                              
             )
        ),
                       
    )));
} 

//Main text
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Trek Main Content", 'dolomia'),
    "base"     => "dolomia_trek_main",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            "type"        => "textarea_html",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Main content pf the post", 'dolomia'),
            "param_name"  => "dolomia_trek_main_content",
            "value"       => "",
            "description" => esc_html__("", 'dolomia'),
        ),
    )));

    

} 


//Similar Trips 
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Similar Trips ", 'dolomia'),
    "base"     => "dolomia_trek_similar",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Title", 'dolomia'),
            "param_name"  => "dolomia_similar_trips_title",
            "value"       => "",
            "description" => esc_html__("Ex: Similar Trips", 'dolomia'),
        ),

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Subtitle", 'dolomia'),
            "param_name"  => "dolomia_similar_trips_subtitle",
            "value"       => "",
            "description" => esc_html__("Ex:Lorem ipsum dolor sit amet, consectetur adipisicing elit", 'dolomia'),
        ),

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("ID of the post that want to show", 'dolomia'),
            "param_name"  => "dolomia_similar_trips_id",
            "value"       => "",
            "description" => esc_html__("Ex: 114,109,...", 'dolomia'),
        ),

    )));
}

//======== End Single treks========

//======== About me =======

//Left content
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Left section", 'dolomia'),
    "base"     => "dolomia_left_section",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Title", 'dolomia'),
            "param_name"  => "dolomia_left_section_title",
            "value"       => "",
            "description" => esc_html__("Ex: Wild Person", 'dolomia'),
        ),

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Subtitle", 'dolomia'),
            "param_name"  => "dolomia_left_section_subtitle",
            "value"       => "",
            "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit", 'dolomia'),
        ),

        array(
            "type"        => "textarea",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Content", 'dolomia'),
            "param_name"  => "dolomia_left_section_content",
            "value"       => "",
            "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero architecto, nesciunt, modi nam reprehenderit eligendi impedit perferendis consequuntur doloribus aliquid officiis ipsa id asperiores eius magnam ut ducimus ipsum dolor nihil molestias commodi quasi sapiente qui explicabo. Soluta, eum, quia.", 'dolomia'),
        ),
        array(
            "type"        => "textarea",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Sub Content", 'dolomia'),
            "param_name"  => "dolomia_left_subsection_content",
            "value"       => "",
            "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero architecto, nesciunt, modi nam reprehenderit eligendi impedit perferendis consequuntur doloribus aliquid officiis ipsa id asperiores eius magnam ut ducimus ipsum dolor nihil molestias commodi quasi sapiente qui explicabo. Soluta, eum, quia.", 'dolomia'),
        ),
    )));
} 

//Skills
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Skills", 'dolomia'),
    "base"     => "dolomia_skill",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_skill_list',
            'params' => array(  
                 
                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Topic ", 'dolomia'),
                    "param_name"  => "dolomia_skill_list_item",
                    "value"       => "",
                    "description" => esc_html__("Ex: Paragliding", 'dolomia')
                ),
                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Percent", 'dolomia'),
                    "param_name"  => "dolomia_skill_list_percent",
                    "value"       => "",
                    "description" => esc_html__("Ex: 25", 'dolomia')
                ),
                              
             )
        ),
    )));
} 


//Testimonials
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Testimonials", 'dolomia'),
    "base"     => "dolomia_about_me_slider",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            'type'        => 'attach_image',
            'heading'     => esc_html__( 'Images background', 'dolomia' ),
            'param_name'  => 'dolomia_about_me_slider_bg',
            'value'       => '',
            'description' => esc_html__( 'Select images from media library.', 'dolomia' )
        ),

        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_about_me_slider_item',
            'params' => array(  
                 
                array(
                    "type"        => "textarea",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Content", 'dolomia'),
                    "param_name"  => "dolomia_about_me_slider_content",
                    "value"       => "",
                    "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur voluptatum fugiat molestias, veritatis perspiciatis laborum modi beatae placeat explicabo at laudantium aliquam, nam vero ut!", 'dolomia')
                ),

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Author", 'dolomia'),
                    "param_name"  => "dolomia_about_me_slider_author",
                    "value"       => "",
                    "description" => esc_html__("Ex: John Doe", 'dolomia')
                ),
                              
             )
        ),      

    )));
} 

//Lastest news
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Lastest News", 'dolomia'),
    "base"     => "dolomia_lastest",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Title', 'dolomia' ),
            'param_name'  => 'dolomia_lastest_title',
            'value'       => '',
            'description' => esc_html__( 'The title of the Lastest News', 'dolomia' )
            ), 

        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Subtitle', 'dolomia' ),
            'param_name'  => 'dolomia_lastest_subtitle',
            'value'       => '',
            'description' => esc_html__( 'The subtitle of the Lastest News', 'dolomia' )
            ),

        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Enter ID', 'dolomia' ),
            'param_name'  => 'dolomia_lastest_id',
            'value'       => '',
            'description' => esc_html__( 'Enter ID of the post that want to show', 'dolomia' )
            ),
        ),      

    ));
} 

//Sponsor
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Sponsor", 'dolomia'),
    "base"     => "dolomia_sponsor",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            'type'        => 'attach_images',
            'heading'     => esc_html__( 'Sponsor Images', 'dolomia' ),
            'param_name'  => 'dolomia_sponsor_imgs',
            'value'       => '',
            'description' => esc_html__( 'Select images from media library.', 'dolomia' )
            ),
        ),      

    ));
} 
//======== End about me ====

// Contact us
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text." Contact Us", 'dolomia'),
    "base"     => "dolomia_contact",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Contact Title.", 'dolomia'),
            "param_name"  => "dolomia_contact_title",
            "value"       => "",
            "description" => esc_html__("Ex: Get in touch Person", 'dolomia')
        ),

        array(
            "type"        => "textarea",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Contact Subtitle.", 'dolomia'),
            "param_name"  => "dolomia_contact_subtitle",
            "value"       => "",
            "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit", 'dolomia')
        ),

        array(
            "type"        => "textarea",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Contact Content.", 'dolomia'),
            "param_name"  => "dolomia_contact_content",
            "value"       => "",
            "description" => esc_html__("Ex:Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem harum aspernatur sapiente error, voluptas fuga, laudantium ullam magni fugit. Qui!", 'dolomia')
        ),

        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_contact_item',
            'description' => esc_html__("Personal information", 'dolomia'),
            'params' => array(  
                 
                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Contact By", 'dolomia'),
                    "param_name"  => "dolomia_contact_by",
                    "value"       => "",
                    "description" => esc_html__("Ex: Email, Address,...", 'dolomia')
                ),

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Contact ", 'dolomia'),
                    "param_name"  => "dolomia_contact_type",
                    "value"       => "",
                    "description" => esc_html__("Ex: 322 Moon St, Venice
                                                Italy, 1231", 'dolomia')
                ),  

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Contact Link", 'dolomia'),
                    "param_name"  => "dolomia_contact_link",
                    "value"       => "",
                    "description" => esc_html__("Ex: #", 'dolomia')
                ),                                                
            )
        ),

        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_contact_day',
            'description' => esc_html__("Time working", 'dolomia'),
            'params' => array(  

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Day", 'dolomia'),
                    "param_name"  => "dolomia_contact_day1",
                    "value"       => "",
                    "description" => esc_html__("Ex: Monday to Friday", 'dolomia')
                ),
                 
                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Time", 'dolomia'),
                    "param_name"  => "dolomia_contact_time",
                    "value"       => "",
                    "description" => esc_html__("Ex: 9.00 am to 12.00 pm", 'dolomia')
                ),                                                  
            )
        ),
        
    )));
}

//Map
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Map", 'dolomia'),
    "base"     => "dolomia_map",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        
        array(
            'type'        => 'textfield',
            'heading'     => esc_html__( 'Latitude', 'dolomia' ),
            'param_name'  => 'dolomia_map_lat',
            'value'       => '',
            'description' => esc_html__( 'Please visit the website for more information: http://www.latlong.net/', 'dolomia' )
        ),

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Longitude", 'dolomia'),
            "param_name"  => "dolomia_map_lon",
            "value"       => "",
            "description" => esc_html__("Please visit the website for more information: http://www.latlong.net/", 'dolomia')
        ),
        
        
    )));
} 

//Project images 
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Project Images", 'dolomia'),
    "base"     => "dolomia_project_img",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_project_image',
            'params' => array(
                array(
                    'type'        => 'attach_image',
                    'heading'     => esc_html__( 'Lightbox Image', 'dolomia' ),
                    'param_name'  => 'dolomia_project_image1',
                    'value'       => '',
                    'description' => esc_html__( 'Select images from media library.', 'dolomia' )
                ),
                 
                array(
                    'type'        => 'attach_image',
                    'heading'     => esc_html__( 'Background Image', 'dolomia' ),
                    'param_name'  => 'dolomia_project_image2',
                    'value'       => '',
                    'description' => esc_html__( 'Select images from media library.', 'dolomia' )
                ),
                              
                )
            ),
        
    )));
} 

//Elements section 1
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Elements Section 1", 'dolomia'),
    "base"     => "dolomia_elements_section1",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            "type"        => "textarea_html",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Content", 'dolomia'),
            "param_name"  => "dolomia_elements_section1_content",
            "value"       => "",
            "description" => esc_html__("Enter the content that you want to show", 'dolomia')
        ),
        array(
            'type' => 'param_group',
            "heading" => esc_html__("Small Heading", 'dolomia'),
            'value' => '',
            'param_name' => 'dolomia_elements_section1_heading_small',
            'params' => array(
                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Heading", 'dolomia'),
                    "param_name"  => "dolomia_heading_small_title",
                    "value"       => "",
                    "description" => esc_html__("Ex: Heading 2 small", 'dolomia')
                ),
                 
                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Link", 'dolomia'),
                    "param_name"  => "dolomia_heading_small_link",
                    "value"       => "",
                    "description" => esc_html__("Ex: #", 'dolomia')
                ),
                              
                )
        ),

         array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Small Heading 2", 'dolomia'),
            "param_name"  => "dolomia_heading_small_2",
            "value"       => "",
            "description" => esc_html__("Ex: Heading 3 small", 'dolomia')
        ),

        array(
            'type' => 'param_group',
            "heading" => esc_html__("Style", 'dolomia'),
            'value' => '',
            'param_name' => 'dolomia_elements_section1_span',
            'params' => array(
                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Enter the color that want to show", 'dolomia'),
                    "param_name"  => "dolomia_elements_section1_span_color",
                    "value"       => "",
                    "description" => esc_html__("Ex: black", 'dolomia')
                ),                
                              
                )
            ),
        
    )));
} 

//Elements section 2
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Elements Section 2", 'dolomia'),
    "base"     => "dolomia_elements_section2",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        
        array(
            'type' => 'param_group',
            "heading" => esc_html__("Button Style", 'dolomia'),
            'value' => '',
            'param_name' => 'dolomia_elements_section2_button',
            'params' => array(
                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Button Link", 'dolomia'),
                    "param_name"  => "dolomia_elements_section2_button_link",
                    "value"       => "",
                    "description" => esc_html__("Ex: #", 'dolomia')
                ),                
                   
                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Class", 'dolomia'),
                    "param_name"  => "dolomia_elements_section2_button_class",
                    "value"       => "",
                    "description" => esc_html__("Easy to understanding that this Class helps show with different ways. Select a one. Ex: btn, btn-alt, btn-alt medium, btn-alt medium active, btn-pro, btn-alt small, btn-alt small active, btn-simple active, ...", 'dolomia')
                ),  

                array(
                    "type"        => "textfield",
                    "holder"      => "div",
                    "class"       => "",
                    "heading"     => esc_html__("Enter the color that want to show", 'dolomia'),
                    "param_name"  => "dolomia_elements_section2_button_name",
                    "value"       => "",
                    "description" => esc_html__("Ex: Button", 'dolomia')
                ),            
                )
            ),
        
    )));
}   

//Service Responsive Icon
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Service Responsive", 'dolomia'),
    "base"     => "dolomia_services_res",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        
        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_services_responsive',
            'params' => array(
                        array(
                            "type"        => "textfield",
                            "holder"      => "div",
                            "class"       => "",
                            "heading"     => esc_html__("Icon", 'dolomia'),
                            "param_name"  => "dolomia_services_res_icon",
                            "value"       => "",
                            "description" => esc_html__("Get class icon in http://fontawesome.io/icons/", 'dolomia')
                        ),

                        array(
                            "type"        => "textfield",
                            "holder"      => "div",
                            "class"       => "",
                            "heading"     => esc_html__("Title.", 'dolomia'),
                            "param_name"  => "dolomia_services_res_title",
                            "value"       => "",
                            "description" => esc_html__("Title on the service.", 'dolomia')
                        ),
                        array(
                            "type"        => "textarea",
                            "holder"      => "div",
                            "class"       => "",
                            "heading"     => esc_html__("Subtitle.", 'dolomia'),
                            "param_name"  => "dolomia_services_res_subtitle",
                            "value"       => "",
                            "description" => esc_html__("SubTitle on the service", 'dolomia')
                        ),
                    )
            ),
        
    )));
}

//Simple Services
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Simple Services", 'dolomia'),
    "base"     => "dolomia_services_simple",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_services_simple_item',
            'params' => array(
                    array(
                        "type"        => "textfield",
                        "holder"      => "div",
                        "class"       => "",
                        "heading"     => esc_html__("Title.", 'dolomia'),
                        "param_name"  => "dolomia_services_simple_title",
                        "value"       => "",
                        "description" => esc_html__("Ex: Professional Team", 'dolomia')
                    ),

                    array(
                        "type"        => "textarea",
                        "holder"      => "div",
                        "class"       => "",
                        "heading"     => esc_html__("Subtitle.", 'dolomia'),
                        "param_name"  => "dolomia_services_simple_subtitle",
                        "value"       => "",
                        "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem harum aspernatur sapiente error, voluptas fuga, laudantium ullam magni fugit. Qui!", 'dolomia')
                    ),
            )
        ),
    )));
} 

//Material Services
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Material Services", 'dolomia'),
    "base"     => "dolomia_services_material",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_services_material_item',
            'params' => array(
                    array(
                            "type"        => "textfield",
                            "holder"      => "div",
                            "class"       => "",
                            "heading"     => esc_html__("Icon", 'dolomia'),
                            "param_name"  => "dolomia_services_material_item_icon",
                            "value"       => "",
                            "description" => esc_html__("Get class icon in http://fontawesome.io/icons/", 'dolomia')
                        ),

                    array(
                        "type"        => "textfield",
                        "holder"      => "div",
                        "class"       => "",
                        "heading"     => esc_html__("Title.", 'dolomia'),
                        "param_name"  => "dolomia_services_material_item_title",
                        "value"       => "",
                        "description" => esc_html__("Ex: Business Goals", 'dolomia')
                    ),

                    array(
                        "type"        => "textarea",
                        "holder"      => "div",
                        "class"       => "",
                        "heading"     => esc_html__("Subtitle.", 'dolomia'),
                        "param_name"  => "dolomia_services_material_item_subtitle",
                        "value"       => "",
                        "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem harum aspernatur sapiente error, voluptas fuga, laudantium ullam magni fugit. Qui!", 'dolomia')
                    ),
            )
        ),
    )));
} 

//Standard Services
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Standard Services", 'dolomia'),
    "base"     => "dolomia_services_standard",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
                "type"        => "textfield",
                "holder"      => "div",
                "class"       => "",
                "heading"     => esc_html__("Icon", 'dolomia'),
                "param_name"  => "dolomia_services_standard_icon",
                "value"       => "",
                "description" => esc_html__("Get class icon in http://fontawesome.io/icons/", 'dolomia')
            ),

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Title.", 'dolomia'),
            "param_name"  => "dolomia_services_standard_title",
            "value"       => "",
            "description" => esc_html__("Ex: Mission", 'dolomia')
        ),

        array(
            "type"        => "textarea",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Subtitle.", 'dolomia'),
            "param_name"  => "dolomia_services_standard_subtitle",
            "value"       => "",
            "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem harum aspernatur sapiente error, voluptas fuga, laudantium ullam magni fugit. Qui!", 'dolomia')
        ),
        
    )));
}

//Tabs
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Tabs", 'dolomia'),
    "base"     => "dolomia_tabs",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            'type' => 'param_group',
            'value' => '',
            'param_name' => 'dolomia_tabs_tablist',
            'params' => array(
                    array(
                         'type' => 'dropdown',
                         'heading' => __( 'Seclect active', 'js_composer' ),
                         'param_name' => 'tablist_active',
                         'value' => array(
                         esc_html__( 'None  ', 'js_composer' ) => 'none',
                            esc_html__( 'Active', 'js_composer' ) => 'active',
                         ),
                         'description' => esc_html__( 'Seclect active', 'js_composer' ),
                    ),

                    array(
                        "type"        => "textfield",
                        "holder"      => "div",
                        "class"       => "",
                        "heading"     => esc_html__("Tab name", 'dolomia'),
                        "param_name"  => "dolomia_tablist_name",
                        "value"       => "",
                        "description" => esc_html__("Ex:Tab One", 'dolomia')
                    ),

                    array(
                        "type"        => "textfield",
                        "holder"      => "div",
                        "class"       => "",
                        "heading"     => esc_html__("Tab order", 'dolomia'),
                        "param_name"  => "dolomia_tablist_order",
                        "value"       => "",
                        "description" => esc_html__("Ex: one, two, three,...", 'dolomia')
                    ),

                    array(
                        "type"        => "textarea",
                        "holder"      => "div",
                        "class"       => "",
                        "heading"     => esc_html__("Tab content", 'dolomia'),
                        "param_name"  => "dolomia_tablist_content",
                        "value"       => "",
                        "description" => esc_html__("Ex: Content one. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias dolorem sequi qui odit accusantium tempore debitis, est mollitia dolores voluptatum eos ea fugit quis rerum expedita non beatae nostrum. Eos.", 'dolomia')
                    ),


            )
        ),       
        
    )));
}

//Header home
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text." Header home", 'dolomia'),
    "base"     => "dolomia_header_home",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        
        array(
            'type'        => 'attach_image',
            'heading'     => esc_html__( 'Header Images background', 'dolomia' ),
            'param_name'  => 'dolomia_header_image',
            'value'       => '',
            'description' => esc_html__( 'Select images from media library.', 'dolomia' )
        ),

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Header Title.", 'dolomia'),
            "param_name"  => "dolomia_header_title",
            "value"       => "",
            "description" => esc_html__("Title on the header.", 'dolomia')
        ),
        array(
            "type"        => "textarea",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Header Subtitle.", 'dolomia'),
            "param_name"  => "dolomia_header_subtitle",
            "value"       => "",
            "description" => esc_html__("Subtitle on the header", 'dolomia')
        ),
        
    )));
}   

//Flexslider Header
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text." Flexslider Header", 'dolomia'),
    "base"     => "dolomia_header_corporate",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            'type'       => 'dropdown',
            'heading'    => esc_html__( 'Slide', 'dolomia' ),
            'param_name' => 'dolomia_header_slide',
            'value'      => array(
                esc_html__( '', 'dolomia' )         => '',    
                esc_html__( 'Slide first', 'dolomia' )   => 'slide_first',    
                esc_html__( 'Slide center', 'dolomia' )   => 'slide_center',
                esc_html__( 'Slide last', 'dolomia' )   => 'slide_last',
            ),
            'description' => esc_html__( 'Location of slide.', 'dolomia' )
        ),
        array(
            'type'        => 'attach_image',
            'heading'     => esc_html__( 'Images background', 'dolomia' ),
            'param_name'  => 'dolomia_header_image',
            'value'       => '',
            'description' => esc_html__( '', 'dolomia' )
        ),

        array(
            "type"        => "textarea_html",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Title.", 'dolomia'),
            "param_name"  => "dolomia_header_title",
            "value"       => "",
            "description" => esc_html__("", 'dolomia')
        ),
        array(
            "type"        => "textarea",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Subtitle.", 'dolomia'),
            "param_name"  => "dolomia_header_subtitle",
            "value"       => "",
            "description" => esc_html__("", 'dolomia')
        ),
        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Link button", 'dolomia'),
            "param_name"  => "dolomia_header_link_button",
            "value"       => "",
            "description" => esc_html__("", 'dolomia')
        ),
        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Text button", 'dolomia'),
            "param_name"  => "dolomia_header_text_button",
            "value"       => "",
            "description" => esc_html__("", 'dolomia')
        ),
    )));
} 

//Section About
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Section About", 'dolomia'),
    "base"     => "dolomia_section_about",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Title.", 'dolomia'),
            "param_name"  => "dolomia_section_about_title",
            "value"       => "",
            "description" => esc_html__("Ex: About Us", 'dolomia')
        ),
        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Subtitle.", 'dolomia'),
            "param_name"  => "dolomia_section_about_sub",
            "value"       => "",
            "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit", 'dolomia')
        ),
        array(
            "type"        => "textarea",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Content", 'dolomia'),
            "param_name"  => "dolomia_section_about_content",
            "value"       => "",
            // "description" => esc_html__("The content", 'dolomia')
        ),

    )));
} 


//Services
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Services", 'dolomia'),
    "base"     => "dolomia_service",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

            array(
                "type"        => "textfield",
                "holder"      => "div",
                "class"       => "",
                "heading"     => esc_html__("Icon", 'dolomia'),
                "param_name"  => "dolomia_service_icon",
                "value"       => "",
                "description" => esc_html__("Ex: cloudy-day.", 'dolomia')
            ),
        
            array(
                "type"        => "textfield",
                "holder"      => "div",
                "class"       => "",
                "heading"     => esc_html__("FA Icon", 'dolomia'),
                "param_name"  => "dolomia_service_fa_icon",
                "value"       => "",
                "description" => esc_html__("Ex: cloudy-day. Get more in http://fontawesome.io/icons/", 'dolomia')
            ),
            
            array(
                "type"        => "textfield",
                "holder"      => "div",
                "class"       => "",
                "heading"     => esc_html__("Title", 'dolomia'),
                "param_name"  => "dolomia_service_title",
                "value"       => "",
                "description" => esc_html__("Ex: In every conditions", 'dolomia')
            ),
            array(
                "type"        => "textarea",
                "holder"      => "div",
                "class"       => "",
                "heading"     => esc_html__("Content", 'dolomia'),
                "param_name"  => "dolomia_service_content",
                "value"       => "",
                "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem harum aspernatur sapiente error, voluptas fuga, laudantium ullam magni fugit. Qui!", 'dolomia')
            ),                               

       
    )));
}

//Heading
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Heading", 'dolomia'),
    "base"     => "dolomia_heading",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Title", 'dolomia'),
            "param_name"  => "dolomia_heading_title",
            "value"       => "",
            "description" => esc_html__("Ex: Our fabulous team", 'dolomia')
        ),
        array(
            "type"        => "textarea",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("SubTitle", 'dolomia'),
            "param_name"  => "dolomia_heading_subtitle",
            "value"       => "",
            "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit", 'dolomia')
        ),
    )));
}

//Team
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Team", 'dolomia'),
    "base"     => "dolomia_team",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
            
            array(
                'type'        => 'attach_image',
                'heading'     => esc_html__( 'Avatar', 'dolomia' ),
                'param_name'  => 'dolomia_team_avatar',
                'value'       => '',
                'description' => esc_html__( 'Select images from media library.', 'dolomia' )
            ),

            array(
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'dolomia_team_social',
                'params' => array(             
                    array(
                      'type' => 'textfield',
                      'value' => '',
                      'heading' => 'Text your icon',
                      'param_name' => 'dolomia_team_social_icon',
                  ),
                     array(
                      'type' => 'textfield',
                      'value' => '',
                      'heading' => 'Text your link',
                      'param_name' => 'dolomia_team_social_link',
                  ),                  

              )
            ),

            array(
                "type"        => "textfield",
                "holder"      => "div",
                "class"       => "",
                "heading"     => esc_html__("Name", 'dolomia'),
                "param_name"  => "dolomia_team_name",
                "value"       => "",
                "description" => esc_html__("Ex: John Doe", 'dolomia')
            ),

            array(
                "type"        => "textfield",
                "holder"      => "div",
                "class"       => "",
                "heading"     => esc_html__("Address", 'dolomia'),
                "param_name"  => "dolomia_team_add",
                "value"       => "",
                "description" => esc_html__("EX: Cortina, Italy", 'dolomia')
            ),   
        
            array(
                "type"        => "textfield",
                "holder"      => "div",
                "class"       => "",
                "heading"     => esc_html__("Link", 'dolomia'),
                "param_name"  => "dolomia_team_url",
                "value"       => "",
                "description" => esc_html__("Ex: http://balkanhiking.com/gordana-rozek/", 'dolomia')
            ),
    )));
}

//Showcase Trip
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Showcase Trip", 'dolomia'),
    "base"     => "dolomia_showcase_trip",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Title", 'dolomia'),
            "param_name"  => "dolomia_showcase_trip_title",
            "value"       => "",
            "description" => esc_html__("Ex: Featured Trips", 'dolomia')
        ),
        array(
            "type"        => "textarea",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("SubTitle", 'dolomia'),
            "param_name"  => "dolomia_showcase_trip_sub",
            "value"       => "",
            "description" => esc_html__("Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit", 'dolomia')
        ),

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Enter the post ID", 'dolomia'),
            "param_name"  => "dolomia_showcase_trip_sub",
            "value"       => "",
            "description" => esc_html__("Enter post ID that want to show", 'dolomia')
        ),


    )));
}

//Carousel Gallery
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Carousel Gallery", 'dolomia'),
    "base"     => "dolomia_carousel",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(
         array(
              'type' => 'attach_images',
              'value' => '',
              'heading' => 'Upload image',
              'param_name' => 'dolomia_carousel_img',
              'description' => esc_html__( 'Select images from media library.', 'dolomia' ),
            ),
    )));
}

//Contact All Treks
if(function_exists('vc_map')){
    vc_map( array(
    "name"     => esc_html__($pre_text."Contact All Treks", 'dolomia'),
    "base"     => "dolomia_all_trek_contact",
    "class"    => "",
    "icon"     => "icon-st",
    "category" => 'Content',
    "params"   => array(

        array(
            "type"        => "textfield",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Title", 'dolomia'),
            "param_name"  => "dolomia_all_trek_contact_title",
            "value"       => "",
            "description" => esc_html__("Ex: Would you like more info about other trips?", 'dolomia')
        ),

        array(
            "type"        => "textarea",
            "holder"      => "div",
            "class"       => "",
            "heading"     => esc_html__("Button", 'dolomia'),
            "param_name"  => "dolomia_all_trek_contact_btn",
            "value"       => "",
            "description" => esc_html__("Ex: Contact us", 'dolomia')
        ),

    )));
}