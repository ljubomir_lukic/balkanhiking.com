<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'moroko_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}




add_action( 'cmb2_admin_init', 'moroko_register_post_metabox' );
/**
 * Hook in and add a metabox to add fields to the post pages
 */
function moroko_register_post_metabox() {

	$cmb_post_content_extra = new_cmb2_box( array(
		'id'            => 'post_content_extra',
        'title'         => __( 'Information', 'cmb2' ),
        'object_types'  => array( 'post', ), // Post type
        'context'       => 'normal',
	) );

	$cmb_post_content_extra->add_field( array(
		'name' => 'URL Vimeo',
		'desc'	=>	'EX: https://www.youtube.com/watch?v=6lV5vSOz6co',
		'id' =>  'url_youtube',
		'type' => 'text',
		'default' => '',
	) );
	$cmb_post_content_extra->add_field( array(
	    'name' => 'Select images for Gallery',
	    'desc' => '',
	    'id'   =>  'gallery',
	    'type' => 'file_list',
	    // 'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	    // Optional, override default text strings
	    'text' => array(
	        'add_upload_files_text' => 'Replacement', // default: "Add or Upload Files"
	        'remove_image_text' => 'Replacement', // default: "Remove Image"
	        'file_text' => 'Replacement', // default: "File:"
	        'file_download_text' => 'Replacement', // default: "Download"
	        'remove_text' => 'Replacement', // default: "Remove"
	    ),
	) );
	



	/**
    * Initiate the metabox
    */
	$cmb_post = new_cmb2_box( array(
		'id'            => 'post_metabox',
        'title'         => __( 'Header', 'cmb2' ),
        'object_types'  => array( 'post', ), // Post type
        'context'       => 'normal',
	) );

	$cmb_post->add_field( array(
		'name' => __( 'Title Header', 'cmb2' ),
		'id' =>  'header_title',
		'type' => 'text',
		'default' => '',
	) );

	$cmb_post->add_field( array(
		'name' => __( 'Header Subtitle', 'cmb2' ),
		'id' =>  'header_subtitle',
		'type' => 'text',
		'default' => '',
	) );
	$cmb_post->add_field( array(
	    'name'    => 'Header Image Background',
	    'desc'    => 'Upload an image or enter an URL.',
	    'id'      =>  'header_image_background',
	    'type'    => 'file',
	    'default' => '',
	    // Optional:
	    'options' => array(
	        'url' => true, // Hide the text input for the url
	        // 'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
	    ),
	) );



// Single portfolio project

	$cmb_portfolios = new_cmb2_box( array(
		'id'            => 'portfolios_metabox',
        'title'         => __( 'Single Project', 'cmb2' ),
        'object_types'  => array( 'portfolio', ), // Post type
        'context'       => 'normal',
	) );

	$cmb_portfolios->add_field( array(
		'name'             => esc_html__( 'Display type', 'cmb2' ),
		'desc'             => esc_html__( 'field description (optional)', 'cmb2' ),
		'id'               =>'project_type',
		'type'             => 'select',
		'show_option_none' => true,
		'options'          => array(
		'standard' => esc_html__( 'Single project 1', 'cmb2' ),
		'custom'   => esc_html__( 'Single project 2', 'cmb2' ),
		  ),
	 ) );

	$cmb_portfolios->add_field( array(
		'name' => __( 'Title Header', 'cmb2' ),
		'id' =>  'project_title',
		'type' => 'text',
		'default' => '',
	) );

	$cmb_portfolios->add_field( array(
		'name' => __( 'Header Subtitle', 'cmb2' ),
		'id' =>  'project_subtitle',
		'type' => 'text',
		'default' => '',
	) );

	$cmb_portfolios->add_field( array(
	    'name'    => 'Header Image Background',
	    'desc'    => 'Upload an image or enter an URL.',
	    'id'      =>  'project_header_image_background',
	    'type'    => 'file',
	    'default' => '',
	    // Optional:
	    'options' => array(
	        'url' => true, // Hide the text input for the url
	        // 'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
	    ),
	) );

	 $cmb_portfolios->add_field( array(
		'name' => __( 'Title of portfolio post', 'cmb2' ),
		'id' =>  'project_name',
		'type' => 'text',
		'default' => '',
	) ); 

	 $cmb_portfolios->add_field( array(
		'name' => __( 'Subitle of portfolio post', 'cmb2' ),
		'id' =>  'project_subname',
		'type' => 'text',
		'default' => '',
	) );


    $cmb_portfolios->add_field( array(
		'name' => __( 'Travel location', 'cmb2' ),
		'id' =>  'project_location',
		'type' => 'text',
		'default' => '',
	) );

	$cmb_portfolios->add_field( array(
		'name' => __( '"Like" quantity', 'cmb2' ),
		'id' =>  'project_like',
		'type' => 'text',
		'default' => '',
	) );

	$cmb_portfolios->add_field( array(
	    'name' => 'Select images for Gallery',
	    'desc' => '',
	    'id'   =>  'gallery_portfolio',
	    'type' => 'file_list',
	    // 'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
	    // Optional, override default text strings
	    'text' => array(
	        'add_upload_files_text' => 'Replacement', // default: "Add or Upload Files"
	        'remove_image_text' => 'Replacement', // default: "Remove Image"
	        'file_text' => 'Replacement', // default: "File:"
	        'file_download_text' => 'Replacement', // default: "Download"
	        'remove_text' => 'Replacement', // default: "Remove"
	    ),
	) );

	$cmb_dolomia = new_cmb2_box( array(
		'id'           =>  'portfolio_img',
		'title'        => __( 'Group Image on Bottom Page', 'cmb2' ),
		'object_types' => array( 'portfolio', ),
	) );

	// $group_field_id is the field id string, so in this case:  'demo'
	$group_field_id = $cmb_dolomia->add_field( array(
		'id'          =>  'portfolio_img',
		'type'        => 'group',
		'description' => __( 'Add a Group Image', 'cmb2' ),
		'options'     => array(
			'group_title'   => __( 'Entry {#}', 'cmb2' ), // {#} gets replaced by row number
			'add_button'    => __( 'Add Group Image', 'cmb2' ),
			'remove_button' => __( 'Remove Group Image', 'cmb2' ),
			'sortable'      => true, // beta
			// 'closed'     => true, // true to have the groups closed by default
		),
	) );

	$cmb_dolomia->add_group_field( $group_field_id, array(
		'name' => __( 'Group Image on Bottom Page', 'cmb2' ),
			'desc' => __( 'Upload Image for bottom Page', 'cmb2' ),
			'id'   =>  'image_portfolio_gr',
			'type' => 'file',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
	) );
	 
	$cmb_dolomia->add_group_field( $group_field_id, array(
		'name' => __( 'Number of Column That You want Show.', 'cmb2' ),
			'desc' => __( 'Number of Column That You want Show.From 1 to 12.', 'cmb2' ),
			'id'   =>  'number_column_portfolio',
			'type' => 'text',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
	) );
	
//======= End Single portfolio project========



// =======Single trek=======

	//header
	$cmb_treks = new_cmb2_box( array(
		'id'            => 'treks_metabox',
        'title'         => __( 'SINGLE TREK', 'cmb2' ),
        'object_types'  => array( 'trek', ), // Post type
        'context'       => 'normal',
	) );

	$cmb_treks->add_field( array(
		'name'             => esc_html__( 'Choose to display type at Gallery', 'cmb2' ),
		'desc'             => esc_html__( 'If you can choose "Display", it will display as a single post when click the link. Opposite, it will display as a image', 'cmb2' ),
		'id'               =>'trek_gallery',
		'type'             => 'select',
		'show_option_none' => true,
		'options'          => array(
		'choose' => esc_html__( 'Display', 'cmb2' ),
		'no_choose'   => esc_html__( 'Not display', 'cmb2' ),
		  ),
	 ) );

	$cmb_treks->add_field( array(
		'name'             => esc_html__( 'Display type', 'cmb2' ),
		'desc'             => esc_html__( 'field description (optional)', 'cmb2' ),
		'id'               =>'trek_type',
		'type'             => 'select',
		'show_option_none' => true,
		'options'          => array(
		'standard' => esc_html__( 'Single trek 1', 'cmb2' ),
		'custom'   => esc_html__( 'Single trek 2', 'cmb2' ),
		  ),
	 ) );

	$cmb_treks->add_field( array(
		'name' => __( 'Title Header', 'cmb2' ),
		'id' =>  'trek_title',
		'type' => 'text',
		'default' => '',
	) );

	$cmb_treks->add_field( array(
		'name' => __( 'Header Subtitle', 'cmb2' ),
		'id' =>  'trek_subtitle',
		'type' => 'text',
		'default' => '',
	) );

	$cmb_treks->add_field( array(
	    'name'    => 'Header Image Background',
	    'desc'    => 'Upload an image or enter an URL.',
	    'id'      =>  'trek_header_image_background',
	    'type'    => 'file',
	    'default' => '',
	    // Optional:
	    'options' => array(
	        'url' => true, // Hide the text input for the url
	        // 'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
	    ),
	) );
	// End header 

	 $cmb_treks->add_field( array(
		'name' => __( 'Subtitle of trek post', 'cmb2' ),
		'id' =>  'trek_subname',
		'type' => 'text',
		'default' => '',
	) );

	$cmb_treks->add_field( array(
		'name' => __( 'Short description for trip', 'cmb2' ),
		'id' =>  'trek_des_trip',
		'type' => 'text',
		'default' => '',
	) );
	// end short des left

	// Short des right
	$cmb_treks->add_field( array(
	    'name'    => 'Avatar of Visitor',
	    'desc'    => 'Upload an image or enter an URL.',
	    'id'      =>  'trek_img_avatar',
	    'type'    => 'file',
	    'default' => '',
	    'options' => array(
	        'url' => true,),
	) );

	$cmb_treks->add_field( array(
		'name' => __( 'Position in club', 'cmb2' ),
		'id' =>  'trek_name_place',
		'type' => 'text',
		'default' => '',
	) );

	$cmb_treks->add_field( array(
		'name' => __( 'Name of the Guide', 'cmb2' ),
		'id' =>  'trek_visitor',
		'type' => 'text',
		'default' => '',
	) );

	 $cmb_treks->add_field( array(
		'name' => __( 'Where the Guide comes from', 'cmb2' ),
		'id' =>  'trek_country',
		'type' => 'text',
		'default' => '',
	) );

	$group_field_trek_id = $cmb_treks->add_field( array(
		'id'          =>  'trek_socials',
		'type'        => 'group',
		'description' => __( 'Add a Group Socials', 'cmb2' ),
		'options'     => array(
			'group_title'   => __( 'Entry {#}', 'cmb2' ), // {#} gets replaced by row number
			'add_button'    => __( 'Add Group Socials', 'cmb2' ),
			'remove_button' => __( 'Remove Group Socials', 'cmb2' ),
			'sortable'      => true, 
		),
	) );
	 
	$cmb_treks->add_group_field( $group_field_trek_id, array(
		'name' => __( 'The social icons', 'cmb2' ),
		'desc' => __( 'You can find more on the website "http://fontawesome.io/icons/". Ex: twitter, dribbble, ...', 'cmb2' ),
		'id'   =>  'trek_socials_icon',
		'type' => 'text',

	) );

	$cmb_treks->add_group_field( $group_field_trek_id, array(
		'name' => __( 'The social link ', 'cmb2' ),
		'desc' => __( '#', 'cmb2' ),
		'id'   =>  'trek_socials_link',
		'type' => 'text',

	) );
	//End short des right

	//middle background
	$cmb_treks->add_field( array(
	    'name'    => 'Background of Trek Data',
	    'desc'    => 'Upload an image or enter an URL.',
	    'id'      =>  'trek_data_bg',
	    'type'    => 'file',
	    'default' => '',
	    'description' => 'You can select it when you choice display type is "Single trek 1"',
	    'options' => array(
	        'url' => true,),
	) );

	//select if you want to show the heading and short description for the post content 
	$cmb_treks->add_field( array(
		'name' => __( 'The heading of the post', 'cmb2' ),
		'id' =>  'trek_heading',
		'type' => 'text',
		'default' => '',
		'description' => 'You can select it when you choice display type is "Single trek 2"',
	) );

	$cmb_treks->add_field( array(
		'name' => __( 'The short description of the post', 'cmb2' ),
		'id' =>  'trek_shot_des',
		'type' => 'text',
		'default' => '',
		'description' => 'You can select it when you choice display type is "Single trek 2"',
	) );


	// trek data

   	$group_field_trek_data = $cmb_treks->add_field( array(
		'id'          =>  'trek_data',
		'type'        => 'group',
		'description' => __( 'Add a group Trek data', 'cmb2' ),
		'options'     => array(
			'group_title'   => __( 'Entry {#}', 'cmb2' ), // {#} gets replaced by row number
			'add_button'    => __( 'Add Group Trek Data', 'cmb2' ),
			'remove_button' => __( 'Remove Group Trek Data', 'cmb2' ),
			'sortable'      => true, 
		),
	) );
	 
	$cmb_treks->add_group_field( $group_field_trek_data, array(
		'name' => __( 'The trek data icons', 'cmb2' ),
		'desc' => __( 'You can find more on the website "http://fontawesome.io/icons/". Ex: money, hour, camp-bag, male, watch, distance...', 'cmb2' ),
		'id'   =>  'trek_data_icon',
		'type' => 'text',

	) );

	$cmb_treks->add_group_field( $group_field_trek_data, array(
		'name' => __( 'Data 1', 'cmb2' ),
		'desc' => __( 'Ex: Price, Data, Difficulty, Max Group Size, Days, Distance,...', 'cmb2' ),
		'id'   =>  'trek_data1',
		'type' => 'text',

	) );

	$cmb_treks->add_group_field( $group_field_trek_data, array(
		'name' => __( 'Data 2', 'cmb2' ),
		'desc' => __( 'Ex: 390€, 12/02/2017, Hight, 20, 2, 10.3 Km,... ', 'cmb2' ),
		'id'   =>  'trek_data2',
		'type' => 'text',

	) );	

	//Similar trips
	$cmb_treks->add_field( array(
		'name' => __( 'ID of the similar trips that want to show', 'cmb2' ),
		'id' =>  'dolomia_similar_trips_id',
		'type' => 'text',
		'default' => '',
		'description' => 'Ex: 144,...',
	) );

	$cmb_treks->add_field( array(
		'name' => __( 'Title', 'cmb2' ),
		'id' =>  'dolomia_similar_trips_title',
		'type' => 'text',
		'default' => '',
		'description' => 'Ex: Similar Trips',
	) );

	$cmb_treks->add_field( array(
		'name' => __( 'Subtitle', 'cmb2' ),
		'id' =>  'dolomia_similar_trips_subtitle',
		'type' => 'text',
		'default' => '',
		'description' => 'Ex: Lorem ipsum dolor sit amet, consectetur adipisicing elit',
	) );

	//trek contact

	$cmb_treks->add_field( array(
		'name' => __( 'Caption of the contact', 'cmb2' ),
		'id' =>  'trek_contact_caption',
		'type' => 'text',
		'default' => '',
		'description' => 'Would you like more info about this trip?',
	) );

	$cmb_treks->add_field( array(
		'name' => __( 'Information 1', 'cmb2' ),
		'id' =>  'trek_contact_info1',
		'type' => 'text',
		'default' => '',
		'description' => 'Call us at ',
	) );

	$cmb_treks->add_field( array(
		'name' => __( 'Information 2', 'cmb2' ),
		'id' =>  'trek_contact_info2',
		'type' => 'text',
		'default' => '',
		'description' => '+40 45615654',
	) );

	$cmb_treks->add_field( array(
		'name' => __( 'Information 3', 'cmb2' ),
		'id' =>  'trek_contact_info3',
		'type' => 'text',
		'default' => '',
		'description' => 'Contact us',
	) );
	
// End Single portfolio trek

//Page default
	$cmb_page = new_cmb2_box( array(
		'id'            => 'page_metabox',
        'title'         => __( 'Flexslider for header. Use it if you want to display only a slider in the header section. Can use for most pages such as: Blog Classic, Gallery, Treks,...', 'cmb2' ),
        'object_types'  => array( 'page', ), 
        'context'       => 'normal',
	) );

	$cmb_page->add_field( array(
		'name' => __( 'Title Header', 'cmb2' ),
		'id' =>  'page_header_title',
		'type' => 'text',
		'default' => '',
		'description' => 'Only use for page that not home',
	) );

	$cmb_page->add_field( array(
		'name' => __( 'Header Subtitle', 'cmb2' ),
		'id' =>  'page_header_subtitle',
		'type' => 'text',
		'default' => '',
		'description' => 'Only use for page that not home',

	) );
	$cmb_page->add_field( array(
	    'name'    => 'Header Image Background',
	    'desc'    => 'Upload an image or enter an URL.',
	    'id'      =>  'page_header_image_background',
	    'type'    => 'file',
	    'default' => '',
		'description' => 'Only use for page that not home',

	    // Optional:
	    'options' => array(
	        'url' => true,
	    ),
	) );

// Flexslider Nav
	$cmb_page = new_cmb2_box( array(
		'id'           =>  'slider_nav',
		'title'        => __( 'Flexslider navigation for header. Use it if you want to display multiple slider in the header section. Should be used for Home Classic. ', 'cmb2' ),
		'object_types' => array( 'page', ),
	) );

	// $group_field_id is the field id string, so in this case:  'demo'
	$group_slider_id = $cmb_page->add_field( array(
		'id'          =>  'slider_nav',
		'type'        => 'group',
		'description' => __( 'Add a Group Slider', 'cmb2' ),
		'options'     => array(
			'group_title'   => __( 'Entry {#}', 'cmb2' ), // {#} gets replaced by row number
			'add_button'    => __( 'Add Group Slider', 'cmb2' ),
			'remove_button' => __( 'Remove Group Slider', 'cmb2' ),
			'sortable'      => true, 
		),
	) );

	$cmb_page->add_group_field( $group_slider_id, array(
		'name' => __( 'Background image of slider', 'cmb2' ),
			'desc' => __( 'Upload Image', 'cmb2' ),
			'id'   =>  'slider_img',
			'type' => 'file',
	) );
	 
	$cmb_page->add_group_field( $group_slider_id, array(
		'name' => __( 'Title', 'cmb2' ),
			'desc' => __( 'The title of Silder', 'cmb2' ),
			'id'   =>  'slider_title',
			'type' => 'textarea',
	) );

	$cmb_page->add_group_field( $group_slider_id, array(
		'name' => __( 'Title', 'cmb2' ),
			'desc' => __( 'The subtitle of Silder', 'cmb2' ),
			'id'   =>  'slider_subtitle',
			'type' => 'textarea',
	) );

	$cmb_page->add_group_field( $group_slider_id, array(
		'name' => __( 'Button', 'cmb2' ),
			'desc' => __( 'The button of Silder', 'cmb2' ),
			'id'   =>  'slider_btn',
			'type' => 'text',
	) );

	$cmb_page->add_group_field( $group_slider_id, array(
		'name' => __( 'Button Link', 'cmb2' ),
			'desc' => __( 'The button of Silder', 'cmb2' ),
			'id'   =>  'slider_btn_link',
			'type' => 'text',
	) );

	// Home showcase
	$cmb_page = new_cmb2_box( array(
		'id'            => 'page_metabox',
        'title'         => __( 'This section displays vertical slider of Home Showecase page content', 'cmb2' ),
        'object_types'  => array( 'page', ), 
        'context'       => 'normal',
	) );

	
	$cmb_page->add_field( array(
		'name' => __( 'Main Slider', 'cmb2' ),
		'id' =>  'page_header_title',
		'type' => 'text',
		'default' => '',
		'description' => 'Only use for page that not home',
	) );

	$cmb_page->add_field( array(
		'name' => __( 'Header Subtitle', 'cmb2' ),
		'id' =>  'page_header_subtitle',
		'type' => 'text',
		'default' => '',
		'description' => 'Only use for page that not home',

	) );
	$cmb_page->add_field( array(
	    'name'    => 'Header Image Background',
	    'desc'    => 'Upload an image or enter an URL.',
	    'id'      =>  'page_header_image_background',
	    'type'    => 'file',
	    'default' => '',
		'description' => 'Only use for page that not home',

	    // Optional:
	    'options' => array(
	        'url' => true,
	    ),
	) );



}

