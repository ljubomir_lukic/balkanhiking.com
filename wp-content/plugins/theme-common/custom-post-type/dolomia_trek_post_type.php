<?php


// Register Custom Post Type
function trek_post_type() {

    $labels = array(
        'name'                  => _x( 'Treks', 'Post Type General Name', 'dolomia' ),
        'singular_name'         => _x( 'Trek', 'Post Type Singular Name', 'dolomia' ),
        'menu_name'             => __( 'Treks', 'dolomia' ),
        'name_admin_bar'        => __( 'Trek', 'dolomia' ),
        'parent_item_colon'     => __( 'Parent Trek:', 'dolomia' ),
        'all_items'             => __( 'All Treks', 'dolomia' ),
        'add_new_item'          => __( 'Add New Trek', 'dolomia' ),
        'add_new'               => __( 'Add New', 'dolomia' ),
        'new_item'              => __( 'New Trek', 'dolomia' ),
        'edit_item'             => __( 'Edit Trek', 'dolomia' ),
        'update_item'           => __( 'Update Trek', 'dolomia' ),
        'view_item'             => __( 'View Trek', 'dolomia' ),
        'search_items'          => __( 'Search Trek', 'dolomia' ),
        'not_found'             => __( 'Not found', 'dolomia' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'dolomia' ),
        'featured_image'        => __( 'Featured Image', 'dolomia' ),
        'set_featured_image'    => __( 'Set featured image', 'dolomia' ),
        'remove_featured_image' => __( 'Remove featured image', 'dolomia' ),
        'use_featured_image'    => __( 'Use as featured image', 'dolomia' ),
        'insert_into_item'      => __( 'Insert into item', 'dolomia' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'dolomia' ),
        'items_list'            => __( 'Treks list', 'dolomia' ),
        'items_list_navigation' => __( 'Treks list navigation', 'dolomia' ),
        'filter_items_list'     => __( 'Filter Treks list', 'dolomia' ),
    );
    $args = array(
        'label'                 => __( 'Trek', 'dolomia' ),
        'description'           => __( 'Trek Description', 'dolomia' ),
        'taxonomies'            => array( 'category' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'thumbnail', 'editor', 'post-formats' ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'menu_icon'             => '', 
        'query_var'             => true,
        'rewrite'               => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'trek', $args );

}
add_action( 'init', 'trek_post_type', 0 );

//add_action( 'init', 'create_category_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it Categoriess for your posts

function create_category_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

  $labels1 = array(
    'name'              => __( 'Categories', 'dolomia' ),
    'singular_name'     => __( 'Categories', 'dolomia' ),
    'search_items'      => __( 'Search Categories','dolomia' ),
    'all_items'         => __( 'All Categories','dolomia' ),
    'parent_item'       => __( 'Parent Categories','dolomia' ),
    'parent_item_colon' => __( 'Parent Categories:','dolomia' ),
    'edit_item'         => __( 'Edit Categories','dolomia' ), 
    'update_item'       => __( 'Update Categories','dolomia' ),
    'add_new_item'      => __( 'Add New Categories','dolomia' ),
    'new_item_name'     => __( 'New Categories Name','dolomia' ),
    'menu_name'         => __( 'Categories','dolomia' ),
  );     

// Now register the taxonomy

  register_taxonomy('category',array('trek'), array(
    'hierarchical'      => true,
    'labels'            => $labels1,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'category' ),
  ));

}

// Register Custom Post Type
function portfolio_post_type() {

    $labels = array(
        'name'                  => _x( 'Portfolios', 'Post Type General Name', 'matelick' ),
        'singular_name'         => _x( 'Portfolio', 'Post Type Singular Name', 'matelick' ),
        'menu_name'             => __( 'Portfolios', 'matelick' ),
        'name_admin_bar'        => __( 'Portfolio', 'matelick' ),
        'parent_item_colon'     => __( 'Parent Portfolio:', 'matelick' ),
        'all_items'             => __( 'All Portfolios', 'matelick' ),
        'add_new_item'          => __( 'Add New Portfolio', 'matelick' ),
        'add_new'               => __( 'Add New', 'matelick' ),
        'new_item'              => __( 'New Portfolio', 'matelick' ),
        'edit_item'             => __( 'Edit Portfolio', 'matelick' ),
        'update_item'           => __( 'Update Portfolio', 'matelick' ),
        'view_item'             => __( 'View Portfolio', 'matelick' ),
        'search_items'          => __( 'Search Portfolio', 'matelick' ),
        'not_found'             => __( 'Not found', 'matelick' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'matelick' ),
        'featured_image'        => __( 'Featured Image', 'matelick' ),
        'set_featured_image'    => __( 'Set featured image', 'matelick' ),
        'remove_featured_image' => __( 'Remove featured image', 'matelick' ),
        'use_featured_image'    => __( 'Use as featured image', 'matelick' ),
        'insert_into_item'      => __( 'Insert into item', 'matelick' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'matelick' ),
        'items_list'            => __( 'Portfolios list', 'matelick' ),
        'items_list_navigation' => __( 'Portfolios list navigation', 'matelick' ),
        'filter_items_list'     => __( 'Filter Portfolios list', 'matelick' ),
    );
    $args = array(
        'label'                 => __( 'Portfolio', 'matelick' ),
        'description'           => __( 'Portfolio Description', 'matelick' ),
        'taxonomies'            => array( 'skill' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'thumbnail', 'editor', 'post-formats' ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'menu_icon'             => '', 
        'query_var'             => true,
        'rewrite'               => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'portfolio', $args );

}
add_action( 'init', 'portfolio_post_type', 0 );

add_action( 'init', 'create_skill_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it Skillss for your posts

function create_skill_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

  $labels1 = array(
    'name'              => __( 'Skills', 'matelick' ),
    'singular_name'     => __( 'Skills', 'matelick' ),
    'search_items'      => __( 'Search Skills','matelick' ),
    'all_items'         => __( 'All Skills','matelick' ),
    'parent_item'       => __( 'Parent Skills','matelick' ),
    'parent_item_colon' => __( 'Parent Skills:','matelick' ),
    'edit_item'         => __( 'Edit Skills','matelick' ), 
    'update_item'       => __( 'Update Skills','matelick' ),
    'add_new_item'      => __( 'Add New Skills','matelick' ),
    'new_item_name'     => __( 'New Skills Name','matelick' ),
    'menu_name'         => __( 'Skills','matelick' ),
  );     

// Now register the taxonomy

  register_taxonomy('skill',array('portfolio'), array(
    'hierarchical'      => true,
    'labels'            => $labels1,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'skill' ),
  ));

}
?>