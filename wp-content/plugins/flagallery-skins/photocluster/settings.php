<?php
$default_options = array(
	// Common Settings
	'activColor' => '#3498db',
	'reverseColor' => '#ffffff',
	'hoverColor' => '#2470a1',
	'gallertMargin' => '0',
	// Splash Page
	'coverHeight' => '300',
	'coverWidth' => '500',
	'labelForNextButtonAlbum' => 'NEXT GALLERY',
	'labelForPrevButtonAlbum' => 'PREVIOUS GALLERY',
	//'labelForNextButtonCategory' => 'NEXT CATEGORY',
	//'labelForPrevButtonCategory' => 'PREVIOUS CATEGORY',
	'coverCounterDisplay'=>'1',
	'coverTitleFontSize' => '20',
	'coverTitleTextColor' => '#ffffff',
	'coverTitleBgColor' => '#000000',
	'coverTitleBgAlpha' => '60',
	'coverDescriptionTextColor' => '#ffffff',
	'coverDescriptionFontSize' => '14',
	// Collection Page
	'collectionHeaderBgColor' => '#ffffff',
	'collectionHeaderTextColor' => '#000000',
	'collectionDescriptionTextColor' => '#333333',
	'collectionThumbRecomendedHeight' => '200',
	'collectionThumbInPadding' => '6',
	'collectionThumbTitleVisibility' => '1',
	'collectionthumbHoverTitleTextColor' => '#ffffff',
	'collectionthumbHoverBgColor' => '#000000',
	'collectionthumbHoverBgAlpha' => '80',
	// Slider Page
	'sliderBgColor' => '#ffffff',
	'sliderThumbBarBgColor' => '#ffffff',
	'sliderThumbBarHeight' => '100',
	'sliderInfoEnable' => '1',
	'sliderInfoBoxBgColor' => '#ffffff',
	'sliderInfoBoxBgAlpha' => '80',
	'sliderInfoBoxTitleTextColor' => '#3498db',
	'sliderInfoBoxTextColor' => '#000000',
	'sliderItemDownload' => '1',
	'sliderItemDiscuss' => '0',
	'sliderSocialShareEnabled' => '1',
	// Custom CSS
	'customCSS' => ''
);
$options_tree = array(
	array('label' => 'Common Settings',
		'fields' => array(
			'gallertMargin' => array('label' => 'Vertical gallery margins',
				'tag' => 'input',
				'attr' => 'type="number" min="0" max="100"',
				'text' => ''
			),
			'activColor' => array('label' => 'Color 1',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => 'Set custom color for gallery'
			),
			'reverseColor' => array('label' => 'Color 2',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => 'Set custom color for gallery'
			),
			'hoverColor' => array('label' => 'Color 3',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => 'Set custom color for gallery'
			)
		)
	),
	array('label' => 'Collections Page (Splash page)',
		'fields' => array(
			'coverHeight' => array('label' => 'Collection Cover Height',
				'tag' => 'input',
				'attr' => 'type="number" min="150" max="800"',
				'text' => ''
			),
			'coverWidth' => array('label' => 'Minimum Collection Cover Width',
				'tag' => 'input',
				'attr' => 'type="number" min="150" max="1500"',
				'text' => ''
			),
			/*'labelForNextButtonCategory' => array('label' => 'Label for Next Button (Categories List)',
				'tag'   => 'input',
				'attr'  => '',
				'text'  => ''
			),
			'labelForPrevButtonCategory' => array('label' => 'Label for Previous Button (Categories List)',
				'tag'   => 'input',
				'attr'  => '',
				'text'  => ''
			),*/
			'labelForNextButtonAlbum' => array('label' => 'Label for Next Button (Galleries List)',
				'tag'   => 'input',
				'attr'  => '',
				'text'  => ''
			),
			'labelForPrevButtonAlbum' => array('label' => 'Label for Previous Button (Galleries List)',
				'tag'   => 'input',
				'attr'  => '',
				'text'  => ''
			),
			'coverCounterDisplay' => array('label' => 'Show items counter on the cover',
				'tag' => 'checkbox',
				'attr' => 'data-watch="change"',
				'text' => ''
			),
			'coverTitleFontSize' => array('label' => 'Cover Title - font size',
				'tag' => 'input',
				'attr' => 'type="number" min="14" max="36" step="1"',
				'text' => ''
			),
			'coverTitleTextColor' => array('label' => 'Cover Title - text color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => 'For Albums / Categories covers'
			),
			'coverTitleBgColor' => array('label' => 'Cover Title - background color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => 'For Albums / Categories covers'
			),
			'coverTitleBgAlpha' => array('label' => 'Cover Title - transparency for background',
				'tag' => 'input',
				'attr' => 'type="number" min="0" max="100" step="10"',
				'text' => 'For Albums / Categories covers'
			),
			'coverDescriptionFontSize' => array('label' => 'Cover Description - font size',
				'tag' => 'input',
				'attr' => 'type="number" min="10" max="20" step="1"',
				'text' => ''
			),
			'coverDescriptionTextColor' => array('label' => 'Cover Description - text color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => 'For Albums / Categories covers'
			)
		)
	),
	array('label' => 'Collection Part Settings',
		'fields' => array(
			'collectionHeaderBgColor' => array('label' => 'Header background color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => ''
			),
			'collectionHeaderTextColor' => array('label' => 'Header title - text color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => ''
			),
			'collectionDescriptionTextColor' => array('label' => 'Description - text color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => ''
			),
			'collectionThumbRecomendedHeight' => array('label' => 'Minimum Thumbnail Height',
				'tag' => 'input',
				'attr' => 'type="number" min="100" max="300"',
				'text' => ''
			),
			'collectionThumbInPadding' => array('label' => 'Thumbnail margin',
				'tag' => 'input',
				'attr' => 'type="number" min="0" max="20"',
				'text' => ''
			),
			'collectionThumbTitleVisibility' => array('label' => 'Show thumbnails title',
				'tag' => 'checkbox',
				'attr' => 'data-watch="change"',
				'text' => ''
			),
			'collectionthumbHoverTitleTextColor' => array('label' => 'Thumbnails Title - text color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => ''
			),
			'collectionthumbHoverBgColor' => array('label' => 'Thumbnails hover color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => ''
			),
			'collectionthumbHoverBgAlpha' => array('label' => 'Thumbnails hover color - alpha channel',
				'tag' => 'input',
				'attr' => 'type="number" min="0" max="100" step="10"',
				'text' => ''
			)
		)
	),
	array('label' => 'Slider Window Settings',
		'fields' => array(
			'sliderBgColor' => array('label' => 'Slider Window Color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => 'Set the background color for the slider window'
			),
			'sliderThumbBarBgColor' => array('label' => 'Slider Thumbnails Bar Color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => 'Set the background color for the thumbnails bar'
			),
			'sliderThumbBarHeight' => array('label' => 'Thumbnails Bar Height',
				'tag' => 'input',
				'attr' => 'type="number" min="60" max="200" step="10"',
				'text' => ''
			),
			'sliderInfoEnable' => array('label' => 'Show Info Button',
				'tag' => 'checkbox',
				'attr' => 'data-watch="change"',
				'text' => 'Enable description bar for item'
			),
			'sliderInfoBoxBgColor' => array('label' => 'Slider Info Bar background color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => 'Set the background color for the slider window'
			),
			'sliderInfoBoxBgAlpha' => array('label' => 'Slider Info Bar background - alpha channel ',
				'tag' => 'input',
				'attr' => 'type="number" min="0" max="100" step="10"',
				'text' => ''
			),
			'sliderInfoBoxTitleTextColor' => array('label' => 'Item title - text color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => ''
			),
			'sliderInfoBoxTextColor' => array('label' => 'Slider Info Bar text color',
				'tag' => 'input',
				'attr' => 'type="text" data-type="color"',
				'text' => ''
			),
			'sliderSocialShareEnabled' => array('label' => 'Show Share Buttons',
				'tag' => 'checkbox',
				'attr' => 'data-watch="change"',
				'text' => ''
			),
			'sliderItemDownload' => array('label' => 'Show Download Button',
				'tag' => 'checkbox',
				'attr' => 'data-watch="change"',
				'text' => 'Download original file'
			),
			/*'sliderItemDiscuss' => array('label' => 'Show Comments',
				'tag' => 'checkbox',
				'attr' => 'data-watch="change"',
				'text' => ''
			)*/
		)
	),
	array('label' => 'Advanced Settings',
		'fields' => array('customCSS' => array('label' => 'Custom CSS',
			'tag' => 'textarea',
			'attr' => 'cols="20" rows="10"',
			'text' => 'You can enter custom style rules into this box if you\'d like. IE: <i>a{color: red !important;}</i>
                                                                      <br />This is an advanced option! This is not recommended for users not fluent in CSS... but if you do know CSS, 
                                                                      anything you add here will override the default styles'
		)
			/*,
			'loveLink' => array(
				'label' => 'Display LoveLink?',
				'tag' => 'checkbox',
				'attr' => '',
				'text' => 'Selecting "Yes" will show the lovelink icon (codeasily.com) somewhere on the gallery'
			)*/
		)
	)
);
