<?php
$default_options = array(
    // Common Settings
    'activColor' => '#3498db',
    'reverseColor' => '#ffffff',
    'hoverColor' => '#2470a1',
    'gallertMargin' => '0',
    // Splash Page
    'columns' => '3',
    'thumbRecomendedSize' => '200',
    'spaceX' => '6',
    'coverThumbRatio' => '0.6',
	'coverCounterDisplay'=>'0',
	'coverTitleTextColor' => '#ffffff',
    'coverTitleBgColor' => '#000000',
    'coverTitleBgAlpha' => '60',
    'coverDescriptionTextColor' => '#ffffff',
    'descriptionBgColor' => '#000000',
    'descriptionImgAlpha' => '40',
	'flipEffect' => '1',
    // Collection Page
    'collectionBgColor' => '#f4f4f4',
    'collectionHeaderBgColor' => '#ffffff',
    'collectionHeaderTextColor' => '#000000',
    'collectionThumbRecomendedHeight' => '300',
    'collectionThumbInPadding' => '6',
    'collectionThumbTitleVisibility' => '1',
    'collectionthumbHoverTitleTextColor' => '#ffffff',
    'collectionthumbHoverBgColor' => '#000000',
    'collectionthumbHoverBgAlpha' => '80',
    // Slider Page
    'sliderBgColor' => '#ffffff',
    'sliderInfoEnable' => '1',
    'sliderInfoBoxBgColor' => '#ffffff',
    'sliderInfoBoxBgAlpha' => '80',
    'sliderInfoBoxTextColor' => '#000000',
    'sliderItemDownload' => '1',
    'sliderItemDiscuss' => '0',
    'sliderSocialShareEnabled' => '1',
    // Custom CSS
    'customCSS' => ''
);
$options_tree = array(
    array('label' => 'Common Settings',
        'fields' => array(
            'gallertMargin' => array('label' => 'Vertical gallery margins',
                'tag' => 'input',
                'attr' => 'type="number" min="0" max="100"',
                'text' => ''
            ),
            'activColor' => array('label' => 'Color 1',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => 'Set custom color for gallery'
            ),
            'reverseColor' => array('label' => 'Color 2',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => 'Set custom color for gallery'
            ),
            'hoverColor' => array('label' => 'Color 3',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => 'Set custom color for gallery'
            )
        )
    ),
    array('label' => 'Collections Page (Splash page)',
        'fields' => array(
            'columns' => array('label' => 'Thumbnail Columns',
                'tag' => 'input',
                'attr' => 'type="number" min="1" max="10"',
                'text' => 'Number of columns in a row'
            ),
            'thumbRecomendedSize' => array('label' => 'Minimum Thumbnail Width',
                'tag' => 'input',
                'attr' => 'type="number" min="100" max="300"',
                'text' => 'The module will ignore the number of columns.'
            ),
            'coverThumbRatio' => array('label' => 'Thumbnail ratio',
                'tag' => 'input',
                'attr' => 'type="number" min="0.1" max="2" step="0.1"',
                'text' => 'Height / Width = Ratio'
            ),
            'spaceX' => array('label' => 'Space between thumbnails',
                'tag' => 'input',
                'attr' => 'type="number" min="0" max="100"',
                'text' => ''
            ),
	        'flipEffect' => array('label' => 'Disable flip on mouse hover',
		        'tag' => 'checkbox',
		        'attr' => 'data-watch="change"',
		        'text' => ''
	        ),
	        'coverCounterDisplay' => array('label' => 'Show items counter on the cover',
		        'tag' => 'checkbox',
		        'attr' => 'data-watch="change"',
		        'text' => ''
	        ),
	        'coverTitleTextColor' => array('label' => 'Title - text color',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => 'For Albums / Categories covers'
            ),
            'coverTitleBgColor' => array('label' => 'Title - background color',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => 'For Albums / Categories covers'
            ),
            'coverTitleBgAlpha' => array('label' => 'Title - transparency for background',
                'tag' => 'input',
                'attr' => 'type="number" min="0" max="100" step="10"',
                'text' => 'For Albums / Categories covers'
            ),
            'coverDescriptionTextColor' => array('label' => 'Description - text color',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => 'For Albums / Categories covers'
            ),
            'descriptionBgColor' => array('label' => 'Description - background color',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => 'For Albums / Categories covers'
            ),
            'descriptionImgAlpha' => array('label' => 'Description - transparency for background',
                'tag' => 'input',
                'attr' => 'type="number" min="0" max="100" step="10"',
                'text' => 'For Albums / Categories covers'
            )
        )
    ),
    array('label' => 'Collection Window Settings',
        'fields' => array(
            'collectionBgColor' => array('label' => 'Collection Window color',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => ''
            ),
            'collectionHeaderBgColor' => array('label' => 'Header background color',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => ''
            ),
            'collectionHeaderTextColor' => array('label' => 'Header title - text color',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => ''
            ),
            'collectionThumbRecomendedHeight' => array('label' => 'Minimum Thumbnail Height',
                'tag' => 'input',
                'attr' => 'type="number" min="100" max="300"',
                'text' => ''
            ),
            'collectionThumbInPadding' => array('label' => 'Thumbnail margin',
                'tag' => 'input',
                'attr' => 'type="number" min="0" max="20"',
                'text' => ''
            ),
            'collectionThumbTitleVisibility' => array('label' => 'Show thumbnails title',
                'tag' => 'checkbox',
                'attr' => 'data-watch="change"',
                'text' => ''
            ),
            'collectionthumbHoverTitleTextColor' => array('label' => 'Thumbnails Title - text color',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => ''
            ),
            'collectionthumbHoverBgColor' => array('label' => 'Thumbnails hover color',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => ''
            ),
            'collectionthumbHoverBgAlpha' => array('label' => 'Thumbnails hover color - alpha channel',
                'tag' => 'input',
                'attr' => 'type="number" min="0" max="100" step="10"',
                'text' => ''
            )
        )
    ),
    array('label' => 'Slider Window Settings',
        'fields' => array(
            'sliderBgColor' => array('label' => 'Slider Window Color',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => 'Set the background color for the slider window'
            ),
            'sliderInfoEnable' => array('label' => 'Show Info Button',
                'tag' => 'checkbox',
                'attr' => 'data-watch="change"',
                'text' => 'Enable description bar for item'
            ),
            'sliderInfoBoxBgColor' => array('label' => 'Slider Info Bar background color',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => 'Set the background color for the slider window'
            ),
            'sliderInfoBoxBgAlpha' => array('label' => 'Slider Info Bar background - alpha channel ',
                'tag' => 'input',
                'attr' => 'type="number" min="0" max="100" step="10"',
                'text' => ''
            ),
            'sliderInfoBoxTextColor' => array('label' => 'Slider Info Bar text color',
                'tag' => 'input',
                'attr' => 'type="text" data-type="color"',
                'text' => ''
            ),
            'sliderSocialShareEnabled' => array('label' => 'Show Share Buttons',
                'tag' => 'checkbox',
                'attr' => 'data-watch="change"',
                'text' => ''
            ),
            'sliderItemDownload' => array('label' => 'Show Download Button',
                'tag' => 'checkbox',
                'attr' => 'data-watch="change"',
                'text' => 'Download original file'
            )
            /*'sliderItemDiscuss' => array('label' => 'Show Comments',
                'tag' => 'checkbox',
                'attr' => 'data-watch="change"',
                'text' => ''
            )*/
        )
    ),
    array('label' => 'Advanced Settings',
        'fields' => array('customCSS' => array('label' => 'Custom CSS',
            'tag' => 'textarea',
            'attr' => 'cols="20" rows="10"',
            'text' => 'You can enter custom style rules into this box if you\'d like. IE: <i>a{color: red !important;}</i>
                                                                      <br />This is an advanced option! This is not recommended for users not fluent in CSS... but if you do know CSS, 
                                                                      anything you add here will override the default styles'
        )
            /*,
            'loveLink' => array(
                'label' => 'Display LoveLink?',
                'tag' => 'checkbox',
                'attr' => '',
                'text' => 'Selecting "Yes" will show the lovelink icon (codeasily.com) somewhere on the gallery'
            )*/
        )
    )
);
