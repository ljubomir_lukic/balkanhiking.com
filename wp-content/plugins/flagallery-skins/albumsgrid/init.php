<?php
$ver = '1.8';

global $wp;
$settings['galleryID'] = $galleryID;
$settings['post_url'] = add_query_arg($_SERVER['QUERY_STRING'], '', home_url($wp->request));

wp_enqueue_style('flagallery-albumsgrid-skin', plugins_url('/css/albumsgrid.css', __FILE__), array(), $ver);
wp_enqueue_script('flagallery-albumsgrid-skin', plugins_url('/js/albumsgrid.js', __FILE__), array(), $ver, true);

?>

<noscript class="fla_AlbumsGrid_noscript">
    <p>JavaScript is not enabled in your browser</p>
</noscript>
<script type="text/javascript">
	function flaListINIT()
	{
		var id = 'flaAlbumsGrid2'+Math.floor((1 + Math.random()) * 0x10000);
		var dataDIVs = document.body.getElementsByClassName('albumsgrid_skin');
		dataDIVs[dataDIVs.length-1].setAttribute('app-id', id);
		this[id] = {'settings':<?php echo json_encode($settings);?>, 'data':<?php echo json_encode($data);?>, };
	}
	flaListINIT();
</script>