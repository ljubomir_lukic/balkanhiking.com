<?php
/*
Skin Name: Mosaic
Version: 1.1
uid: mosaic
status: GRANDPack
Skin URI: https://mypgc.co/portfolio-item/mosaic/
Description: Mosaic Skin transform your gallery into a visually-striking design piece by giving it a fresh masonry layout worthy of your favorite design blog
Author: PGC
Author URI: http://mypgc.co
*/
