<?php
$ver = '1.1';

$base_url_host = parse_url(site_url(), PHP_URL_HOST);
unset($settings['customCSS']);

wp_enqueue_style('flagallery-mosaic-skin', plugins_url('/css/mosaic.css', __FILE__), array(), $ver);
wp_enqueue_script('flagallery-mosaic-skin', plugins_url('/js/mosaic.min.js', __FILE__), array(
    'jquery',
), $ver, true
);

$content = array();
if( !isset($is_mob)){
    $is_mob = false;
}
foreach($data as $gallery){
    $path  = $gallery['gallery']['path'];
    $items = $gallery['data'];
    //if($gallery['gallery']['title']){
    //    echo "<h3>{$gallery['gallery']['title']}</h3>";
    //}
    //if($gallery['gallery']['galdesc']){
    //    echo '<div class="flagPhantom_galleryDescription">' . apply_filters('the_content', stripslashes($gallery['gallery']['galdesc'])) . '</div>';
    //}
    foreach($items as $item){
        $type = 'image';
        $ext  = pathinfo($item['filename'], PATHINFO_EXTENSION);

        $meta_data = $item['meta_data'];

        $web   = $path . '/webview/' . $item['filename'];
        $thumb = $path . '/thumbs/thumbs_' . $item['filename'];
        $image = $path . '/' . $item['filename'];

        //$width  = $meta_data['thumbnail']['width'];
        //$height = $meta_data['thumbnail']['height'];
        $width  = $meta_data['webview'][0];
        $height = $meta_data['webview'][1];

        $alttext = stripslashes($item['alttext']);
        $content[] = array(
            'id'        => $item['pid'],
            'type'      => $type,
            'ext'       => strtolower($ext),
            'src'       => $image,
            'sizes'     => array(
                'thumb' => $web,
                'full'  => $web,
            ),
            'title'     => $alttext,
            'alt'       => $alttext,
            'caption'   => str_replace(array("\r\n", "\r", "\n"), '', wpautop(stripslashes($item['description']))),
            'url'       => $item['link'],
            'views'     => $item['hitcounter'],
            'likes'     => $item['total_votes'],
            'width'     => $width,
            'height'    => $height,
        );
    }
}

if( !empty($content)){
    $settings = array_merge($settings, array(
        'ID'            => $galleryID,
        'is_mobile'     => $is_mob
    )
    );
    if(isset($settings['modal_name']) && empty($settings['modal_name'])){
        $settings['modal_name'] = 'flaglightbox';
    }
    $json_settings = json_encode($settings);
    ?>
    <div class="flagMosaic_Container noLightbox" data-flagid="<?php echo $galleryID; ?>"><?php
        if(empty($settings['key']) || 'MINIPack' === $settings['name']){
            echo "<a class='flaglove bl{$galleryID}' target='_blank' href='http://bit.ly/2jPNRB0'>" . __('Trial version - Get License', 'flash-album-gallery') . "</a>";
        }
        ?>
        <noscript><?php
            foreach($content as $item){
                if($item['alt']){
                    $item['alt'] = esc_attr($item['alt']);
                }
                $url = $item['url']? $item['url'] : $item['sizes']['full'];
                echo "<a href='{$url}'><img src='{$item['sizes']['thumb']}' alt='{$item['alt']}' /></a>";
            }
            ?></noscript><?php
        ?></div>
    <script type="text/javascript">
        jQuery(function($){
            var settings = <?php echo $json_settings; ?>;
            var flagallery_mosaic_content = <?php echo json_encode($content); ?>;
            settings.gallery = flagallery_mosaic_content;
            $('#FlaGallery_<?php echo $galleryID; ?> .flagMosaic_Container').flagalleryMosaic(settings);
        });
    </script>
    <?php
}
