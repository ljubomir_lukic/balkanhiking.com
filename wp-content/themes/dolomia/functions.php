<?php

// Widget
require_once get_template_directory() . '/framework/recent_post.php';
require_once get_template_directory() . '/framework/wp_bootstrap_navwalker.php';

// Reqire File Style and JS
function dolomia_load_theme_textdomain() {
load_theme_textdomain( 'dolomia', get_template_directory_uri() . '/languages/' );
}
add_action( 'after_setup_theme', 'dolomia_load_theme_textdomain' );

//Theme Set up:
function dolomia_theme_setup() {

    add_theme_support( 'custom-header' ); 
    add_theme_support( 'custom-background' );
    add_theme_support ('title-tag');
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
    add_theme_support( 'post-formats', 
    array(
        'gallery', 
        'video' , 
    ));
    add_post_type_support( 'portfolio', array( 'comments' ) );
    register_nav_menus( array(
        'primary' => 'Primary Navigation Menu (Use For All Page)',  
    ) );
}
add_action( 'after_setup_theme', 'dolomia_theme_setup' );
if ( ! isset( $content_width ) ) $content_width = 900;


function dolomia_trirong_fonts_url() {
    $font_url = '';
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'dolomia' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Trirong:300,300i,400,400i' ), "https://fonts.googleapis.com/css" );
    }
    return $font_url;
}
function dolomia_montserrat_fonts_url() {
    $font_url = '';
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'dolomia' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Montserrat' ), "https://fonts.googleapis.com/css" );
    }
    return $font_url;
}
function dolomia_open_sans_fonts_url() {
    $font_url = '';
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'dolomia' ) ) {
        $font_url = add_query_arg( 'family=Open+Sans:300,300i,400,400i', urlencode( '' ), "https://fonts.googleapis.com/css" );
    }
    return $font_url;
}
/** Call style & bootstrap **/
function dolomia_theme_scripts_styles(){
     
    /**** Theme Specific CSS ****/
    $protocol = is_ssl() ? 'https' : 'http';
    wp_enqueue_style( 'dolomia-style' , get_stylesheet_uri(), array(),'2017/06/16'); 
    wp_enqueue_style( 'bootstrap-min', get_template_directory_uri() .'/assets/css/bootstrap/bootstrap.min.css');
    wp_enqueue_style( 'bootstrap-theme-min', get_template_directory_uri() .'/assets/css/bootstrap/bootstrap-theme.min.css');

    wp_enqueue_style( 'style', get_template_directory_uri() .'/assets/css/style.css');

    wp_enqueue_style( 'dolomia-trirong-fonts', dolomia_trirong_fonts_url(), array(), '1.0.0' );
    wp_enqueue_style( 'dolomia-montserrat-fonts', dolomia_montserrat_fonts_url(), array(), '1.0.0' );
    wp_enqueue_style( 'dolomia-open-sans-fonts', dolomia_open_sans_fonts_url(), array(), '1.0.0' );

    wp_enqueue_style( 'font-awesome-min', get_template_directory_uri() .'/assets/css/font-awesome.min.css');
    wp_enqueue_style( 'ionicons-min', get_template_directory_uri() .'/assets/css/ionicons.min.css');   
    wp_enqueue_style( 'dolomia-puredesign', get_template_directory_uri() .'/assets/css/puredesign.css');
    wp_enqueue_style( 'flexslider', get_template_directory_uri() .'/assets/css/flexslider.css');
    wp_enqueue_style( 'carousel', get_template_directory_uri() .'/assets/css/owl.carousel.css');
    wp_enqueue_style( 'magnific-popup', get_template_directory_uri() .'/assets/css/magnific-popup.css');
    wp_enqueue_style( 'jquery-fullPage', get_template_directory_uri() .'/assets/css/jquery.fullPage.css');


    // js
    wp_enqueue_script( 'bootstrap-min', get_template_directory_uri().'/assets/js/bootstrap/bootstrap.min.js',array(),false,true);
    wp_enqueue_script( 'flexslider-min', get_template_directory_uri().'/assets/js/jquery.flexslider-min.js',array(),false,true);
    wp_enqueue_script( 'jquery-fullPage', get_template_directory_uri().'/assets/js/jquery.fullPage.min.js',array(),false,true);
    wp_enqueue_script( 'owl-carousel-min', get_template_directory_uri().'/assets/js/owl.carousel.min.js',array(),false,true);
    wp_enqueue_script( 'isotope-min', get_template_directory_uri().'/assets/js/isotope.min.js',array(),false,true);

    wp_enqueue_script( 'api-js', "https://maps.googleapis.com/maps/api/js?key=AIzaSyDpxLVjd9mHpWb7ghOCk7UoVwrb4NBcvaU",array(),null,true);    
    wp_enqueue_script( 'magnific-popup-min', get_template_directory_uri().'/assets/js/jquery.magnific-popup.min.js',array(),false,true);
    wp_enqueue_script( 'scrollTo-min', get_template_directory_uri().'/assets/js/jquery.scrollTo.min.js',array(),false,true);
    wp_enqueue_script( 'jquery-appear', get_template_directory_uri().'/assets/js/jquery.appear.js',array(),false,true);
    wp_enqueue_script( 'jquery-countTo', get_template_directory_uri().'/assets/js/jquery.countTo.js',array(),false,true);
    wp_enqueue_script( 'jquery-scrolly', get_template_directory_uri().'/assets/js/jquery.scrolly.js',array(),false,true);
    wp_enqueue_script( 'plugins-scroll', get_template_directory_uri().'/assets/js/plugins-scroll.js',array(),false,true);
    wp_enqueue_script( 'imagesloaded-min', get_template_directory_uri().'/assets/js/imagesloaded.min.js',array(),false,true);
    wp_enqueue_script( 'pace-min', get_template_directory_uri().'/assets/js/pace.min.js',array(),false,true);
    wp_enqueue_script( 'dolomia-main', get_template_directory_uri().'/assets/js/main.js',array(),false,true);

    // Load the html5 shiv.
    wp_enqueue_script( 'dolomia-html5', "https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js", array(), '3.7.2' );
    wp_script_add_data( 'dolomia-html5', 'conditional', 'lt IE 9' );
     wp_enqueue_script( 'dolomia-html5', "https://oss.maxcdn.com/respond/1.4.2/respond.min.js", array(), '1.4.2' );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

}
add_action( 'wp_enqueue_scripts', 'dolomia_theme_scripts_styles' );

add_filter('clean_url', 'dolomia_so_handle_038', 99, 3);
function dolomia_so_handle_038($url, $original_url, $_context) {
    if (strstr($url, "googleapis.com") !== false) {
        $url = str_replace("&#038;", "&", $url);
    }
    return $url;
}
// Comment Form
function dolomia_theme_comment($comment, $args, $depth) {

    $GLOBALS['comment'] = $comment; ?>
    <div class="comment">
        <div class="row margin-null">
            <div class="col-md-12 padding-leftright-null">
                <div class="custom-img">
                    <?php echo get_avatar($comment,$size='80')?>
                </div>
                <div class="content">
                    <h3>
                        <span class="comment-author">
                            <?php comment_author(); ?>
                        </span>
                        <span class="comment-date">
                            <?php
                            printf( '<time datetime="%2$s">%3$s</time>',
                                    esc_url( get_comment_link( $comment->comment_ID ) ),
                                    get_comment_time( 'c' ),
                                    sprintf( __( '%1$s - %2$s', 'dolomia' ), get_comment_date(), get_comment_time() )
                                ); ?>
                        </span>
                        <span class="comment-btn">
                            <!-- <a href="#">&nbsp;</a> -->
                            <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                          
                        </span>
                    </h3>
                    <p><?php comment_text() ?></p>
                </div>
        </div>
    </div>
    </div>
<?php
}

function dolomia_move_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;

    return $fields;
}
add_filter( 'comment_form_fields', 'dolomia_move_comment_field_to_bottom' );

// Fuction register widgets
function dolomia_widgets_init() {

    // sidebar in single blog
    register_sidebar( array(
        'name'          => esc_html__( 'Primary Sidebar', 'dolomia' ),
        'id'            => 'sidebar-1',
        'class'         => 'widget',
        'description'   => esc_html__( 'Main sidebar that appears on the single blog.', 'dolomia' ),
        'before_widget' => '<div class="widget-wrapper %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
    // sidebar in Footer.
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar for Footer one.', 'dolomia' ),
        'id'            => 'footer-1',
        'class'         => 'widget',
        'description'   => esc_html__( 'Sidebar 1 for Footer.', 'dolomia' ),
        'before_widget' => '<div class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h6 class="heading white margin-bottom-extrasmall">',
        'after_title'   => '</h6>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar for Footer Two.', 'dolomia' ),
        'id'            => 'footer-2',
        'class'         => 'widget',
        'description'   => esc_html__( 'Sidebar 2 for Footer.', 'dolomia' ),
        'before_widget' => '<div class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h6 class="heading white margin-bottom-extrasmall">',
        'after_title'   => '</h6>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar for Footer Three.', 'dolomia' ),
        'id'            => 'footer-3',
        'class'         => 'widget',
        'description'   => esc_html__( 'Sidebar 3 for Footer.', 'dolomia' ),
        'before_widget' => '<div class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h6 class="heading white margin-bottom-extrasmall">',
        'after_title'   => '</h6>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Primary Newsletter ', 'dolomia' ),
        'id'            => 'newsletters_widget',
        'class'         => 'widget',
        'description'   => esc_html__( 'The Newsletter of footer', 'dolomia' ),
        'before_widget' => '<div class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h6 class="heading white margin-bottom-extrasmall">',
        'after_title'   => '</h6>',
    ) );
}
add_action( 'widgets_init', 'dolomia_widgets_init' );

function dolomia_search_form( $form ) {
    $form = '<form class="search-form" action="' . home_url( '/' ) . '" role="search">';
    $form .=    '<div class="form-input">';
    $form .=        '<input type="text" placeholder="Search..." id="s" name="s" value="' . get_search_query() . '" class="form-field black big">';
    $form .=        '<span class="form-button">';
    $form .=            '<button type="submit">';
    $form .=                '<i class="icon ion-ios-search-strong"></i>';
    $form .=            '</button>';
    $form .=        '</span>';
    $form .=    '</div>';
    $form .='</form>';
    return $form;
}
add_filter( 'get_search_form', 'dolomia_search_form' );



// Custom Excerpt
function dolomia_excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'.';
    } else {
        $excerpt = implode(" ",$excerpt);
    } 
    $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
    return $excerpt;
}

function dolomia_get_limit_content($string,$length=20){$string=strip_tags($string);
        if(strlen($string)>0) {$arr=explode(' ',$string);$return='';if(count($arr)>0) {$count=0;if($arr) foreach($arr as $str) {$count+=strlen($str);if($count>$length) {$return.='...'; break;}$return.=' '.$str;}}return $return;}
}


function dolomia_custom_tag_cloud_widget($args) {
    $args['largest'] = 18; //largest tag
    $args['smallest'] = 10; //smallest tag
    $args['unit'] = 'px'; //tag font unit
    return $args;
}
add_filter( 'widget_tag_cloud_args', 'dolomia_custom_tag_cloud_widget' );


function dolomia_pagination() {
     global $wp_query;
 
     if ( $wp_query->max_num_pages > 0 ) { ?>
        <div class="row">
            <div class="col-xs-6">
                <div class="nav-left">                

                    <?php previous_posts_link('<i class="icon ion-ios-arrow-left"></i><span>Older posts</span>') ?>

                </div>
            </div>
            <div class="col-xs-6">
                <div class="nav-right">
                    <?php next_posts_link('<span>Newer posts</span><i class="icon ion-ios-arrow-right"></i></a>') ?>
                </div>
            </div>
        </div>

<?php }
}
add_filter('next_posts_link_attributes', 'dolomia_posts_link_attributes_1');
add_filter('previous_posts_link_attributes', 'dolomia_posts_link_attributes_2');

function dolomia_posts_link_attributes_1() {
    return 'class="btn-alt small margin-null"';
}
function dolomia_posts_link_attributes_2() {
    return 'class="btn-alt small margin-null"';
}


/* Visual Composer */
//if(class_exists('WPBakeryVisualComposerSetup')){
function dolomia_custom_css_classes_for_vc_row_and_vc_column($class_string, $tag) {
    if($tag=='vc_row' || $tag=='vc_row_inner') {
        $class_string = str_replace('vc_row-fluid', '', $class_string);
    }
    return $class_string;
}
// Filter to Replace default css class for vc_row shortcode and vc_column
add_filter('vc_shortcodes_css_class', 'dolomia_custom_css_classes_for_vc_row_and_vc_column', 10, 2); 

if(function_exists('vc_add_param')){

  vc_add_param('vc_row',array(
          "type" => "textfield",
          "heading" => esc_html__('Section HTML', 'dolomia'),
          "param_name" => "el_html",
          "value" => "",
          "description" => esc_html__("Set HTML section", 'dolomia'),   
    ));   
  vc_add_param('vc_row',array(
        "type" => "dropdown",
        "heading" => esc_html__('Fullwidth', 'dolomia'),
        "param_name" => "fullwidth",
        "value" => array(   
                esc_html__('No', 'dolomia') => 'no',  
                esc_html__('Yes', 'dolomia') => 'yes',                                                                                
                ),
        "description" => esc_html__("Select Fullwidth or not", 'dolomia'),      
      ) 
    );
  vc_add_param('vc_row',array(
        "type" => "dropdown",
        "heading" => esc_html__('Show or not Show Div Overlay', 'dolomia'),
        "param_name" => "overlay_div",
        "value" => array(   
                esc_html__('Not Show', 'dolomia') => 'no',  
                esc_html__('Show', 'dolomia') => 'yes',                                                                                
                ),
        "description" => esc_html__("Select Show Div Overlay or not", 'dolomia'),      
      ) 
    );
  vc_add_param('vc_row',array(
        "type" => "attach_image",
        "heading" => esc_html__('Background Image', 'dolomia'),
        "param_name" => "bg_image_custom",
        "description" => esc_html__("Select Fullwidth or not", 'dolomia'),      
      ) 
    );
}




/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.4.0
 * @author     Thomas Griffin <thomasgriffinmedia.com>
 * @author     Gary Jones <gamajo.com>
 * @copyright  Copyright (c) 2014, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/thomasgriffin/TGM-Plugin-Activation
 */
/**
 * Include the TGM_Plugin_Activation class.
 */
require_once get_template_directory() . '/framework/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'dolomia_theme_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
 
 
function dolomia_theme_register_required_plugins() {
    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(   
        // This is an example of how to include a plugin from the WordPress Plugin Repository.
    array(  'name'               => esc_html__('WPBakery Visual Composer','dolomia'),  // The plugin name.
            'slug'               => 'js_composer', // The plugin slug (typically the folder name).
            'source'             => get_template_directory() . '/framework/plugins/js_composer.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),
   
    array(
            'name'               => esc_html__('Theme Common', 'dolomia'), // The plugin name.
            'slug'               => 'theme-common', // The plugin slug (typically the folder name).
            'source'             => get_template_directory() . '/framework/plugins/theme-common.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),
        array(
            'name'      => esc_html__('Newsletter','dolomia'), 
            'slug'      => 'newsletter',
            'required'  => true,
        ),
        array(
            'name'      => esc_html__('Contact Form 7','dolomia'), 
            'slug'      => 'contact-form-7',
            'required'  => true,
        ),
        array(
            'name'      => esc_html__('CMB2','dolomia'), 
            'slug'      => 'cmb2',
            'required'  => true,
        ),
        array(
            'name'      => esc_html__('One Click Demo Import', 'dolomia'),
            'slug'      => 'one-click-demo-import',
            'required'  => true,
        ),
    );
    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => esc_html__( 'Install Required Plugins', 'tgmpa' ),
            'menu_title'                      => esc_html__( 'Install Plugins', 'tgmpa' ),
            'installing'                      => esc_html__( 'Installing Plugin: %s', 'tgmpa' ), // %s = plugin name.
            'oops'                            => esc_html__( 'Something went wrong with the plugin API.', 'tgmpa' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'tgmpa' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'tgmpa' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'tgmpa' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'tgmpa' ),
            'return'                          => esc_html__( 'Return to Required Plugins Installer', 'tgmpa' ),
            'plugin_activated'                => esc_html__( 'Plugin activated successfully.', 'tgmpa' ),
            'complete'                        => esc_html__( 'All plugins installed and activated successfully. %s', 'tgmpa' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );
    tgmpa( $plugins, $config );
}


    function dolomia_add_menu_icons_styles(){
    ?>

        <style>
        #adminmenu .menu-icon-trek div.wp-menu-image:before {
            content: "\f122";
        }
        #adminmenu .menu-icon-portfolio div.wp-menu-image:before {
            content: "\f119";
        }
        </style>

    <?php
    }
    add_action( 'admin_head', 'dolomia_add_menu_icons_styles' );


    //Breadcrumbs

    function dolomia_breadcrumbs() {
       // / === OPTIONS === /
    $text['home']     = esc_html__('Home','dolomia'); // text for the 'Home' link
    $text['category'] = esc_html__('Archive by Category "%s"','dolomia'); // text for a category page
    $text['tax']      = esc_html__('Archive for "%s"','dolomia'); // text for a taxonomy page
    $text['search']   = esc_html__('Search Results for "%s" Query','dolomia'); // text for a search results page
    $text['tag']      = esc_html__('Posts Tagged "%s"','dolomia'); // text for a tag page
    $text['author']   = esc_html__('Articles Posted by %s','dolomia'); // text for an author page
    $text['404']      = esc_html__('Error 404','dolomia'); // text for the 404 page
 
    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $showOnHome  = 1; // 1 - show breadcrumbs on the homepage, 0 - don't show
    $delimiter   = ''; // delimiter between crumbs
    $before      = '<li class="active">'; // tag before the current crumb
    $after       = '</li>'; // tag after the current crumb
    // / === END OF OPTIONS === /
 
    global $post;
    $homeLink = home_url() . '/';
    $linkBefore = '<li>';
    $linkAfter = '</li>';
    $linkAttr = ' rel="v:url" property="v:title"';
    $link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;
 
    if (is_home() || is_front_page()) { 
 
        if ($showOnHome == 1) echo '<ol class="breadcrumb"><li><a href="' . $homeLink . '" class="pathway">' . $text['home'] . '</a></li></ol>';
 
    } else {
 
        echo ' <ol class="breadcrumb">' . sprintf($link, $homeLink, $text['home']) . $delimiter;
 
        
        
        if( is_tax() ){
            $thisCat = get_category(get_query_var('cat'), false);
            if ($thisCat->parent != 0) {
                $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
                $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                echo esc_attr($cats);
            }
            echo $before . sprintf($text['tax'], single_cat_title('', false)) . $after;
        
        }elseif ( is_search() ) {
            echo $before . sprintf($text['search'], get_search_query()) . $after;
 
        } elseif ( is_day() ) {
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
            echo $before . get_the_time('d') . $after;
 
        } elseif ( is_month() ) {
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo $before . get_the_time('F') . $after;
 
        } elseif ( is_year() ) {
            echo $before . get_the_time('Y') . $after;
 
        } elseif ( is_single() && !is_attachment() ) {
            if ( get_post_type() != 'post' ) {
                $post_type = get_post_type_object(get_post_type());
                $slug = $post_type->rewrite;
                printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
                if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
            } else {
                $cat = get_the_category(); $cat = $cat[0];
                $cats = get_category_parents($cat, TRUE, $delimiter);
                if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                echo $cats;
                if ($showCurrent == 1) echo $before . get_the_title() . $after;
            }
 
        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
            $post_type = get_post_type_object(get_post_type());
            echo $before . $post_type->labels->singular_name . $after;
 
        } elseif ( is_attachment() ) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID); $cat = $cat[0];
            $cats = get_category_parents($cat, TRUE, $delimiter);
            $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
            $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
            echo $cats;
            printf($link, get_permalink($parent), $parent->post_title);
            if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
 
        } elseif ( is_page() && !$post->post_parent ) {
            if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
        } elseif ( is_page() && $post->post_parent ) {
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                $parent_id  = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            for ($i = 0; $i < count($breadcrumbs); $i++) {
                echo $breadcrumbs[$i];
                if ($i != count($breadcrumbs)-1) echo $delimiter;
            }
            if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
 
        } elseif ( is_tag() ) {
            echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
 
        } elseif ( is_author() ) {
             global $author;
            $userdata = get_userdata($author);
            echo $before . sprintf($text['author'], $userdata->display_name) . $after;
 
        } elseif ( is_404() ) {
            echo $before . $text['404'] . $after;
        }
 
        if ( get_query_var('paged') ) {
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() );
            if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
        }
 
        echo '</ol>';
 
    }
}

function dolomia_import_files() {
    return array(
        array(
            'import_file_name'           => 'Demo Import Dolomia',
            'import_file_url'            => 'http://themetrademark.com/import/dolomia/dolomia_sample_data.xml',
            'import_widget_file_url'     => 'http://themetrademark.com/import/dolomia/widgets.json',
            'import_notice'              => esc_html__( 'Import data example Dolomia', 'dolomia' ),
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'dolomia_import_files' );



function dolomia_after_import_setup() {
    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Header Menu', 'primary' );

    set_theme_mod( 'nav_menu_locations', array(
            'primary' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home Classic' );
    $blog_page_id  = get_page_by_title( 'Blog Mansory' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'dolomia_after_import_setup' );

function dolomia_custom_styles() {

    $theme_option = get_option('theme_option');  
    $font_family = $theme_option['body-font']['font-family'];  
    wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/assets/css/custom-style.css' );
if(isset($theme_option['main-color'])){
    $bold_headlines = $theme_option['main-color']; 
}else{
    $bold_headlines = '#CBBD9A';
}

if(isset($theme_option['custom-css'])){
        $bold_custom_css = $theme_option['custom-css']; 
    }else{
        $bold_custom_css = '';
    }   
    
    $custom_inline_style = '
    h1.title:after, h2.title:after, h3.title:after, h4.title:after, h5.title:after, h6.title:after {
        border: 2px solid '.$bold_headlines.';
    }

    h1.title.line:after, h2.title.line:after, h3.title.line:after, h4.title.line:after, h5.title.line:after, h6.title.line:after {
      background-color: '.$bold_headlines.';
    }


    h1.small strong, h1.small b, h2.small strong, h2.small b {
      color: '.$bold_headlines.';
    }

    h1 i:after, h2 i:after {
      background-color: '.$bold_headlines.';
    }

    h3.small strong, h3.small b {
      color: '.$bold_headlines.';
    }

    a {
      color: '.$bold_headlines.';
    }

    a.btn:hover {
      background-color: '.$bold_headlines.';
    }

    a.btn-alt {
      background-color: '.$bold_headlines.';
    }

    a.btn-alt.small {
      border: 1px solid '.$bold_headlines.';
      color: '.$bold_headlines.';
    }

    a.btn-alt.small:hover {
      background-color: '.$bold_headlines.';
    }

    a.btn-alt.small.white:hover {
      color: '.$bold_headlines.' !important;
    }

    a.btn-alt.small.white.active {
      color: '.$bold_headlines.' !important;
    }

    a.btn-alt.small.active {
      background-color: '.$bold_headlines.';
    }

    a.btn-alt.small.active:hover {
      color: '.$bold_headlines.' !important;
    }

    a.btn-alt.small.activetwo {
      background-color: '.$bold_headlines.';
    }

    a.btn-alt.small.activetwo:hover {
      color: '.$bold_headlines.' !important;
    }

    a.btn-alt.medium {
      border: 1px solid '.$bold_headlines.';
      color: '.$bold_headlines.';
    }

    a.btn-alt.medium:hover {
      background-color: '.$bold_headlines.';
    }


    a.btn-alt.medium.white:hover {
      color: '.$bold_headlines.' !important;
    }

    a.btn-alt.medium.active {
      background-color: '.$bold_headlines.';
    }

    a.btn-alt.medium.active:hover {
      color: '.$bold_headlines.' !important;
    }

    a.btn-alt.medium.activetwo {
      background-color: '.$bold_headlines.';
    }

    a.btn-alt.medium.activetwo:hover {
      color: '.$bold_headlines.' !important;
    }

    a.btn-alt i {
      margin-right: 8px;
    }

    a.btn-pro {
      color: '.$bold_headlines.';
    }

    a.btn-simple {
      color: '.$bold_headlines.';
    }

    a.btn-simple:before {
      background-color: '.$bold_headlines.';
    }

    a#open-filters:hover {
      color: '.$bold_headlines.' !important;
    }

    .color-background {
      background-color: '.$bold_headlines.';
    }

    i.service.material {
      color: '.$bold_headlines.';
    }


    i.service:hover {
      color: '.$bold_headlines.';
    }

    dl dd {
      border-left: 2px solid '.$bold_headlines.';
    }


    mark {
      background: '.$bold_headlines.';
    }


    code {
      color: '.$bold_headlines.';
    }


    blockquote.heading.post, q.heading.post {
      background-color: '.$bold_headlines.';
    }

    .popup-youtube.simple, .popup-vimeo.simple {
      background-color: '.$bold_headlines.';
    }

    .popup-youtube.simple.small, .popup-vimeo.simple.small {
      border: 1px solid '.$bold_headlines.';
      color: '.$bold_headlines.';
    }

    .popup-youtube.simple.small:hover, .popup-vimeo.simple.small:hover {
      background-color: '.$bold_headlines.';
    }

    .popup-youtube.simple.small.white:hover, .popup-vimeo.simple.small.white:hover {
      color: '.$bold_headlines.' !important;
    }

    .popup-youtube.simple.small.white.active, .popup-vimeo.simple.small.white.active {
      color: '.$bold_headlines.' !important;
    }

    .popup-youtube.simple.small.active, .popup-vimeo.simple.small.active {
      background-color: '.$bold_headlines.';
    }

    .popup-youtube.simple.small.active:hover, .popup-vimeo.simple.small.active:hover {
      color: '.$bold_headlines.' !important;
    }

    .popup-youtube.simple.small.activetwo, .popup-vimeo.simple.small.activetwo {
      background-color: '.$bold_headlines.';
    }

    .popup-youtube.simple.small.activetwo:hover, .popup-vimeo.simple.small.activetwo:hover {
      color: '.$bold_headlines.' !important;
    }

    .popup-youtube.simple.medium, .popup-vimeo.simple.medium {
      border: 1px solid '.$bold_headlines.';
      color: '.$bold_headlines.';
    }

    .popup-youtube.simple.medium:hover, .popup-vimeo.simple.medium:hover {
      background-color: '.$bold_headlines.';
    }


    .popup-youtube.simple.medium.white:hover, .popup-vimeo.simple.medium.white:hover {
      color: '.$bold_headlines.' !important;
    }

    .popup-youtube.simple.medium.active, .popup-vimeo.simple.medium.active {
      background-color: '.$bold_headlines.';
    }

    .popup-youtube.simple.medium.active:hover, .popup-vimeo.simple.medium.active:hover {
      color: '.$bold_headlines.' !important;
    }

    .popup-youtube.simple.medium.activetwo, .popup-vimeo.simple.medium.activetwo {
      background-color: '.$bold_headlines.';
    }

    .popup-youtube.simple.medium.activetwo:hover, .popup-vimeo.simple.medium.activetwo:hover {
      color: '.$bold_headlines.' !important;
    }


    .popup-youtube:before, .popup-vimeo:before {
      background-color: '.$bold_headlines.';
    }

    .popup-youtube.black:hover:before, .popup-vimeo.black:hover:before {
      background-color: '.$bold_headlines.';
    }

    .nav-tabs li a:hover, .nav-tabs li a:focus {
      color: '.$bold_headlines.';
    }

    .nav-tabs li.active a {
      color: '.$bold_headlines.';
    }

    .nav-tabs li.active a:hover, .nav-tabs li.active a:focus {
      color: '.$bold_headlines.';
    }

    .nav-tabs.alt li.active a {
      background-color: '.$bold_headlines.';
    }

    /* core classes */
    ::-moz-selection {
      background: '.$bold_headlines.';
    }

    ::selection {
      background: '.$bold_headlines.';
    }

    /* class color: set font with secondary color */
    [class~="color"] {
      color: '.$bold_headlines.' !important;
    }


    header.transparent.fixed-top .navbar.white #menu .menu-holder ul:not(.sub-menu) > li > a.active-item, header.transparent.fixed-top .navbar.white #sidemenu .menu-holder ul:not(.sub-menu) > li > a.active-item, header.transparent.fixed-top .navbar.white #menu-classic .menu-holder ul:not(.sub-menu) > li > a.active-item {
      color: '.$bold_headlines.';
    }


    header nav.navbar #menu .menu-holder ul li a:hover, header nav.navbar #sidemenu .menu-holder ul li a:hover, header nav.navbar #menu-classic .menu-holder ul li a:hover {
      color: '.$bold_headlines.';
    }


    header nav.navbar #menu .menu-holder ul li.submenu ul.sub-menu, header nav.navbar #sidemenu .menu-holder ul li.submenu ul.sub-menu, header nav.navbar #menu-classic .menu-holder ul li.submenu ul.sub-menu {
      border-top: 2px solid '.$bold_headlines.';
    }

    header nav.navbar #menu .menu-holder ul li.submenu ul.sub-menu li a:hover, header nav.navbar #sidemenu .menu-holder ul li.submenu ul.sub-menu li a:hover, header nav.navbar #menu-classic .menu-holder ul li.submenu ul.sub-menu li a:hover {
      color: '.$bold_headlines.' !important;
    }

    @media screen and (max-width: 991px) {
      header nav.navbar #menu .menu-holder ul li.lang span.current a, header nav.navbar #sidemenu .menu-holder ul li.lang span.current a, header nav.navbar #menu-classic .menu-holder ul li.lang span.current a {
        color: '.$bold_headlines.';
      }
      header nav.navbar #menu .menu-holder ul li.lang span.current a:hover, header nav.navbar #sidemenu .menu-holder ul li.lang span.current a:hover, header nav.navbar #menu-classic .menu-holder ul li.lang span.current a:hover {
        color: '.$bold_headlines.';
      }
    }

    header nav.navbar #menu .menu-holder ul li.lang ul, header nav.navbar #sidemenu .menu-holder ul li.lang ul, header nav.navbar #menu-classic .menu-holder ul li.lang ul {
      background-color: '.$bold_headlines.';
    }


    header nav.navbar #menu .menu-holder#home-menu ul.filters li a.is-checked, header nav.navbar #sidemenu .menu-holder#home-menu ul.filters li a.is-checked, header nav.navbar #menu-classic .menu-holder#home-menu ul.filters li a.is-checked {
      color: '.$bold_headlines.';
    }


    header nav.navbar #menu .menu-holder button.close-menu, header nav.navbar #sidemenu .menu-holder button.close-menu, header nav.navbar #menu-classic .menu-holder button.close-menu {

      color: '.$bold_headlines.';
    }

      header nav.navbar #menu.open .menu-holder ul li a:hover, header nav.navbar #sidemenu.open .menu-holder ul li a:hover, header nav.navbar #menu-classic.open .menu-holder ul li a:hover {
        color: '.$bold_headlines.';
      }
      header nav.navbar #menu.open .menu-holder ul li.submenu ul.sub-menu li a, header nav.navbar #sidemenu.open .menu-holder ul li.submenu ul.sub-menu li a, header nav.navbar #menu-classic.open .menu-holder ul li.submenu ul.sub-menu li a {
        color: '.$bold_headlines.';
      }
      header nav.navbar #menu.sidemenu .menu-holder ul li.submenu ul.sub-menu li a:hover, header nav.navbar #sidemenu.sidemenu .menu-holder ul li.submenu ul.sub-menu li a:hover, header nav.navbar #menu-classic.sidemenu .menu-holder ul li.submenu ul.sub-menu li a:hover {
        color: '.$bold_headlines.' !important;
      }
    }


    header nav.navbar #menu.minimal.open .menu-holder ul li a:hover, header nav.navbar #sidemenu.minimal.open .menu-holder ul li a:hover, header nav.navbar #menu-classic.minimal.open .menu-holder ul li a:hover {
      color: '.$bold_headlines.';
    }


    header nav.navbar #menu.minimal.open .menu-holder ul li.submenu ul.sub-menu li a:hover, header nav.navbar #sidemenu.minimal.open .menu-holder ul li.submenu ul.sub-menu li a:hover, header nav.navbar #menu-classic.minimal.open .menu-holder ul li.submenu ul.sub-menu li a:hover {
      color: '.$bold_headlines.' !important;
    }


    header nav.navbar #menu.minimal.open .menu-holder ul li.lang span.current a, header nav.navbar #sidemenu.minimal.open .menu-holder ul li.lang span.current a, header nav.navbar #menu-classic.minimal.open .menu-holder ul li.lang span.current a {
      color: '.$bold_headlines.';
    }

    header nav.navbar #menu.minimal.open .menu-holder ul li.lang ul, header nav.navbar #sidemenu.minimal.open .menu-holder ul li.lang ul, header nav.navbar #menu-classic.minimal.open .menu-holder ul li.lang ul {
      display: block;
      visibility: visible;
      z-index: 90;
      position: relative;
      top: inherit;
      opacity: 1;
      right: inherit;
      width: auto;
      text-align: left;
      background-color: transparent;
      display: inline-block;
      padding: 0;
      transition: all .4s ease;
    }

    header nav.navbar #menu.minimal.open .menu-holder ul li.lang ul li a, header nav.navbar #sidemenu.minimal.open .menu-holder ul li.lang ul li a, header nav.navbar #menu-classic.minimal.open .menu-holder ul li.lang ul li a {
      margin-left: 0;
      font-family: "Montserrat", sans-serif;
      text-transform: uppercase;
      font-size: 14px;
      letter-spacing: 2px;
      margin-right: 16px;
    }

    header nav.navbar #menu.minimal.open .menu-holder ul a, header nav.navbar #sidemenu.minimal.open .menu-holder ul a, header nav.navbar #menu-classic.minimal.open .menu-holder ul a {
      font-size: 20px;
      line-height: 30px;
      font-family: "Roboto", sans-serif;
      color: white;
      margin-left: 0;
    }

    header nav.navbar #menu.minimal.open .menu-holder button.close-menu, header nav.navbar #sidemenu.minimal.open .menu-holder button.close-menu, header nav.navbar #menu-classic.minimal.open .menu-holder button.close-menu {
      display: block;
    }

    header nav.navbar #menu-classic {
      margin: 25px 0;
    }

    @media screen and (max-width: 991px) {
      header nav.navbar #menu-classic.open {
        position: fixed;
        height: auto;
        max-height: 350px;
        top: 78px;
        background-color: white;
      }
    }

    @media screen and (max-width: 991px) and (max-width: 480px) {
      header nav.navbar #menu-classic.open {
        top: 60px;
      }
    }

    @media screen and (max-width: 991px) {
      
      header nav.navbar #menu-classic.open .menu-holder ul li.submenu ul.sub-menu.mega-sub-menu li div ul li a {
        color: '.$bold_headlines.';
      }
      header nav.navbar #menu-classic.open .menu-holder ul li.submenu.open > a {
        color: '.$bold_headlines.';
      }
      header nav.navbar #menu-classic.open .menu-holder ul li.submenu.open:after {
        color: '.$bold_headlines.';
      }
      header nav.navbar #menu-classic.open .menu-holder ul li.submenu.open ul.sub-menu {
        display: block;
      }
    }

    header nav.navbar #search-box button.close-search-box {
      color: '.$bold_headlines.';
    }


    header nav.navbar.white #menu .menu-holder ul li > a.active-item, header nav.navbar.white #sidemenu .menu-holder ul li > a.active-item, header nav.navbar.white #menu-classic .menu-holder ul li > a.active-item {
      color: '.$bold_headlines.';
    }


    @media screen and (max-width: 991px) {
      header nav.navbar.white #menu .menu-holder ul li.lang span.current a, header nav.navbar.white #sidemenu .menu-holder ul li.lang span.current a, header nav.navbar.white #menu-classic .menu-holder ul li.lang span.current a {
        color: '.$bold_headlines.';
      }
    }


    header nav.navbar.white #menu.open .menu-holder ul li > a.active-item, header nav.navbar.white #sidemenu.open .menu-holder ul li > a.active-item, header nav.navbar.white #menu-classic.open .menu-holder ul li > a.active-item {
      color: '.$bold_headlines.';
    }


    header nav.navbar.white #sidemenu.open .menu-holder ul li > a.active-item {
      color: '.$bold_headlines.';
    }


    #page-content #flexslider ul li .text h2, #page-content #flexslider-nav ul li .text h2 {
      color: '.$bold_headlines.';
    }


    #page-content #projects ul.filters li.is-checked, #page-content #projects-filters ul.filters li.is-checked, #page-content #masonry-filters ul.filters li.is-checked, #page-content #showcase-treks ul.filters li.is-checked, #page-content #gallery ul.filters li.is-checked {
      color: '.$bold_headlines.';
    }

    #page-content #projects ul.filters li:hover, #page-content #projects-filters ul.filters li:hover, #page-content #masonry-filters ul.filters li:hover, #page-content #showcase-treks ul.filters li:hover, #page-content #gallery ul.filters li:hover {
      color: '.$bold_headlines.';
    }


    #page-content #projects ul.filters.light li.is-checked, #page-content #projects-filters ul.filters.light li.is-checked, #page-content #masonry-filters ul.filters.light li.is-checked, #page-content #showcase-treks ul.filters.light li.is-checked, #page-content #gallery ul.filters.light li.is-checked {
      color: '.$bold_headlines.';
    }

    #page-content #projects ul.filters.light li:hover, #page-content #projects-filters ul.filters.light li:hover, #page-content #masonry-filters ul.filters.light li:hover, #page-content #showcase-treks ul.filters.light li:hover, #page-content #gallery ul.filters.light li:hover {
      color: '.$bold_headlines.';
    }


    #page-content #projects .projects-items .one-item .content span.icon, #page-content #projects .masonry-items .one-item .content span.icon, #page-content #projects-filters .projects-items .one-item .content span.icon, #page-content #projects-filters .masonry-items .one-item .content span.icon, #page-content #masonry-filters .projects-items .one-item .content span.icon, #page-content #masonry-filters .masonry-items .one-item .content span.icon, #page-content #showcase-treks .projects-items .one-item .content span.icon, #page-content #showcase-treks .masonry-items .one-item .content span.icon, #page-content #gallery .projects-items .one-item .content span.icon, #page-content #gallery .masonry-items .one-item .content span.icon {
      background-color: '.$bold_headlines.';
    }


    #page-content .section-text a.btn-pro {
      color: '.$bold_headlines.';
    }

    #page-content #news .single-news article span.category {
      color: '.$bold_headlines.';
    }


    #page-content #news .single-news:hover article h3 {
      color: '.$bold_headlines.';
    }

    #page-content #showcase-projects .item .showcase-project .content .meta h3, #page-content #showcase-projects .item .showcase-trek .content .meta h3, #page-content #showcase-treks .item .showcase-project .content .meta h3, #page-content #showcase-treks .item .showcase-trek .content .meta h3 {
      color: '.$bold_headlines.';
    }


    #page-content #showcase-projects .item .showcase-project span.read, #page-content #showcase-projects .item .showcase-trek span.read, #page-content #showcase-treks .item .showcase-project span.read, #page-content #showcase-treks .item .showcase-trek span.read {
      background-color: '.$bold_headlines.';
    }


    #page-content #showcase-projects .item:hover .showcase-project h6, #page-content #showcase-projects .item:hover .showcase-trek h6, #page-content #showcase-treks .item:hover .showcase-project h6, #page-content #showcase-treks .item:hover .showcase-trek h6 {
      color: '.$bold_headlines.' !important;
    }

    #page-content #team .single-person .content ul.social li a {
      background-color: '.$bold_headlines.';
    }

    aside.sidebar form .form-input span.form-button {
      background-color: '.$bold_headlines.';
    }


    aside.sidebar ul.recent-posts li a:hover p {
      color: '.$bold_headlines.';
    }


    aside.sidebar ul.recent-posts li .category {
      color: '.$bold_headlines.';
    }

    aside.sidebar ul.widget-categories li a:hover {
      color: '.$bold_headlines.';
    }


    aside.sidebar ul.tagCloud li a.popular {
      color: '.$bold_headlines.';
    }

    aside.sidebar ul.tagCloud li a:hover {
      color: '.$bold_headlines.';
    }


    .content-section ul.services li .icon {

      background-color: '.$bold_headlines.';
    }

    .content-section ul.services li:hover .icon-text p {
      color: '.$bold_headlines.';
    }

    #post-wrap span.category {
      color: '.$bold_headlines.';
    }

    #post-wrap span.category:after {
      color: '.$bold_headlines.';
    }

    #post-wrap ul.post li:before {
      color: '.$bold_headlines.';
    }


    #post-wrap #post-meta.project ul.tagCloud li a, #post-wrap #project-meta.project ul.tagCloud li a {
      background-color: '.$bold_headlines.';
    }


    #post-wrap #post-meta ul.tagCloud li a, #post-wrap #project-meta ul.tagCloud li a {
      color: '.$bold_headlines.';
      border: 1px solid '.$bold_headlines.';
    }

    #comments .comment span.comment-btn a {
      color: '.$bold_headlines.';
    }

    #comments .comment a {
      color: '.$bold_headlines.';
    }

    .project-images .item .content .social ul li a, .team-profiles .item .content .social ul li a {
      background-color: '.$bold_headlines.';
    }


    .project-alt ul li a:hover {
      color: '.$bold_headlines.';
    }

    #contact-form #submit-contact.btn-alt, #search-form #submit-contact.btn-alt {
      border: 1px solid '.$bold_headlines.';
      color: '.$bold_headlines.';
    }

    #contact-form #submit-contact.btn-alt:hover, #search-form #submit-contact.btn-alt:hover {
      background-color: '.$bold_headlines.';
    }

    #contact-form #submit-contact.btn-alt.white:hover, #search-form #submit-contact.btn-alt.white:hover {
      color: '.$bold_headlines.' !important;
    }

    #contact-form #submit-contact.btn-alt.active, #search-form #submit-contact.btn-alt.active {
      background-color: '.$bold_headlines.';
    }

    #contact-form #submit-contact.btn-alt.active:hover, #search-form #submit-contact.btn-alt.active:hover {
      color: '.$bold_headlines.' !important;
    }


    #contact-form.color ::-webkit-input-placeholder, #search-form.color ::-webkit-input-placeholder {
      color: '.$bold_headlines.';
    }

    #contact-form.color :-moz-placeholder, #search-form.color :-moz-placeholder {
      color: '.$bold_headlines.';
    }

    #contact-form.color ::-moz-placeholder, #search-form.color ::-moz-placeholder {
      color: '.$bold_headlines.';
    }

    #contact-form.color :-ms-input-placeholder, #search-form.color :-ms-input-placeholder {
      color: '.$bold_headlines.';
    }


    span.contact-info a em {
      color: '.$bold_headlines.';
    }

    #contact-newsletter #submit-newsletter.btn-alt {
      border: 1px solid '.$bold_headlines.';
      color: '.$bold_headlines.';
    }

    @media screen and (max-width: 480px) {
      #contact-newsletter #submit-newsletter.btn-alt {
        border-radius: 40px;
      }
    }

    #contact-newsletter #submit-newsletter.btn-alt:hover {
      background-color: '.$bold_headlines.';
    }


    #contact-newsletter #submit-newsletter.btn-alt.white:hover {
      color: '.$bold_headlines.' !important;
    }

    #contact-newsletter #submit-newsletter.btn-alt.color {
      border: 1px solid '.$bold_headlines.';
      color: '.$bold_headlines.';
    }

    #contact-newsletter #submit-newsletter.btn-alt.color.active {
      background-color: '.$bold_headlines.';
    }

    #contact-newsletter #submit-newsletter.btn-alt.color.active:hover {
      color: '.$bold_headlines.' !important;
      border-left: 1px solid '.$bold_headlines.';
    }

    @media screen and (max-width: 410px) {
      #contact-newsletter #submit-newsletter.btn-alt.color.active:hover {
        border: 1px solid '.$bold_headlines.';
      }
    }

    footer .copy a:hover, .footer .copy a:hover {
      color: '.$bold_headlines.';
    }

    footer ul.social li a, .footer ul.social li a {
      background-color: '.$bold_headlines.';
    }


    #pricing .price ul.style li:before {
      color: '.$bold_headlines.';
    }


    .mycolors span.mycolor {
      background-color: '.$bold_headlines.';
    }


    .testimonials-carousel span.quote, .testimonials-carousel-simple span.quote {
      color: '.$bold_headlines.';
    }


    .testimonials-carousel-simple blockquote.grey-light:before, .testimonials-carousel-simple blockquote.white:before {
      color: '.$bold_headlines.';
    }


    .table-responsive.shadow:hover {
      border: 1px solid '.$bold_headlines.';
    }

    .alert.alert-info {
      border: 2px solid '.$bold_headlines.';
    }

    #skills ul.skill-list li span.border-color {

      background: '.$bold_headlines.';
      background-color: '.$bold_headlines.';
    }

    #skills ul.skill-list li span.label {

      background-color: '.$bold_headlines.';
    }


    #skills.white ul.skill-list li span.label {
      color: '.$bold_headlines.';
    }


    .image-carousel .owl-controls .owl-nav .owl-prev span, .image-carousel .owl-controls .owl-nav .owl-next span {

      background-color: '.$bold_headlines.';
    }

    #newsletter-form form .form-input span.form-button {
      background-color: '.$bold_headlines.';
    }


    .hiker ul.social li a {
      background-color: '.$bold_headlines.';
    }

    .gallery-meta span.like i {
      color: '.$bold_headlines.';
    }


    .gallery-meta ul.tags li:after {
      color: '.$bold_headlines.';
    }

    button.mfp-close {
      background-color: '.$bold_headlines.' !important;
    }

    .owl-theme .owl-dots .owl-dot span {
      border: 1px solid '.$bold_headlines.';
    }

    .owl-theme .owl-dots .owl-dot.active span {
      background: '.$bold_headlines.';
    }
    '.$bold_custom_css.'


    ';

    wp_add_inline_style( 'custom-style', $custom_inline_style );
}
add_action( 'wp_enqueue_scripts', 'dolomia_custom_styles' );


// Remove WP Version From Styles
add_filter( 'style_loader_src', 'sdt_remove_ver_css_js', 9999 );
// Remove WP Version From Scripts
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999 );

// Function to remove version numbers
function sdt_remove_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
        return $src;
}
?>