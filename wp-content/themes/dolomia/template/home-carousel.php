<?php 
    /*
    *Template Name: Home Carousel
    *
    */
        
    get_header('carousel');
    ?>
    <div id="page-content" class="padding-onlybottom-sm">
        <?php   
            if(have_posts()):
                while ( have_posts() ) : the_post();
                    the_content();
                endwhile; 
            endif;?>
    </div>
<?php
    get_footer('fullwidth');
    
 ?>