<?php 
    /*
    *Template Name: Blog Classic
    *
    */
    get_header();

    $theme_option = get_option('theme_option');

    $page_header_title    = get_post_meta( get_the_ID(), 'page_header_title', true );
    $page_header_subtitle = get_post_meta( get_the_ID(), 'page_header_subtitle', true );
    $page_header_image_background  = get_post_meta( get_the_ID(), 'page_header_image_background', 1 );

 ?>


<!--  Page Content, class footer-fixed if footer is fixed  -->
            <div id="page-content" class="header-static footer-fixed">
                <!--  Slider  -->
                <div id="flexslider" class="fullpage-wrap small">
                    <ul class="slides">
                        <li <?php if(isset($theme_option['index_page_image_background'])){ ?>
                            style="background-image: url(<?php echo esc_url($theme_option['index_page_image_background']['url']); ?>)"  <?php }?> >
                            <div class="container text text-center">
                                <h1 class="white margin-bottom-small">
                                    <?php 
                                        if (isset($theme_option['index_page_title']) && !empty($theme_option['index_page_title'])) {
                                            echo esc_attr($theme_option['index_page_title']); 
                                        }
                                    ?>
                                </h1> 
                                <p class="heading white">
                                    <?php 
                                        if (isset($theme_option['index_page_subtitle']) && !empty($theme_option['index_page_subtitle'])) {
                                            echo esc_attr($theme_option['index_page_subtitle']); 
                                        } 
                                    ?>
                                </p>
                            </div>
                            <div class="gradient dark"></div>
                        </li>
                        <?php echo dolomia_breadcrumbs(); ?>
                    </ul>
                </div>
                <!--  END Slider  -->
                <div id="home-wrap" class="content-section fullpage-wrap row grey-background">
                    <div class="container">
                        <div class="col-md-8 padding-leftright-null">
                            <!--  News Section  -->
                            <section id="news" class="page">
                                <div class="news-items equal one-columns">
                                <?php
                                    $args = array( 'post_type' => 'post','paged' => $paged, );
                                    $wp_query = new WP_Query( $args );
                                    if(have_posts()):
                                        while ( have_posts() ) : the_post();
                                            $format_of_post = get_post_format();
                                                if ( false === $format_of_post ) {
                                                        $format_of_post = 'standard';
                                                }
                                ?>
                                    <div class="single-news one-item horizontal-news">
                                        <article>
                                        <?php 
                                            $url_img = get_the_post_thumbnail_url(get_the_ID(),'full');

                                        ?>
                                            <div class="col-md-6 padding-leftright-null">
                                                <div class="image" style="background-image:url(<?php echo esc_url($url_img); ?>)"></div>
                                            </div>
                                            <div class="col-md-6 padding-leftright-null">
                                                <div class="content">
                                                    <h3><?php the_title(); ?></h3>
                                                    <span class="date"><?php the_time('m.d.Y'); ?></span>
                                                    <p><?php echo dolomia_excerpt(20); ?></p>
                                                    <?php 
                                                        // GET Category in post
                                                        $categories = get_the_category();
                                                        if ( ! empty( $categories ) ) {
                                                            foreach ($categories as $category) {    
                                                            echo '<span class="category">'.$category->name.'</span>';
                                                            }
                                                        }
                                                    ?>
                                                    
                                                    
                                                </div>
                                            </div>
                                            <a href="<?php the_permalink(); ?>" class="link"></a>
                                        </article>
                                    </div>
                                <?php 
                                        endwhile;
                                    else:
                                        echo esc_html__('No content found','dolomia');
                                    endif; 
                                ?>
                                </div>
                            </section>
                            <!--  END News Section  -->
                        </div>
                        <!--  Right Sidebar  -->
                        <div class="col-md-4 text">
                            <aside class="sidebar">
                                <div class="widget-wrapper">
                                    <?php if ( is_active_sidebar( 'sidebar-1' ) ) : 
                                      dynamic_sidebar('sidebar-1');
                                      endif;
                                    ?>
                                </div>
                            </aside>
                        </div>
                        <!--  END Right Sidebar  -->
                        <div class="row margin-leftright-null">
                            <!--  Navigation  -->
                            <section id="nav" class="padding-top-null grey-background">
                                <?php dolomia_pagination();  ?>
                            </section>
                            <!--  END Navigation  -->
                        </div>
                    </div>
                </div>
            </div>
            <!--  END Page Content, class footer-fixed if footer is fixed  -->

<?php get_footer(); ?>