<?php 
    /*
    *Template Name: Gallery
    *
    */
    
    get_header();

    $theme_option = get_option('theme_option');

    $page_header_title    = get_post_meta( get_the_ID(), 'page_header_title', true );
    $page_header_subtitle = get_post_meta( get_the_ID(), 'page_header_subtitle', true );
    $page_header_image_background  = get_post_meta( get_the_ID(), 'page_header_image_background', 1 );

    ?>
    <div id="page-content" class="header-static footer-fixed">
         <!--  Slider  -->
        <div id="flexslider" class="fullpage-wrap small">
            <ul class="slides">
                <li style="background-image:url(<?php echo esc_url($page_header_image_background); ?>)">
                <div class="container text text-center">
                    <h1 class="white margin-bottom-small"><?php echo esc_attr($page_header_title); ?></h1> 
                    <p class="heading white"><?php echo esc_attr($page_header_subtitle); ?></p>
                </div>
                <div class="gradient dark"></div>
            </li>
            <?php echo dolomia_breadcrumbs(); ?>
            </ul>
        </div>

        <?php   
            if(have_posts()):
                while ( have_posts() ) : the_post();
                    the_content();
                endwhile; 
            endif;?>
        
    </div>
<?php
    get_footer();
    
 ?>