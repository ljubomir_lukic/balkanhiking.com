<?php 
    /*
    *Template Name: Home Classic
    *
    */
    
    get_header();

    $theme_option = get_option('theme_option');
    
    $slider_nav    = get_post_meta( get_the_ID(), 'slider_nav', true );
    ?>
    <div id="page-content" class="header-static footer-fixed">
        <!--  Slider  -->

        <div id="flexslider-nav" class="fullpage-wrap small">
            <ul class="slides">
            <?php 
                foreach ($slider_nav as $key => $entrys ) {
                    
                    if ( isset( $entrys['slider_img'] ) )
                        $slider_img = esc_url($entrys['slider_img']);
                    if ( isset( $entrys['slider_title'] ) )
                        $slider_title =  $entrys['slider_title'] ;
                    if ( isset( $entrys['slider_subtitle'] ) )
                        $slider_subtitle =  $entrys['slider_subtitle'] ;
                    if ( isset( $entrys['slider_btn'] ) )
                        $slider_btn =  $entrys['slider_btn'] ;
                    if ( isset( $entrys['slider_btn_link'] ) )
                        $slider_btn_link =  $entrys['slider_btn_link'] ; 
                ?>
                <li style="background-image:url(<?php echo esc_url($slider_img); ?>)">
                    <div class="container text">
                        <h1 class="white flex-animation"><?php echo htmlspecialchars_decode($slider_title) ?></h1>
                        <h2 class="white flex-animation"><?php echo htmlspecialchars_decode($slider_subtitle) ?></h2>
                        <a href="<?php echo esc_attr($slider_btn_link) ?>" class="shadow btn-alt small activetwo margin-bottom-null flex-animation"><?php echo esc_attr($slider_btn) ?></a>
                    </div>
                    <div class="gradient dark"></div>
                </li>
                <?php
            
                }
                ?>               
            </ul>
            <div class="slider-navigation">
                <a href="#" class="flex-prev"><i class="icon ion-minus"></i></a>
                <div class="slider-controls-container"></div>
                <a href="#" class="flex-next"><i class="icon ion-minus"></i></a>
            </div>
        </div>
        <!--  END Slider  -->

        <div id="home-wrap" class="content-section fullpage-wrap">
        <?php   
            if(have_posts()):
                while ( have_posts() ) : the_post();
                    the_content();
                endwhile; 
            endif;?>
        </div>
    </div>
<?php
    get_footer();
    
 ?>