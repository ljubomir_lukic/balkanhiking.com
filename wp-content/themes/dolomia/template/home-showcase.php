<?php 
    /*
    *Template Name: Home Showcase
    *
    */
        
    get_header('showcase');
    ?>
    <?php $theme_option = get_option('theme_option');  ?>
    <div id="page-content" class="padding-onlybottom-sm">
        <div id="fullpage" class="full-width">
        <?php   
            if(have_posts()):
                while ( have_posts() ) : the_post();
                    the_content();
                endwhile; 
            endif;?>

        <!-- </div> -->
            <!-- <div class="section fp-auto-height footer"> -->
                        <!-- <div class="container"> -->
                            
                        <!-- </div> -->
                    <!-- </div> -->
                    <!-- END Footer -->
                </div>
                <!--  END Vertical slider  -->
            </div>
            <!--  END Page Content, class footer-fixed if footer is fixed  -->
    
<?php
    get_footer('showcase');
    ?>
