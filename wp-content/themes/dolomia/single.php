<?php 
    // Get header layout
    get_header();
    // Define variable

    if(have_posts()):
        while ( have_posts() ) : the_post();
        $format_of_post = get_post_format();
             if ( false === $format_of_post ) {
                    $format_of_post = 'standard';
            }  
        $header_imagebg  = get_post_meta( get_the_ID(), 'header_image_background', 1 );
        $header_title    = get_post_meta( get_the_ID(), 'header_title', true );
        $header_subtitle = get_post_meta( get_the_ID(), 'header_subtitle', true );
        $link_video = get_post_meta( get_the_ID(), 'url_youtube', true );
        ?>

        <!--  Page Content, class footer-fixed if footer is fixed  -->
<?php if ( is_active_sidebar( 'footer-1' ) ||  is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) || is_active_sidebar( 'footer-4' )){ ?>
    <div id="page-content" class="header-static footer-fixed">
<?php }else{ ?>
    <div id="page-content" class="header-static footer-fixed page-content-nonfooter">
<?php } ?>
            <?php if(isset($header_imagebg)){ ?>
            <!--  Slider  -->
            <div id="flexslider" class="fullpage-wrap small">
                <ul class="slides">
                    <li style="background-image:url(<?php echo esc_url($header_imagebg); ?>)">
                        <div class="container text text-center">
                            <h1 class="white margin-bottom-small"><?php echo esc_attr($header_title); ?></h1> 
                            <p class="heading white"><?php echo esc_attr($header_subtitle); ?></p>
                        </div>
                        <div class="gradient dark"></div>
                    </li>
                    <?php echo dolomia_breadcrumbs(); ?>
                </ul>
            </div>
            <?php }else{ ?>
            <!--  Slider  -->                
            <div id="flexslider" class="flexslider-custom fullpage-wrap">
                <ul class="slides slides-custom">
                    <li <?php if(isset($theme_option['index_page_image_background'])){ ?>
                        style="background-image: url(<?php echo esc_url($theme_option['index_page_image_background']['url']); ?>)"  <?php }?> >
                        
                        <div class="gradient dark"></div>
                    </li>
                </ul>
            </div>
            <!--  END Slider  -->
            <?php } ?>
            <div id="post-wrap" class="content-section fullpage-wrap">
                <div class="row margin-leftright-null">
                   
                    <?php
                    if($format_of_post == 'gallery'){ ?>
                    <div class="container text">
                        <div class="col-md-12 padding-leftright-null text-center">
                            <h2 class="margin-bottom-null title simple left"><?php the_title(); ?></h2>
                            <?php 
                                // GET Category in post
                                $categories = get_the_category();
                                if ( ! empty( $categories ) ) {
                                    foreach ($categories as $category) {    
                                    echo '<span class="category">'.$category->name.'</span>';
                                    }
                                }
                            ?>
                            <span class="date"><?php the_date(); ?></span>
                        </div>
                            <!--  END Post Meta  -->
                    </div>

                    <!-- gallery post -->
                    <div class="row margin-null">
                        <div class="col-md-12 padding-leftright-null post-gallery">
                        <?php 
                        $gallery = get_post_meta( get_the_ID(), 'gallery', true );
                            foreach ($gallery as $gallery_id) { ?>
                                <div class="item">
                                    <img class="img-responsive" src="<?php echo esc_url($gallery_id);?>" alt="">
                                </div> 
                        <?php
                        }
                        ?>                                                                                
                        </div>
                    </div>
                    <!-- end gallery post --> 
                    <div class="container text">

                         <?php
                            the_content(); 
                            wp_link_pages();
                        ?>
                    </div>
                    <?php 
                    }
                    ?>
                     
                     <!-- standard post -->
                    <?php
                    if($format_of_post == 'standard'){ ?>
                        <div class="container text">
                            <div class="col-md-12 padding-leftright-null text-center">
                                <h2 class="margin-bottom-null title simple left"><?php the_title(); ?></h2>
                                <?php 
                                    // GET Category in post
                                    $categories = get_the_category();
                                    if ( ! empty( $categories ) ) {
                                        foreach ($categories as $category) {    
                                        echo '<span class="category">'.$category->name.'</span>';
                                        }
                                    }
                                ?>
                                <span class="date"><?php the_date(); ?></span>
                            </div>
                            <!--  END Post Meta  -->
                            <div class="col-md-12 padding-onlytop-md padding-leftright-null">
                                <section class="grid-images padding-sm padding-md-bottom-null">
                                    <div class="row">
                                    <?php if( has_post_thumbnail() ){ ?>
                                        <div class="image simple-shadow" style="background-image:url(<?php the_post_thumbnail_url(); ?>);?>">
                                            <a class="lightbox-image" href="<?php the_post_thumbnail_url(); ?>"></a>
                                        </div>      
                                    <?php } ?>                                                                                            
                                    </div>
                                </section>

                            <?php
                                the_content(); 
                                wp_link_pages();
                            ?>
                            </div>
                        </div>
                    <?php 
                    }
                    ?>

                    <!-- video post -->
                    <?php
                    if($format_of_post == 'video'){ ?>
                        <div class="container text">
                            <div class="col-md-12 padding-leftright-null text-center">
                                <h2 class="margin-bottom-null title simple left"><?php the_title(); ?></h2>
                                <?php 
                                    // GET Category in post
                                    $tags = get_the_tags();
                                    if ( ! empty( $tags ) ) {
                                        foreach ($tags as $tag) {    
                                        echo '<span class="category">'.$tag->name.'</span>';
                                        }
                                    }
                                ?>
                                <span class="date"><?php the_date(); ?></span>
                            </div>
                            <!--  END Post Meta  -->
                            <div class="row content-post no-margin">
                                <div class="col-md-12 padding-leftright-null padding-sm">
                                    <a class="popup-youtube" href="<?php echo esc_url($link_video); ?>">
                                        <div class="bg-img static-height" style="background-image:url(<?php the_post_thumbnail_url( ); ?>)"></div>
                                    </a>
                                </div>

                            <?php
                                the_content(); 
                                wp_link_pages();
                            ?>
                            </div>
                        </div>
                    <?php 
                    }
                    ?>
                            
                    <div class="container text">
                    <?php if( !empty($current_user->description)){ ?>
                        <!--  Author Meta  -->
                        <div class="row no-margin">
                            <div class="col-md-12 padding-leftright-null">
                                <div id="post-meta">
                                    <?php echo get_avatar( get_the_author_meta( 'ID' ), 32 );?>
                                    <div class="author">
                                        <h3><?php the_author(); ?></h3>
                                        <p><?php echo esc_attr($current_user->description);?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  END Author Meta  -->
                        <?php } ?>

                        <?php if( isset($theme_option['social_button']) && $theme_option['social_button'] == 1){ ?>
                        <!--  Share Btn  -->
                        <div id="share">
                            <a class="share-btn">
                                <i class="icon ion-android-share-alt"></i>
                            </a>
                            <div class="share-icons" style="display:none">
                                <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" class="share-google" target="_blank">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                                <a href="http://www.twitter.com/share?url=<?php the_permalink(); ?>" class="share-twitter" target="_blank">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                                
                                <a href="https://www.linkedin.com/cws/share?url=<?php the_permalink(); ?>" class="share-linkedin" target="_blank">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                                <a href="mailto:example@email.com?subject=I wanted you to see this site&body=Check out this site <?php the_permalink(); ?>" class="share-mail">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                        <!--  END Share Btn  -->
                        <?php } ?>
                        <!--  Comments  -->
                        <div class="row no-margin">
                            <div class="col-md-12 padding-leftright-null">
                                <div id="comments">
                                <?php // if ( ! comments_open() && get_comments_number() ) : ?>
                                    <?php comments_template(); ?>
                                <?php //endif; ?>
                                </div>
                            </div>
                        </div>
                        <!--  END Comments  -->
                    </div>                     
                </div>
            </div>
        </div>

    <!--  END Page Content, class footer-fixed if footer is fixed  -->

<?php endwhile;
     else:
        echo esc_html__('No content found','dolomia');
 endif; ?>
<!-- Get footer layout -->
<?php get_footer();?>