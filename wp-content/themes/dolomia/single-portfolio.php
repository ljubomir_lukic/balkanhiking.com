
<?php get_header(); ?>

<!--  Page Content, class footer-fixed if footer is fixed  -->
<?php 
     if(have_posts()):
        while ( have_posts() ) : the_post();  
        $theme_option = get_option('theme_option');

        $project_title    = get_post_meta( get_the_ID(), 'project_title', true );
        $project_subtitle = get_post_meta( get_the_ID(), 'project_subtitle', true );
        $project_header_image_background  = get_post_meta( get_the_ID(), 'project_header_image_background', 1 );

        $project_name = get_post_meta( get_the_ID(), 'project_name', true );
        $project_subname = get_post_meta( get_the_ID(), 'project_subname', true );
        $project_description = get_post_meta( get_the_ID(), 'project_description', true );
        $project_location = get_post_meta( get_the_ID(), 'project_location', true );
        $project_like = get_post_meta( get_the_ID(), 'project_like', true );

       // $format = get_post_format($post->ID);

    ?>

    <div id="page-content" class="header-static footer-fixed">
        <!--  Slider  -->
        <div id="flexslider" class="fullpage-wrap small">
            <ul class="slides">
                <li style="background-image:url(<?php echo esc_url($project_header_image_background); ?>)">
                <div class="container text text-center">
                    <h1 class="white margin-bottom-small"><?php echo esc_attr($project_title); ?></h1> 
                    <p class="heading white"><?php echo esc_attr($project_subtitle); ?></p>
                </div>
                <div class="gradient dark"></div>
            </li>
            <?php echo dolomia_breadcrumbs(); ?>
            </ul>
        </div>

        <!--  END Slider  -->
        <div id="post-wrap" class="content-section fullpage-wrap">
            <!-- Section same Height. Child get the parent Height. Set the same id -->
            <div class="row margin-leftright-null">
                <div class="container">
                    <div class="col-md-6 padding-leftright-null">
                        <div data-respo
                        nsive="parent-height" data-responsive-id="project">
                            <div class="col-md-12 text">
                                <h2 class="margin-bottom-null title simple left"><?php echo esc_attr($project_name); ?></h2>
                                <p class="heading center grey margin-bottom-null"><?php echo esc_attr($project_subname); ?></p>
                                <div class="padding-onlytop-md">
                                    <!-- Project Meta -->
                                    <div class="gallery-meta">
                                        <div class="col-md-8 padding-leftright-null">
                                            <span class="info"><em><?php echo esc_html__('Location','dolomia'); ?></em> <?php echo esc_attr($project_location) ?></span>
                                            <span class="info"><em><?php echo esc_html__('Year','dolomia'); ?></em> <?php the_time('Y'); ?></span>
                                            <ul class="tags">

                                                <?php

                                                    $skill = array(
                                                        'style'     => 'list',
                                                        'post_type' => 'portfolio',
                                                        'orderby'   => 'name',
                                                        'hierarchical'  => 1,
                                                        'taxonomy' =>'skill',
                                                    );

                                                     $tax_skill = get_categories($skill);
                                                    if( isset($tax_skill) && $tax_skill != ''){
                                                    foreach ($tax_skill as $tax_skills){ ?>

                                                        <li><a href=""><?php echo esc_attr($tax_skills->name); ?></a></li>

                                                    <?php }
                                                    } ?>                                                     
                                            </ul>
                                        </div>
                                        <div class="col-md-4 padding-leftright-null">
                                            <span class="like"><a href=""><?php echo esc_attr($project_like); ?><i class="icon ion-android-favorite-outline"></i></a></span>
                                        </div>
                                    </div>
                                    <!-- END Project Meta -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 padding-leftright-null">
                        <div data-responsive="child-height" data-responsive-id="project" class="height-auto-lg">
                            <!-- Project Description -->
                            <div class="col-md-12 padding-lg-top-null text">
                                <p class="padding-onlytop-md padding-lg-top-null margin-bottom-null"><?php the_content(); ?></p>
                            </div>
                            <!-- END Project Meta -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Section same Height. Child get the parent Height. Set the same id -->

            <?php 

            // Project 1
            if(get_post_meta(get_the_ID(),'display_type', true)== 'standard'){ ?>

            <div class="row margin-leftright-null">
                <div class="container text padding-md-topbottom-null">
                    <!-- Project Images -->
                    <section class="grid-images padding-top-null">
                        <div class="row">
                    <?php 
                       $portfolio_group = get_post_meta( get_the_ID(),'portfolio_img', true );
                          if(isset($portfolio_group) && $portfolio_group != ''){
                          foreach ( (array) $portfolio_group as $key => $entrys ) {
                              $image_portfolio_gr = $number_column_portfolio = '';
                              if ( isset( $entrys['image_portfolio_gr'] ) )
                                  $image_portfolio_gr =  $entrys['image_portfolio_gr'] ;
                              if ( isset( $entrys['number_column_portfolio'] ) )
                                  $number_column_portfolio =  $entrys['number_column_portfolio'] ; 

                     ?>
                         <?php if( $number_column_portfolio == 1 ){ ?>
                                    <div class="col-md-<?php echo esc_attr($number_column_portfolio); ?>">
                                        <div class="image simple-shadow" style="background-image:url(<?php echo esc_url($image_portfolio_gr); ?>)">
                                            <a class="lightbox-image" href="<?php echo esc_url($image_portfolio_gr); ?>"></a>
                                        </div>
                                    </div>
                                <?php }
                                else{ ?>
                                    <div class="col-md-<?php echo esc_attr($number_column_portfolio); ?> padding-onlytop-sm">
                                        <div class="image simple-shadow" style="background-image:url(<?php echo esc_url($image_portfolio_gr); ?>)">
                                            <a class="lightbox-image" href="<?php echo esc_url($image_portfolio_gr); ?>"></a>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php }

                                } ?>
                        
                        </div>
                    </section>
                    <!-- END Project Images -->
                </div>
            </div>

            <?php } 
            // End project 1
            
            // Project 2
            else{ ?>
            <div class="row margin-leftright-null">
                <div class="col-md-12 padding-md post-gallery padding-md-topbottom-null">
                <?php 
                    $gallery = get_post_meta( get_the_ID(), 'gallery_portfolio', true ); 
                    foreach ($gallery as $id) {
                ?>
                    <div class="item">
                        <img class="img-responsive" src="<?php echo esc_url($id);?>" alt="">
                    </div>
                <?php } 
                ?>
                </div>
            </div>

            <?php
            }
             ?>
            <!-- End project 2 -->
            <?php if( isset($theme_option['social_button']) && $theme_option['social_button'] == 1){ ?>
           <!--  Share Btn  -->
            <div id="share">
                <a class="share-btn">
                    <i class="icon ion-android-share-alt"></i>
                </a>
                <div class="share-icons" style="display:none">
                    <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" class="share-google" target="_blank">
                        <i class="fa fa-google-plus" aria-hidden="true"></i>
                    </a>
                    <a href="http://www.twitter.com/share?url=<?php the_permalink(); ?>" class="share-twitter" target="_blank">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    
                    <a href="https://www.linkedin.com/cws/share?url=<?php the_permalink(); ?>" class="share-linkedin" target="_blank">
                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                    <a href="mailto:example@email.com?subject=I wanted you to see this site&body=Check out this site <?php the_permalink(); ?>" class="share-mail">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <!--  END Share Btn  -->
            <?php } ?>
            <!--  Navigation  -->
            <div class="row margin-leftright-null">
                <div class="container">
                   <section id="nav">
                       <div class="row">
                            <div class="col-xs-6">
                                <div class="nav-left">
                                <?php $prev_post = get_previous_post();
                                if (!empty( $prev_post )): ?>
                                    <a href="<?php echo get_permalink( $prev_post->ID ); ?>" class="btn-alt small  margin-null"><i class="icon ion-ios-arrow-left"></i><span><?php echo esc_html__('Prev project','dolomia');?></span>
                                    </a>
                                <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="nav-right">
                                <?php $next_post = get_next_post();
                                if (!empty( $next_post )): ?>
                                    <a href="<?php echo get_permalink( $next_post->ID ); ?>" class="btn-alt small margin-null"><?php echo esc_html__('Next project','dolomia');?></span><i class="icon ion-ios-arrow-right"></i><span></a>
                                <?php endif; ?>
                                </div>
                            </div>
                        </div>
                   </section>
                </div>
            </div>
            <!--  END navigation  -->
        </div>
    </div>
    <!--  END Page Content, class footer-fixed if footer is fixed  -->
    <?php endwhile;
     else:
        echo esc_html__('No content found','dolomia');
 endif; ?>

<?php get_footer(); ?>