<?php
/** 
 * The template for displaying all pages
 */
get_header(); ?>
<!--  Page Content, class footer-fixed if footer is fixed  -->
<?php if ( is_active_sidebar( 'footer-1' ) ||  is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) || is_active_sidebar( 'footer-4' )){ ?>
    <div id="page-content" class="header-static footer-fixed">
<?php }else{ ?>
    <div id="page-content" class="header-static footer-fixed page-content-nonfooter">
<?php } ?>
    <!--  Slider  -->
    <div id="flexslider" class="fullpage-wrap small">
        <ul class="slides">
            <?php 
                if (isset($theme_option['index_page_image_background']) && !empty($theme_option['index_page_image_background'])) {
                    $header_bg = $theme_option['index_page_image_background'];
                } else {
                    $header_bg['url'] = '#';
                }
            ?>
            <li style="background-image:url(<?php echo esc_url($header_bg['url']); ?>);">
                <div class="text text-center">
                    <h1 class="white margin-bottom-small">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h1>
                    
                </div>
                <div class="gradient dark"></div>
            </li>
        </ul>
    </div>
	<!-- Blog Posts -->
	<div id="home-wrap" class="content-section fullpage-wrap row custom-page">
		<div class="container">
		    <div class="col-md-8 padding-leftright-null custom-page">
		    	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php while (have_posts()): the_post(); ?> 	
						<!-- Post -->
							
						<div class="custom-page-content">		
							<?php the_content(); ?>	
										
							<?php wp_link_pages(); ?>
							<div id="comments">
								
	                            <?php comments_template(); ?>
	                                
							</div>
						</div>
					<?php endwhile; ?>	
				</div>
		    </div><!-- /.col-md-9 -->
		    <!--  Right Sidebar  -->
		    <div class="col-md-4 text">
		        <aside class="sidebar">
		        	<?php
	                        if ( is_active_sidebar('sidebar-1') ) {
	                                dynamic_sidebar( 'sidebar-1' );
	                        }
	                    ?>
		        </aside>
		    </div><!-- /.col-md-3 -->
		</div>
	</div><!-- /.row -->
</div>

<?php
get_footer();
?>
