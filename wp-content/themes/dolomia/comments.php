<?php
/**
 * The template for displaying comments.
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package dolomia
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
    return;
}
?>    
    <ul class="nav nav-tabs" role="tablist">
        <?php 
                if ( have_comments() ) : 
            ?>
        <li role="presentation" class="active"><a href="#tab-one" aria-controls="tab-one" role="tab" data-toggle="tab" aria-expanded="true"><?php echo esc_html__('All comments','dolomia'); ?></a></li>
        
        
        <?php endif; ?>
    </ul>



<div class="tab-content no-margin-bottom">
    <div role="tabpanel" class="tab-pane padding-md active" id="tab-one">
        <?php 
            if ( have_comments() ) : 
        ?>
                <?php wp_list_comments('callback=dolomia_theme_comment'); ?>
                <!--End comments and replys-->
                <?php
                    if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
                ?>
                    <nav class="navigation comment-navigation" role="navigation">          
                        <div class="nav-previous"><?php previous_comments_link( esc_html__( '&larr; Older Comments', 'dolomia' ) ); ?></div>
                        <div class="nav-next"><?php next_comments_link( esc_html__( 'Newer Comments &rarr;', 'dolomia' ) ); ?></div>
                    </nav>
                <?php endif;  ?>

                <?php if ( ! comments_open() && get_comments_number() ) : ?>
                    <p class="no-comments"><?php esc_html_e( 'Comments are closed.' , 'dolomia' ); ?></p>
                <?php endif; ?> 
        <?php endif; ?>
    </div>
    


<!-- //COMMENTS -->
    <div role="tabpanel" class="tab-pane padding-md active" id="tab-two">
        <section class="comment-form">

            <?php

            if ( is_singular() )

                $aria_req = ( $req ? " aria-required='true'" : '' );
                $comment_args = array(
                        'id_form'    => 'contact-form',                                
                        'title_reply'=> '<h5>'.esc_html__('Leave a Reply','dolomia').'</h5>',
                        'fields'     => apply_filters( 'comment_form_default_fields', array(
                            'author' => '<div class="row">
                                            <div class="col-md-6">
                                                <input class="form-field" type="text" name="author" id="author" placeholder="'.esc_html__('Name','dolomia').'" >
                                            </div>',   
                            'email' => '    <div class="col-md-6">
                                                <input class="form-field" type="text" name="email" id="email" placeholder="'.esc_html__('Email','dolomia').'" >
                                            </div>
                                        </div>',
                        ) ),                                
                        'comment_field' => '<div class="row">
                                                <div class="col-md-12">
                                                    <textarea rows="6" name="comment"'.$aria_req.' id="comment" class="form-field" placeholder="'.esc_html__('Comments','dolomia').'"></textarea></div></div>',                                                 
                        'label_submit' => 'submit',
                        'class_submit' => 'btn-alt',
                        //'id_submit' => 'submit-contact',
                        'comment_notes_before' => '',
                        'comment_notes_after' => '', 
               
                                                )
                                                ?>
                <?php comment_form($comment_args); ?>
                    
                
        </section>
    </div>
</div>


