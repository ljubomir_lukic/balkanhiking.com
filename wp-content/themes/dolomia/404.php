<?php 
    /*
    */
    get_header();
    $theme_option = get_option('theme_option');
?>          
            <!--  Page Content, class footer-fixed if footer is fixed  -->
            <div id="page-content" class="header-static">
                <!--  Slider  -->
                <div id="flexslider" class="fullpage-wrap small">
                    <ul class="slides page-notfound">
                        <li class="notfound-first"> <?php if( isset($theme_option['404customizemedia-imageheader']['url']) && $theme_option['404customizemedia-imageheader']['url'] != ''){ ?>style="background-image:url(<?php echo esc_url($theme_option['404customizemedia-imageheader']['url']) ?>)"<?php } ?>>
                            <div class="container text text-center">
                                <h1 class="white margin-bottom-small">
                                    <?php 
                                        if (isset($theme_option['text-header']) && !empty($theme_option['text-header'])) {
                                            echo esc_attr($theme_option['text-header']);
                                        } else {
                                            echo esc_html__('404', 'dolomia');
                                        }
                                    ?>
                                </h1>
                                <p class="heading white margin-bottom">
                                    <?php 
                                        if (isset($theme_option['text-message-error']) && !empty($theme_option['text-message-error'])) {
                                            echo esc_attr($theme_option['text-message-error']);
                                        } else {
                                            echo esc_html__('The page has been removed or the link you folowed probabably broken', 'dolomia');
                                        }
                                    ?>
                                </p>
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn-alt small margin-null">
                                    <?php 
                                        if (isset($theme_option['text-backhome']) && !empty($theme_option['text-backhome'])) {
                                            echo esc_attr($theme_option['text-backhome']);
                                        } else {
                                            echo esc_html__('Return Home', 'dolomia');                                    
                                        }
                                    ?>
                                </a>
                            </div>
                            <div class="gradient dark"></div>     
                        </li>

                        <?php echo dolomia_breadcrumbs(); ?>

                    </ul>
                </div>
                <!--  END Slider  -->
            </div>
            <!-- Style fix footer static -->

<?php 
    get_footer('fullwidth');
?>