<?php 
    get_header();

 ?>
<!--  Page Content, class footer-fixed if footer is fixed  -->
<?php if ( is_active_sidebar( 'footer-1' ) ||  is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) || is_active_sidebar( 'footer-4' )){ ?>
    <div id="page-content" class="header-static footer-fixed">
<?php }else{ ?>
    <div id="page-content" class="header-static footer-fixed page-content-nonfooter">
<?php } ?>
                <?php if(isset($theme_option['index_page_image_background'])){ ?>
                <!--  Slider  -->
                <div id="flexslider" class="fullpage-wrap small">
                    <ul class="slides">
                        <li <?php if(isset($theme_option['index_page_image_background'])){ ?>
                            style="background-image: url(<?php echo esc_url($theme_option['index_page_image_background']['url']); ?>)"  <?php }?> >
                            <div class="container text text-center">
                                <h1 class="white margin-bottom-small">
                                   <?php printf( esc_html__( 'Tag Archives: %s', 'dolomia' ), single_tag_title( '', false ) ); ?>
                                </h1> 
                            </div>
                            <div class="gradient dark"></div>
                        </li>
                        
                    </ul>
                </div>
                <!--  END Slider  -->
                <?php }else{ ?>
                <!--  Slider  -->                
                <div id="flexslider" class="flexslider-custom fullpage-wrap">
                    <ul class="slides slides-custom">
                        <li <?php if(isset($theme_option['index_page_image_background'])){ ?>
                            style="background-image: url(<?php echo esc_url($theme_option['index_page_image_background']['url']); ?>)"  <?php }?> >
                            
                            <div class="gradient dark"></div>
                        </li>
                    </ul>
                </div>
                <!--  END Slider  -->
                <?php } ?>
                <div id="home-wrap" class="content-section fullpage-wrap row grey-background">
                    <div class="container">
                        <div class="col-md-8 padding-leftright-null">
                            <!--  News Section  -->
                            <section id="news" class="page">
                                <div class="news-items equal one-columns">
                                <?php 
                                if(have_posts()) : while (have_posts()) : the_post();
                                    
                                 ?>
                                    <div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
                                        <div class="single-news one-item horizontal-news">
                                            <article>
                                            
                                            <?php if(has_post_thumbnail()){ ?>
                                            <div class="col-md-6 padding-leftright-null">
                                                <div class="image" style="background-image:url(<?php the_post_thumbnail_url(); ?>);"></div>
                                            </div>
                                            <?php } ?>
                                            <?php if(has_post_thumbnail()){ ?>
                                            <div class="col-md-6 padding-leftright-null">
                                            <?php }else{ ?>
                                            <div class="col-md-12 padding-leftright-null">
                                            <?php } ?>
                                                <div class="content">
                                                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                    <?php if ( is_sticky() && is_home() && ! is_paged() ){ ?>
                                                        <div class="featured-post">
                                                            <?php esc_html_e( 'Featured post', 'dolomia' ); ?>
                                                        </div>
                                                    <?php } ?>
                                                    <span class="date"><?php the_date(); ?></span>
                                                    <p><?php echo dolomia_excerpt(20); ?></p>
                                                    <?php 
                                                        $categories = get_the_category();
                                                        if (! empty($categories)) {
                                                            foreach ($categories as $category) { 
                                                                echo '<span class="category"><a  href="'.get_category_link($category->cat_ID).'">'.$category->name.'</a></span>';
                                                            }
                                                        }
                                                     ?>                        
                                                </div>
                                            </div>
                                            
                                        </article>
                                        </div>
                                    </div>
                                <?php 
                                endwhile;
                                else:
                                        echo esc_html__('No content found','dolomia');
                                endif;
                                ?>
                                </div>
                            </section>
                            <!--  END News Section  -->
                        </div>
                        <!--  Right Sidebar  -->
                        <div class="col-md-4 text">
                            <aside class="sidebar">
                                <div class="widget-wrapper">
                                    <?php if ( is_active_sidebar( 'sidebar-1' ) ) : 
                                      dynamic_sidebar('sidebar-1');
                                      endif;
                                    ?>
                                </div>
                            </aside>
                        </div>
                        <!--  END Right Sidebar  -->
                        <div class="row margin-leftright-null">
                            <!--  Navigation  -->
                            <section id="nav" class="padding-top-null grey-background">
                                <?php dolomia_pagination();  ?>
                            </section>
                            <!--  END Navigation  -->
                        </div>
                    </div>
                </div>
            </div>
            <!--  END Page Content, class footer-fixed if footer is fixed  -->

<?php get_footer(); ?>