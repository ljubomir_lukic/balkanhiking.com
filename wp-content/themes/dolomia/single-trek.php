<?php get_header(); ?>

<?php 
     if(have_posts()):
        while ( have_posts() ) : the_post();  
    	$theme_option = get_option('theme_option');

        $trek_title    = get_post_meta( get_the_ID(), 'trek_title', true );
        $trek_subtitle = get_post_meta( get_the_ID(), 'trek_subtitle', true );
        $trek_header_image_background  = get_post_meta( get_the_ID(), 'trek_header_image_background', 1 );

        // $trek_name = get_post_meta( get_the_ID(), 'trek_name', true );
        $trek_subname = get_post_meta( get_the_ID(), 'trek_subname', true );
        $trek_des_trip = get_post_meta( get_the_ID(), 'trek_des_trip', true );

        $trek_img_avatar  = get_post_meta( get_the_ID(), 'trek_img_avatar', 1 );
        $trek_name_place = get_post_meta( get_the_ID(), 'trek_name_place', true );
        $trek_visitor = get_post_meta( get_the_ID(), 'trek_visitor', true );
        $trek_country = get_post_meta( get_the_ID(), 'trek_country', true );

        $trek_socials = get_post_meta( get_the_ID(),'trek_socials', true );

        $trek_data_bg  = get_post_meta( get_the_ID(), 'trek_data_bg', 1 );
        $trek_heading = get_post_meta( get_the_ID(),'trek_heading', true );
        $trek_shot_des = get_post_meta( get_the_ID(),'trek_shot_des', true );

        $trek_data = get_post_meta( get_the_ID(),'trek_data', true );

        $dolomia_similar_trips_id = get_post_meta( get_the_ID(),'dolomia_similar_trips_id', true );
        $dolomia_similar_trips_title = get_post_meta( get_the_ID(),'dolomia_similar_trips_title', true );
        $dolomia_similar_trips_subtitle = get_post_meta( get_the_ID(),'dolomia_similar_trips_subtitle', true );


        //contact
        $trek_contact_caption = get_post_meta( get_the_ID(),'trek_contact_caption', true );
        $trek_contact_info1 = get_post_meta( get_the_ID(),'trek_contact_info1', true );
        $trek_contact_info2 = get_post_meta( get_the_ID(),'trek_contact_info2', true );
        $trek_contact_info3 = get_post_meta( get_the_ID(),'trek_contact_info3', true );
?>
	<div id="page-content" class="header-static footer-fixed">
        <!--  Slider  -->
        <div id="flexslider" class="fullpage-wrap small">
            <ul class="slides">
                <li style="background-image:url(<?php echo esc_url($trek_header_image_background); ?>)">
                <div class="container text text-center">
                    <h1 class="white margin-bottom-small"><?php echo esc_attr($trek_title); ?></h1> 
                    <p class="heading white"><?php echo esc_attr($trek_subtitle); ?></p>
                </div>
                <div class="gradient dark"></div>
            </li>
            <?php echo dolomia_breadcrumbs(); ?>
            </ul>
        </div>
		
		<?php if($trek_subname !== 'showcase'):?>
            <div id="post-wrap" class="content-section fullpage-wrap">
                <!-- Section same Height. Child get the parent Height. Set the same id -->
                <div class="row margin-leftright-null">
                    <div class="container">
                        <!-- About -->
                        <div class="col-lg-6 padding-leftright-null">
                            <div data-responsive="parent-height" data-responsive-id="adv" class="text padding-lg-bottom-null">
                                <h2 class="margin-bottom-null title line left"><?php the_title();?></h2>
                                <p class="heading left grey"><?php echo esc_attr($trek_subname); ?></p>
                                <div class="padding-onlytop-md">
                                    <p><?php echo esc_attr($trek_des_trip); ?></p>
                                </div>
                            </div>
                        </div>
                        <!-- END About -->
                        <div class="col-lg-6 padding-leftright-null">
                            <div data-responsive="child-height" data-responsive-id="adv" class="text padding-md-top-null height-auto-lg">
                                <div class="grey-background box-shadow text responsive-padding-sm">
                                    <!-- Hiker -->
                                    <div class="hiker">
                                        <img src="<?php echo esc_url($trek_img_avatar); ?>" alt="">
                                        <h3 class=""><?php echo esc_attr($trek_name_place); ?></h3>
                                        <span class="meta grey">Name: <?php echo esc_attr($trek_visitor); ?></span>
                                        <span class="meta grey">Nation: <?php echo esc_attr($trek_country); ?></span>
    
                                        <ul class="social">
                                       <?php  
                                       if(isset($trek_socials) && $trek_socials != ''){
    			                            foreach ( (array) $trek_socials as $key => $entrys ) {
    				                            $trek_socials_icon = $trek_socials_link = '';
    
    				                            if ( isset( $entrys['trek_socials_icon'] ) )
    			                                  	$trek_socials_icon =  $entrys['trek_socials_icon'];
    			                                if ( isset( $entrys['trek_socials_link'] ) )
                              						$trek_socials_link = $entrys['trek_socials_link']; 
    
    			                             ?>
                                            <li>
                                                <a href="<?php echo esc_attr($trek_socials_link); ?>"><i  class="icon ion-social-<?php echo esc_attr($trek_socials_icon); ?>"></i></a>
                                            </li>
                                
                                            <?php 
                                            } 
                                        }
                                        ?>
                                        </ul>
                                    </div>
                                    <!-- END Hiker -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Section same Height. Child get the parent Height. Set the same id -->
                <!--  Section Image Background with overlay  -->
    
                <?php 
                //trek post style 1
                if(get_post_meta(get_the_ID(),'trek_type', true)== 'standard'){ ?>
                <div class="row margin-leftright-null grey-background">
                    <div class="bg-img overlay simple-parallax responsive" style="background-image:url(<?php echo esc_url($trek_data_bg); ?>)">
                       <div class="container">                       
                            <div class="text trek-data text-center">
                            <?php  
                            if(isset($trek_data) && $trek_data != ''){
                                foreach ( (array) $trek_data as $key => $entrys ) {
                                    $trek_data_icon = $trek_data1 =$trek_data2= '';
    
                                    if ( isset( $entrys['trek_data_icon'] ) )
                                        $trek_data_icon =  $entrys['trek_data_icon'] ;
                                    if ( isset( $entrys['trek_data1'] ) )
                                        $trek_data1 = $entrys['trek_data1'] ; 
                                    if ( isset( $entrys['trek_data2'] ) )
                                        $trek_data2 = $entrys['trek_data2'] ; 
                                 ?>
                                <div class="col-sm-4 col-md-2">
                                   <i class="pd-icon-<?php echo esc_attr($trek_data_icon);?> service big margin-bottom-null white"></i>
                                   <em class="color"><?php echo esc_attr($trek_data1);?></em>
                                   <h3 class="white"><?php echo esc_attr(rtrim($trek_data2,'|'));?></h3>
                                </div>
                                <?php 
                                } 
                            }
                            ?>
                               
                           </div>
                           <!-- END Trek data -->
                       </div>
                    </div>
                </div>
                <?php
                }
                else{ ?>
                <div class="row margin-leftright-null grey-background">
                    <div class="container">
                        <div class="col-md-12 text padding-bottom-null text-center">
                            <h2 class="margin-bottom-null title line center"><?php echo esc_attr($trek_heading); ?></h2>
                            <p class="heading center grey margin-bottom-null"><?php echo esc_attr($trek_shot_des); ?></p>
                        </div>
                        <!-- Trek Data -->
                        <div class="text padding-bottom-null trek-data text-center">
                        <?php  
                        if(isset($trek_data) && $trek_data != ''){
                            foreach ( (array) $trek_data as $key => $entrys ) {
                                $trek_data_icon = $trek_data1 =$trek_data2= '';
    
                                if ( isset( $entrys['trek_data_icon'] ) )
                                    $trek_data_icon =  $entrys['trek_data_icon'] ;
                                if ( isset( $entrys['trek_data1'] ) )
                                    $trek_data1 =  $entrys['trek_data1'] ; 
                                if ( isset( $entrys['trek_data2'] ) )
                                    $trek_data2 =  $entrys['trek_data2'] ; 
                             ?>
                                <div class="col-sm-4 col-md-2">
                                   <i class="pd-icon-<?php echo esc_attr($trek_data_icon);?> service big margin-bottom-null"></i>
                                   <em><?php echo esc_attr($trek_data1);?></em>
                                   <h3 class="color"><?php echo esc_attr($trek_data2);?></h3>
                                </div>
                                <?php 
                                } 
                            }
                            ?>
                            </div>
                            <!-- END Trek Data -->                        
                        </div>
                </div>
                   <?php
                    }
                 ?>               
                <!--  END Section Image Background with overlay  -->
            <?php else:?>
            <div id="post-wrap" class="content-section fullpage-wrap" style="margin-top: 30px;"></div>
            <?php endif;?>
            <?php 
                the_content();
            ?>
            <?php if( isset($theme_option['social_button']) && $theme_option['social_button'] == 1){ ?>
             <!--  Share Btn  -->
            <div id="share">
                <a class="share-btn">
                    <i class="icon ion-android-share-alt"></i>
                </a>
                <div class="share-icons" style="display:none">
                    <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" class="share-google" target="_blank">
                        <i class="fa fa-google-plus" aria-hidden="true"></i>
                    </a>
                    <a href="http://www.twitter.com/share?url=<?php the_permalink(); ?>" class="share-twitter" target="_blank">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    
                    <a href="https://www.linkedin.com/cws/share?url=<?php the_permalink(); ?>" class="share-linkedin" target="_blank">
                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                    <a href="mailto:example@email.com?subject=I wanted you to see this site&body=Check out this site <?php the_permalink(); ?>" class="share-mail">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <!--  END Share Btn  -->
            <?php } ?>
            <div id="showcase-treks" class="row margin-leftright-null grey-background">
                <div class="container">
                    <div class="col-md-12 text padding-bottom-null text-center">
                        <h2 class="margin-bottom-null title line center"><?php echo esc_attr($dolomia_similar_trips_title) ?></h2>
                        <p class="heading center grey margin-bottom-null"><?php echo esc_attr($dolomia_similar_trips_subtitle) ?></p>
                    </div>
                    <div class="col-md-12 text" id="treks">
                    <!-- Single Trip -->
                    <?php
                        $arr_id_trip = explode(',' ,$dolomia_similar_trips_id);
                        $show_post = array(
                            'post_type' => 'trek',
                            'posts_per_page'  => -1,
                            'post__in'  => $arr_id_trip,
                            );
                         $all_post = new WP_Query($show_post);

                        if ($all_post->have_posts()) : while($all_post->have_posts()) : $all_post->the_post();
                            $name_place = get_post_meta( get_the_ID(), 'trek_name_place', true );
                            $country = get_post_meta( get_the_ID(), 'trek_country', true );
                            $trek_datas = get_post_meta( get_the_ID(),'trek_data', true );
                            $guide = get_post_meta( get_the_ID(),'trek_visitor', true );
                        ?>
                        <div class="item col-md-6">
                            <div class="showcase-trek">
                             <?php 
                                if(isset($trek_datas) && $trek_datas != ''){
                                     $x = '';
                                     $y = '';
                                    foreach ($trek_datas as $entrys ) {
                                       $x .= $entrys['trek_data2'].' ';
                                       $y .= $entrys['trek_data1'].',';
                                }
                                $a = explode("|", $x);
                                $b = explode(",", $y);
                            ?>
                               <span class="read">
                                    <?php echo esc_attr($a[0]); ?>
                                </span>
                                <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="">
                                <div class="content text-center">
                                    <div class="row margin-leftright-null">
                                        <div class="meta">
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3><?php echo esc_attr($a[4]); ?></h3>
                                                <h4><?php echo esc_attr($b[4]); ?></h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3><?php echo esc_attr($a[3]); ?></h3>
                                                <h4><?php echo esc_attr($b[3]); ?></h4>
                                            </div>
                                            <div class="col-md-4 padding-leftright-null">
                                                <h3><?php echo esc_attr($a[2]); ?></h3>
                                                <h4><?php echo esc_attr($b[2]); ?></h4>
                                            </div>
                                        </div>
                                        <div class="category">
                                            <h3><?php the_title(); ?></h3>
                                        </div>
                                        <div class="info">
                                        <!-- <?php 
                                             if(isset($trek_datas) && $trek_datas != ''){
                                         ?> -->
                                            <div class="col-md-12 padding-leftright-null">
                                                <h6 class="heading"><?php echo esc_attr($name_place); ?></h6>
                                                <p class="margin-null"><?php echo esc_attr($guide); ?></p>
                                            </div>
                                            <!-- <?php } ?> -->
                                        </div>
                                    </div>
                                </div>
                                <a href="<?php the_permalink() ?>" class="link"></a>
                                <?php } ?>
                            </div>
                        </div>
                        <!-- END Single Trip -->
                        <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
            <!-- END Similar Trips -->

            <!-- Call to Action -->
            <div class="row text margin-leftright-null color-background">
                <div class="col-md-12 text-center">
                    <h4 class="big white"><?php echo esc_attr($trek_contact_caption); ?></h4>
                    <h4 class="big margin-bottom-small white"><?php echo esc_attr($trek_contact_info1) ?><a href="" class="btn-pro simple white"><?php echo esc_attr($trek_contact_info2) ?></a></h4>
                    <a href="<?php echo site_url('contact')?>" class="btn-alt small white margin-null active shadow"><?php echo esc_attr($trek_contact_info3) ?></a>
                </div>
            </div>
            <!-- END Call to Action -->
        </div>
	</div>
	<?php 
		endwhile;
    else:
        echo esc_html__('No content found','dolomia');
    endif; ?>

<?php get_footer(); ?>