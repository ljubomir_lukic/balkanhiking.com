<?php $theme_option = get_option('theme_option');  ?>

   <!--  Footer. Class fixed for fixed footer  -->
    <footer class="full-width">
        <div class="container">
            <div class="row no-margin">
                <div class="col-sm-4 col-md-2 padding-leftright-null">
                    <?php if ( is_active_sidebar( 'footer-1' ) ) : 
                                      dynamic_sidebar('footer-1');
                                      endif;
                                    ?>
                </div>
                <div class="col-sm-4 col-md-2 padding-leftright-null">
                    <?php if ( is_active_sidebar( 'footer-2' ) ) : 
                                      dynamic_sidebar('footer-2');
                                      endif;
                                    ?>
                </div>
                <div class="col-sm-4 col-md-4 padding-leftright-null">
                    <?php if ( is_active_sidebar( 'footer-3' ) ) : 
                          dynamic_sidebar('footer-3');
                          endif;
                        ?>
                </div>
                <div class="col-md-4 padding-leftright-null">
                    <?php 
                        if (is_active_sidebar('newsletters_widget')) {
                          dynamic_sidebar('newsletters_widget');
                        }                       
                     ?>
                </div>
            </div>
            <div class="copy">
                <div class="row no-margin">
                    <div class="col-md-8 padding-leftright-null">
                        <?php if (isset($theme_option['footer_copyright_content'])) { echo esc_attr($theme_option['footer_copyright_content']);
                                }; ?>
                    </div>
                    <div class="col-md-4 padding-leftright-null">
                        <ul class="social">
                            <?php  
                            if (isset($theme_option['footer_social']) && $theme_option['footer_social'] != '' ) {
                              foreach ($theme_option['footer_social'] as $footer_socials) {
                                $footer_social_explode = explode(",",$footer_socials);

                            ?>
                              <li> <a href="<?php echo esc_attr($footer_social_explode[0]) ?>"><i class="fa fa-<?php echo esc_attr($footer_social_explode[1]) ?>" aria-hidden="true"></i></a> </li>
                     
                          <?php } 
                            }
                           ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<!--  END Footer. Class fixed for fixed footer  -->
</div>
<!--  Main Wrap  -->

<?php wp_footer(); ?>
</body>
</html>