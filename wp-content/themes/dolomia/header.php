<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <?php wp_head();?>
        
        <!-- Yandex.Metrika counter --> 
        <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter47096934 = new Ya.Metrika({ id:47096934, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/47096934" style="position:absolute; left:-9999px;" alt="" /></div></noscript> 
        <!-- /Yandex.Metrika counter -->
    </head>
     <?php 
    $theme_option = get_option('theme_option');
    ?>
    <body <?php body_class(); ?>>
      
        <!--  loader  -->
        <div id="myloader">
            <span class="loader">
                <div class="inner-loader"></div>
            </span>
        </div>
        
        <!--  Main Wrap  -->
        <div id="main-wrap" class="full-width">
            <!--  Header & Menu  -->
            <header id="header" class="fixed transparent full-width">
                <div class="container">
                    <nav class="navbar navbar-default white">
                        <!--  Header Logo  -->
                        <div id="logo">
                            <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                                <?php 
                                    if (isset($theme_option['home_image_logo']) && !empty($theme_option['home_image_logo'])) { ?>
                                        <img src="<?php echo esc_url($theme_option['home_image_logo']['url']); ?>" class="normal" alt="">
                                    <?php
                                    }
                                    else { ?>
                                        <h3 class="site-title"><?php bloginfo( 'name' ); ?></h3>
                                    <?php } ?>
                                <?php 
                                    if (isset($theme_option['home_image_logo_retina']) && !empty($theme_option['home_image_logo_retina'])) { ?>
                                        <img src="<?php echo esc_url($theme_option['home_image_logo_retina']['url']); ?>" class="retina" alt="">
                                    <?php
                                    }
                                    else{}
                                 ?>
                                <?php 
                                    if (isset($theme_option['home_image_logo_white']) && !empty($theme_option['home_image_logo_white'])) { ?>
                                        <img src="<?php echo esc_url($theme_option['home_image_logo_white']['url']); ?>" class="normal white-logo" alt="">
                                    <?php
                                    }
                                    else{}
                                ?>
                                <?php 
                                    if (isset($theme_option['home_image_logo_white_retina']) && !empty($theme_option['home_image_logo_white_retina'])) { ?>
                                        <img src="<?php echo esc_url($theme_option['home_image_logo_white_retina']['url']); ?>" class="retina white-logo" alt="">
                                    <?php
                                    }
                                    else{}
                                 ?>
                            </a>
                        </div>
                        <!--  END Header Logo  -->
                        <!--  Classic menu, responsive menu classic  -->
                        <div id="menu-classic">
                            <div class="menu-holder">
                                <?php
                                    $primarymenu = array(
                                      'theme_location'  => 'primary',
                                      'menu'            => '',
                                      'container'       => '',
                                      'container_class' => '',
                                      'container_id'    => '',
                                      'menu_class'      => 'submenu',
                                      'menu_id'         => '',
                                      'echo'            => true,
                                       'fallback_cb'       => 'dolomia_wp_bootstrap_navwalker::fallback',
                                       'walker'            => new dolomia_wp_bootstrap_navwalker(),
                                      'before'          => '',
                                      'after'           => '',
                                      'link_before'     => '',
                                      'link_after'      => '',
                                      'items_wrap'      => '<ul>%3$s</ul>',
                                      'depth'           => 0,
                                      );
                                      if ( has_nav_menu( 'primary' ) ) {
                                        wp_nav_menu( $primarymenu );
                                      }
                                ?>
                                    <!-- Search Icon -->
                                    <li class="search" >
                                        <i class="icon ion-ios-search"></i>
                                    </li>  
                            </div>  

                        </div>
                        <!--  END Classic menu, responsive menu classic  -->
                        <!--  Button for Responsive Menu Classic  -->
                        <div id="menu-responsive-classic">
                            <div class="menu-button">
                                <span class="bar bar-1"></span>
                                <span class="bar bar-2"></span>
                                <span class="bar bar-3"></span>
                            </div>
                        </div>
                        <!--  END Button for Responsive Menu Classic  -->
                        <!--  Search Box  -->
                        <div id="search-box" class="full-width">
                            <form role="search" id="search-form" method="get" class="black big"  action = "<?php echo esc_url( home_url( '/' ) ); ?>">
                                <div class="form-input">
                                    <input class="form-field black big" type="search" name="s" placeholder="<?php echo esc_html__('Search...','dolomia'); ?>" value= "<?php echo get_search_query() ?>">
                                    <span class="form-button big">
                                        <button type="button">
                                            <i class="icon ion-ios-search"></i>
                                        </button>
                                    </span>
                                </div>       
                            </form>
                            <button class="close-search-box">
                                <i class="icon ion-ios-close-empty"></i>
                            </button>
                        </div>
                        <!--  END Search Box  -->
                    </nav>
                </div>
            </header>
            <!--  END Header & Menu  -->
            