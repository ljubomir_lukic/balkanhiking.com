<?php
// class widget
class recent_post extends WP_Widget {
        
        // declere widget
        // config: ID , Description
        function recent_post() {
                $wget_options = array(
                        'classname' => 'latest-widget', // ID and class name of widget
                        'description' => 'Widget show latest course.' // DES show in dashboard
                );
                parent::__construct('recent_post', 'Recent Post Dolomia', $wget_options);
        }

        // Widget Backend - anything show in widget
        function form( $instance ) {
                $default = array(
                        'title'   => '',
                        'number'  => '',
                );
                $instance         = wp_parse_args( (array) $instance, $default );
                $title            = esc_attr($instance['title']);
                $number_course    = esc_attr($instance['number']);
                echo '<p>Title <input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
                echo '<p>Number Recent Post show <input type="text" class="widefat" name="'.$this->get_field_name('number').'" value="'.$number_course.'"/></p>';
        }

        // Updating widget replacing old instances with new
        function update( $new_instance, $old_instance ) {
                $instance = $old_instance;
                $instance['title'] = strip_tags($new_instance['title']);
                $instance['number'] = strip_tags($new_instance['number']);
                return $instance;
        }
        
        // Creating widget front-end
        function widget( $args, $instance ) {
                extract($args);
                $title         = apply_filters( 'widget_title', $instance['title'] );
                $number_course = apply_filters( 'widget_title', $instance['number'] );

                echo $before_widget;
                echo $before_title.$title.$after_title;
                $args = array(
                        'post_type'      => 'post',
                        'orderby'        => 'date',
                        'order'          => 'DESC',
                        'posts_per_page' => $number_course,
                    );
                $query = new WP_Query( $args );

                if($query->have_posts()):
                ?>

                
                <!-- recent post -->
                
                    <h5>Recent Post</h5>
                    <ul class="recent-posts">
                    <?php
                        while ( $query->have_posts()) : $query->the_post();
                            $format_of_post = get_post_format();
                        ?>
                        <li>
                            <img src="<?php echo esc_url(wp_get_attachment_url(get_post_thumbnail_id()));?>" alt="">
                            <div class="content">
                                <a href="<?php the_permalink(); ?>">
                                    <span class="meta">
                                        <?php the_date('F j, Y'); ?>
                                    </span>
                                    <p><?php echo esc_html__('Post with','dolomia'); ?> <?php  if(false === $format_of_post ){echo 'image';} else{echo $format_of_post; }?></p>
                                </a>
                            </div>
                        </li>
                        
                        <?php
                        endwhile;?>
                    </ul>
               
                <?php endif;
                echo $after_widget;
        }
 
}
 
register_widget('recent_post');